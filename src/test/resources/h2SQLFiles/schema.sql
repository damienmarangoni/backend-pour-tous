-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 10.195.181.101    Database: pomniws
-- ------------------------------------------------------
-- Server version	5.1.44-community-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `art`
--

DROP TABLE IF EXISTS `art`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art` (
  `art_id` tinyint(4) NOT NULL DEFAULT '0',
  `art_txt` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`art_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `billing`
--

DROP TABLE IF EXISTS `billing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing` (
  `billing_id` int(11) NOT NULL DEFAULT '0',
  `billing_description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`billing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blacklist`
--

DROP TABLE IF EXISTS `blacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blacklist` (
  `blacklist_recipient` varchar(100) NOT NULL DEFAULT '',
  `blacklist_date` date DEFAULT NULL,
  PRIMARY KEY (`blacklist_recipient`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaign`
--

DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `id_campaign` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(45) NOT NULL DEFAULT '',
  `date_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_end` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_campaign`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contract_customer`
--

DROP TABLE IF EXISTS `contract_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract_customer` (
  `id_contract_customer` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_contract_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_qualification`
--

DROP TABLE IF EXISTS `customer_qualification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_qualification` (
  `id_customer_qualification` int(11) NOT NULL AUTO_INCREMENT,
  `customer_qualification_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_customer_qualification`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dlr`
--

DROP TABLE IF EXISTS `dlr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dlr` (
  `art_id` tinyint(4) NOT NULL DEFAULT '0',
  `message_id` int(11) unsigned NOT NULL DEFAULT '0',
  `dlr_ts` datetime NOT NULL,
  PRIMARY KEY (`art_id`,`message_id`),
  KEY `dlr_index3977` (`dlr_ts`),
  KEY `dlr_FKIndex1` (`art_id`),
  KEY `dlr_FKIndex2` (`message_id`),
  CONSTRAINT `dlr_ibfk_1` FOREIGN KEY (`art_id`) REFERENCES `art` (`art_id`),
  CONSTRAINT `dlr_ibfk_2` FOREIGN KEY (`message_id`) REFERENCES `message` (`message_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `format`
--

DROP TABLE IF EXISTS `format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `format` (
  `format_id` varchar(20) NOT NULL DEFAULT '',
  `format_description` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`format_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id_location` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_location`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location_zipcode`
--

DROP TABLE IF EXISTS `location_zipcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_zipcode` (
  `id_location` int(11) NOT NULL DEFAULT '0',
  `id_zipcode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_location`,`id_zipcode`),
  KEY `fk_location_zipcode` (`id_zipcode`),
  CONSTRAINT `fk_location_zipcode` FOREIGN KEY (`id_location`) REFERENCES `location` (`id_location`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mask`
--

DROP TABLE IF EXISTS `mask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mask` (
  `mask_masked_nb` varchar(20) NOT NULL DEFAULT '',
  `mask_public_nb` varchar(20) NOT NULL DEFAULT '',
  `mask_description` varchar(100) NOT NULL DEFAULT '',
  `mask_message` varchar(255) DEFAULT '',
  `mask_audio_msg` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`mask_masked_nb`),
  UNIQUE KEY `mask_description` (`mask_description`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `message_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `format_id` varchar(20) NOT NULL DEFAULT '',
  `message_du` int(11) unsigned NOT NULL DEFAULT '0',
  `message_service_type` int(11) NOT NULL DEFAULT '0',
  `message_template_type` int(11) NOT NULL DEFAULT '0',
  `message_template` varchar(255) DEFAULT NULL,
  `message_values` text NOT NULL,
  `message_origin` varchar(10) NOT NULL DEFAULT '',
  `message_client` varchar(10) NOT NULL DEFAULT '',
  `message_recipient` varchar(100) NOT NULL DEFAULT '',
  `message_delivery` varchar(255) NOT NULL DEFAULT '',
  `message_ts_rcvd` datetime DEFAULT NULL,
  `message_ts_save` datetime DEFAULT NULL,
  `message_state` char(1) NOT NULL DEFAULT '',
  `message_format_sent` varchar(10) NOT NULL DEFAULT '',
  `message_content_sent` int(11) NOT NULL DEFAULT '0',
  `message_billing` int(11) NOT NULL DEFAULT '0',
  `message_mo_id` bigint(20) DEFAULT NULL,
  `message_channel` varchar(255) DEFAULT NULL,
  `message_category` int(11) DEFAULT NULL,
  `message_community` int(11) DEFAULT NULL,
  `message_comment_text` varchar(255) DEFAULT NULL,
  `message_origin_geoid` varchar(4) DEFAULT '1',
  `message_customer` int(11) DEFAULT '1',
  `message_template_id` int(11) DEFAULT NULL,
  `message_template_content_id` varchar(45) DEFAULT NULL,
  `message_optional` varchar(255) DEFAULT NULL,
  `message_additionnal` text,
  PRIMARY KEY (`message_id`),
  KEY `message_FKIndex1` (`format_id`),
  KEY `message_index4054` (`message_state`),
  KEY `message_index4077` (`message_ts_rcvd`),
  KEY `fk_service_type_id` (`message_service_type`),
  KEY `fk_template_type_id` (`message_template_type`),
  KEY `fk_billing_id` (`message_billing`),
  CONSTRAINT `fk_billing_id` FOREIGN KEY (`message_billing`) REFERENCES `billing` (`billing_id`),
  CONSTRAINT `fk_service_type_id` FOREIGN KEY (`message_service_type`) REFERENCES `service_type` (`service_type_id`),
  CONSTRAINT `fk_template_type_id` FOREIGN KEY (`message_template_type`) REFERENCES `template_type` (`template_type_id`),
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`format_id`) REFERENCES `format` (`format_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13136977 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `poi`
--

DROP TABLE IF EXISTS `poi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poi` (
  `poi_id` int(11) NOT NULL DEFAULT '0',
  `poi_zipcode` varchar(100) NOT NULL DEFAULT '',
  `poi_prefix` varchar(100) NOT NULL DEFAULT '',
  `poi_bonus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`poi_id`,`poi_zipcode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `program`
--

DROP TABLE IF EXISTS `program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `program` (
  `id_program` int(11) NOT NULL AUTO_INCREMENT,
  `date_start` timestamp NULL DEFAULT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  `access` int(11) DEFAULT '1',
  `id_program_parent` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_program`),
  KEY `fk_id_program_parent` (`id_program_parent`),
  CONSTRAINT `fk_program_program_parent_constraint` FOREIGN KEY (`id_program_parent`) REFERENCES `program_parent` (`id_program_parent`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1131 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `program_parent`
--

DROP TABLE IF EXISTS `program_parent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `program_parent` (
  `id_program_parent` int(11) NOT NULL AUTO_INCREMENT,
  `date_start` timestamp NULL DEFAULT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  `program_interval` int(11) DEFAULT NULL,
  `track_number` int(11) DEFAULT NULL,
  `id_customer_qualification` int(11) NOT NULL DEFAULT '0',
  `id_campaign` int(11) NOT NULL DEFAULT '0',
  `id_template` int(11) NOT NULL DEFAULT '0',
  `id_location` int(11) NOT NULL DEFAULT '1',
  `access` int(11) DEFAULT '1',
  `template_content_id` varchar(45) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '3',
  `vEvent` longtext,
  PRIMARY KEY (`id_program_parent`),
  KEY `fk_program_customer_qualification` (`id_customer_qualification`),
  KEY `fk_program_template` (`id_template`),
  KEY `fk_program_campaign` (`id_campaign`),
  KEY `fk_program_location` (`id_location`),
  KEY `fk_template_content_id` (`template_content_id`),
  KEY `fk_id_priority` (`priority`),
  CONSTRAINT `fk_parent_template_content_id` FOREIGN KEY (`template_content_id`) REFERENCES `template_content` (`template_content_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_priority_program_parent_constraint` FOREIGN KEY (`priority`) REFERENCES `ref_priority` (`id_priority`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_program_parent_campaign` FOREIGN KEY (`id_campaign`) REFERENCES `campaign` (`id_campaign`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_program_parent_customer_qualification` FOREIGN KEY (`id_customer_qualification`) REFERENCES `customer_qualification` (`id_customer_qualification`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_program_parent_location` FOREIGN KEY (`id_location`) REFERENCES `location` (`id_location`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_program_parent_template` FOREIGN KEY (`id_template`) REFERENCES `template` (`id_template`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=829 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `program_parent_activity`
--

DROP TABLE IF EXISTS `program_parent_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `program_parent_activity` (
  `id_program_parent` int(11) NOT NULL DEFAULT '0',
  `code_activity` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_program_parent`,`code_activity`),
  KEY `fk_program_parent_has_ref_activity_program_parent` (`id_program_parent`),
  KEY `fk_program_parent_has_ref_activity_ref_activity` (`code_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qualification_expression`
--

DROP TABLE IF EXISTS `qualification_expression`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qualification_expression` (
  `id_qualification_expression` int(11) NOT NULL AUTO_INCREMENT,
  `service_du` varchar(10) DEFAULT NULL,
  `code_category` int(11) DEFAULT NULL,
  `id_customer_qualification` int(11) NOT NULL DEFAULT '0',
  `community_filter` int(11) DEFAULT NULL,
  `community_response` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_qualification_expression`),
  KEY `fk_qualification_expression_service` (`service_du`),
  KEY `fk_qualification_expression_ref_category` (`code_category`),
  KEY `fk_qualification_expression_customer_qualification` (`id_customer_qualification`),
  CONSTRAINT `fk_qualification_expression_customer_qualification` FOREIGN KEY (`id_customer_qualification`) REFERENCES `customer_qualification` (`id_customer_qualification`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_qualification_expression_ref_category` FOREIGN KEY (`code_category`) REFERENCES `ref_category` (`code_category`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_qualification_expression_service` FOREIGN KEY (`service_du`) REFERENCES `service` (`service_du`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=721 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `real_program`
--

DROP TABLE IF EXISTS `real_program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `real_program` (
  `id_real_program` int(11) NOT NULL AUTO_INCREMENT,
  `date_start` timestamp NULL DEFAULT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  `id_program` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_real_program`),
  KEY `fk_id_program` (`id_program`),
  CONSTRAINT `fk_program_parent_constraint` FOREIGN KEY (`id_program`) REFERENCES `program` (`id_program`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4205 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_activity`
--

DROP TABLE IF EXISTS `ref_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_activity` (
  `code_activity` varchar(10) NOT NULL DEFAULT '',
  `lib_activity` varchar(155) DEFAULT NULL,
  PRIMARY KEY (`code_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_category`
--

DROP TABLE IF EXISTS `ref_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_category` (
  `code_category` int(11) NOT NULL DEFAULT '0',
  `lib_category` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`code_category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_channel`
--

DROP TABLE IF EXISTS `ref_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_channel` (
  `id_channel` int(11) NOT NULL DEFAULT '0',
  `lib_channel` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_channel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_community`
--

DROP TABLE IF EXISTS `ref_community`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_community` (
  `code_community` int(11) NOT NULL DEFAULT '0',
  `lib_community` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`code_community`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_default_audio_track`
--

DROP TABLE IF EXISTS `ref_default_audio_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_default_audio_track` (
  `filename` varchar(255) NOT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `phase` varchar(45) DEFAULT NULL,
  `service` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`filename`),
  KEY `index` (`libelle`),
  KEY `fk_ref_default_audio_track_phase` (`phase`),
  KEY `fk_ref_default_audio_track_service` (`service`),
  CONSTRAINT `fk_ref_default_audio_track_phase` FOREIGN KEY (`phase`) REFERENCES `ref_default_audio_track_phase` (`libelle`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ref_default_audio_track_service` FOREIGN KEY (`service`) REFERENCES `ref_default_audio_track_service` (`service_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_default_audio_track_phase`
--

DROP TABLE IF EXISTS `ref_default_audio_track_phase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_default_audio_track_phase` (
  `libelle` varchar(45) NOT NULL,
  PRIMARY KEY (`libelle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_default_audio_track_service`
--

DROP TABLE IF EXISTS `ref_default_audio_track_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_default_audio_track_service` (
  `service_name` varchar(45) NOT NULL,
  PRIMARY KEY (`service_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_phase`
--

DROP TABLE IF EXISTS `ref_phase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_phase` (
  `code_phase` varchar(5) NOT NULL DEFAULT '',
  `lib_phase` varchar(45) DEFAULT NULL,
  `id_channel` int(11) NOT NULL DEFAULT '0',
  `max_track` int(11) DEFAULT NULL,
  `id_template_defaut` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`code_phase`),
  KEY `fk_ref_phase_ref_channel` (`id_channel`),
  KEY `fk_ref_phase_template` (`id_template_defaut`),
  CONSTRAINT `fk_ref_phase_ref_channel` FOREIGN KEY (`id_channel`) REFERENCES `ref_channel` (`id_channel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ref_phase_template` FOREIGN KEY (`id_template_defaut`) REFERENCES `template` (`id_template`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_priority`
--

DROP TABLE IF EXISTS `ref_priority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_priority` (
  `id_priority` int(11) NOT NULL AUTO_INCREMENT,
  `lib_priority` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_priority`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ref_template_item`
--

DROP TABLE IF EXISTS `ref_template_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_template_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID de l''�l�ment. Les �l�ments ressortiront dans l''ordre des ID.',
  `keyword` varchar(45) COLLATE latin1_general_ci NOT NULL COMMENT 'Mot cl� utilis� dans les templates',
  `infoadd_id` int(11) DEFAULT NULL COMMENT 'Id de l''info add',
  `description` text COLLATE latin1_general_ci NOT NULL COMMENT 'Description pour affichage sur l''�cran d''�dition des mod�les',
  `content_sent` int(11) DEFAULT NULL COMMENT 'Valeur du content_sent correspondant (� des fins statitisques)',
  `priorisable` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Priorit� par d�faut : sera surtout utilis�e pour le num�ro et l''adresse pour ne pas �tre supprimables.',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Actif ?',
  `example` varchar(255) COLLATE latin1_general_ci DEFAULT NULL COMMENT 'Donn�es d''exemple pour utilisation avec le simulateur',
  PRIMARY KEY (`id`),
  KEY `ref_template_item_keyword` (`keyword`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `service_du` varchar(10) NOT NULL DEFAULT '',
  `service_description` varchar(50) NOT NULL DEFAULT '',
  `service_user` varchar(20) NOT NULL DEFAULT '',
  `operateur` varchar(10) DEFAULT '',
  PRIMARY KEY (`service_du`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service_type`
--

DROP TABLE IF EXISTS `service_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_type` (
  `service_type_id` int(11) NOT NULL DEFAULT '0',
  `service_type_description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sms_content_rules`
--

DROP TABLE IF EXISTS `sms_content_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_content_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(145) COLLATE latin1_general_ci DEFAULT NULL,
  `id_template_item` int(11) NOT NULL,
  `search_value` varchar(145) COLLATE latin1_general_ci DEFAULT NULL,
  `replace_value` varchar(145) COLLATE latin1_general_ci DEFAULT NULL,
  `action` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `position` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci,
  `rule_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_content_name` (`name`),
  KEY `fk_sms_content_rules_id_template_item_constraint` (`id_template_item`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stop`
--

DROP TABLE IF EXISTS `stop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stop` (
  `stop_recipient` varchar(100) NOT NULL DEFAULT '',
  `du` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`stop_recipient`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template` (
  `format_id` varchar(20) NOT NULL DEFAULT '',
  `template_type` int(11) NOT NULL DEFAULT '0',
  `template_template` varchar(255) NOT NULL DEFAULT '',
  `template_size` int(11) NOT NULL DEFAULT '0',
  `id_template` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(150) DEFAULT NULL,
  `template_content_id` varchar(100) DEFAULT 'MessageDefaut',
  `code_phase` varchar(5) NOT NULL DEFAULT 'ENVOI',
  `access` int(11) DEFAULT '1',
  `display` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_template`),
  KEY `template_FKIndex2` (`format_id`),
  KEY `fk_template_type` (`template_type`),
  KEY `fk_template_ref_phase` (`code_phase`),
  KEY `fk_template_message_service` (`template_content_id`),
  CONSTRAINT `fk_template_message` FOREIGN KEY (`template_content_id`) REFERENCES `template_content` (`template_content_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_template_ref_phase` FOREIGN KEY (`code_phase`) REFERENCES `ref_phase` (`code_phase`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_template_type` FOREIGN KEY (`template_type`) REFERENCES `template_type` (`template_type_id`),
  CONSTRAINT `template_ibfk_2` FOREIGN KEY (`format_id`) REFERENCES `format` (`format_id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `template_content`
--

DROP TABLE IF EXISTS `template_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_content` (
  `template_content_id` varchar(45) NOT NULL DEFAULT '',
  `template_content_txt` varchar(160) DEFAULT NULL,
  `id_channel` int(11) NOT NULL DEFAULT '1',
  `template_content_name` varchar(145) DEFAULT NULL,
  `template_content_short_txt` varchar(145) DEFAULT NULL,
  `id_contract_customer` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`template_content_id`),
  KEY `fk_local_ref_channel` (`id_channel`),
  KEY `fk_contract_customer` (`id_contract_customer`),
  CONSTRAINT `fk_contract_customer` FOREIGN KEY (`id_contract_customer`) REFERENCES `contract_customer` (`id_contract_customer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_local_ref_channel` FOREIGN KEY (`id_channel`) REFERENCES `ref_channel` (`id_channel`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `template_type`
--

DROP TABLE IF EXISTS `template_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_type` (
  `template_type_id` int(11) NOT NULL DEFAULT '0',
  `template_type_description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`template_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-29 15:57:29
