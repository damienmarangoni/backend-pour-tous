package orange.omn.services.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import orange.omn.Application;
import orange.omn.bean.Mask;
import orange.omn.services.interfaces.IMaskService;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
public class MaskServiceTest {

    @Autowired
    private IMaskService maskService;



    @Test
    public void getByNbPublic() {
        
        final Mask mask = maskService
                .getMaskByNbPublic("0157323827");

        assertNotNull(mask);
        assertEquals("Expedia - Service de réservation Hôtels", mask.getMaskDescription());

    }    

}
