package orange.omn.services.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import orange.omn.Application;
import orange.omn.bean.Template;
import orange.omn.services.interfaces.ITemplateService;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
public class TemplateServiceTest {

    @Autowired
    private ITemplateService templateService;


    @Test
    public void getTemplateByPhase() {
        
        Pageable pageable = null ;

        final Page<Template> templates = templateService
                .getTemplate("codePhase:ENVOI", pageable);

        assertNotNull(templates);
        assertEquals(9, templates.getContent().size());
        assertEquals("Template par defaut", templates.getContent().get(0).getTemplateName());
        assertEquals("Auto-promo sms 118712", templates.getContent().get(8).getTemplateName());

    }


    @Test
    public void getTemplateByName() {
        
        final Template template = templateService
                .getTemplateByIdTemplate(7);

        assertNotNull(template);
        //assertEquals("Template SMS QUERY 118 712 par défaut", template.getTemplateName());

    }    

}
