package orange.omn.services.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import orange.omn.Application;
import orange.omn.bean.CustomerQualification;
import orange.omn.services.interfaces.ICustomerRouteService;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
public class CustomerRouteServiceTest {
    
    @Autowired
    private ICustomerRouteService CustomerRouteService;


    @Test
    public void getCustomerRoutesForSimulator1() {
        
        Pageable pageable = null ;

        String[] message= {"25", "Toutes catégories", "1"};
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String text = date.format(formatters);
        LocalDate parsedDate = LocalDate.parse(text, formatters);
        
        final Page<CustomerQualification> CustomerRoutes = CustomerRouteService.getCustomerRoutesForSimulator("ACC", parsedDate, message, pageable);

        assertNotNull(CustomerRoutes);
        assertEquals(3, CustomerRoutes.getContent().size());
        assertEquals("Parcours 118712", CustomerRoutes.getContent().get(0).getCustomerQualificationName());
        assertEquals("Service international", CustomerRoutes.getContent().get(1).getCustomerQualificationName());
        assertEquals("Service client", CustomerRoutes.getContent().get(2).getCustomerQualificationName());
    }  


    @Test
    public void getCustomerRoutesForSimulator2() {
        
        Pageable pageable = null ;

        String[] message= {"25", "Toutes catégories", "1"};
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String text = date.format(formatters);
        LocalDate parsedDate = LocalDate.parse(text, formatters);
        
        final Page<CustomerQualification> CustomerRoutes = CustomerRouteService.getCustomerRoutesForSimulator("TARIF", parsedDate, message, pageable);

        assertNotNull(CustomerRoutes);
        assertEquals(3, CustomerRoutes.getContent().size());
        assertEquals("Parcours 118712", CustomerRoutes.getContent().get(0).getCustomerQualificationName());
        assertEquals("Service international", CustomerRoutes.getContent().get(1).getCustomerQualificationName());
        assertEquals("Service client", CustomerRoutes.getContent().get(2).getCustomerQualificationName());
    } 


    @Test
    public void getCustomerRoutesForSimulator3() {
        
        Pageable pageable = null ;

        String[] message= {"31", "Toutes catégories", "0"};
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String text = date.format(formatters);
        LocalDate parsedDate = LocalDate.parse(text, formatters);
        
        final Page<CustomerQualification> CustomerRoutes = CustomerRouteService.getCustomerRoutesForSimulator("ACC", parsedDate, message, pageable);

        assertNotNull(CustomerRoutes);
        assertEquals(1, CustomerRoutes.getContent().size());
        assertEquals("Parcours SMS query 118 712 par défaut", CustomerRoutes.getContent().get(0).getCustomerQualificationName());
    } 


    @Test
    public void getCustomerRoutesForSimulator4() {
        
        Pageable pageable = null ;

        String[] message= {"31", "Toutes catégories", "1"};
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String text = date.format(formatters);
        LocalDate parsedDate = LocalDate.parse(text, formatters);
        
        final Page<CustomerQualification> CustomerRoutes = CustomerRouteService.getCustomerRoutesForSimulator("ACC", parsedDate, message, pageable);

        assertNotNull(CustomerRoutes);
        assertEquals(1, CustomerRoutes.getContent().size());
        assertEquals("Parcours SMS query 118 712 par défaut", CustomerRoutes.getContent().get(0).getCustomerQualificationName());
    } 


    @Test
    public void getCustomerRoutesForSimulator5() {
        
        Pageable pageable = null ;

        String[] message= {"70", "Toutes catégories", "0"};
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String text = date.format(formatters);
        LocalDate parsedDate = LocalDate.parse(text, formatters);
        
        final Page<CustomerQualification> CustomerRoutes = CustomerRouteService.getCustomerRoutesForSimulator("ACC", parsedDate, message, pageable);

        assertNotNull(CustomerRoutes);
        assertEquals(2, CustomerRoutes.getContent().size());
        assertEquals("Parcours 118712 Orange Fixes", CustomerRoutes.getContent().get(0).getCustomerQualificationName());
        assertEquals("Parcours 118712", CustomerRoutes.getContent().get(1).getCustomerQualificationName());
    } 
}
