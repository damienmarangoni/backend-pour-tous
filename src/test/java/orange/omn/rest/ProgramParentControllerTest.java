package orange.omn.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import orange.omn.services.interfaces.IProgramParentService;
import orange.omn.web.dto.CampaignDTO;
import orange.omn.web.dto.LocationDTO;
import orange.omn.web.dto.ProgramDTO;
import orange.omn.web.dto.ProgramParentDTO;
import orange.omn.web.dto.RefPriorityDTO;
import orange.omn.web.dto.TemplateContentDTO;
import orange.omn.web.dto.TemplateDTO;
import orange.omn.web.mapper.IProgramParentMapper;

@SpringBootTest
@WebMvcTest(ProgramParentController.class)
public class ProgramParentControllerTest {
	
	private MockMvc mockMvc;

	@InjectMocks
	private ProgramParentController programParentController;

	@Mock
	private IProgramParentMapper programParentMapper;

	@Mock
	private IProgramParentService programParentService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(programParentController).build();
	}
	
	@Test
	public void createProgramParent() throws Exception {
		
		ProgramParentDTO ppDTO1 = new ProgramParentDTO(); 

		ppDTO1.setAccess(0);
		ppDTO1.setCampaign(new CampaignDTO());
		LocalDate dateEnd = null;
        ppDTO1.setDateEnd(dateEnd);
		LocalDate dateStart = null;
        ppDTO1.setDateStart(dateStart);
		//ppDTO1.setIdProgramParent(idProgramParent);
		ppDTO1.setLocation(new LocationDTO());
		RefPriorityDTO refPriorityDTO = new RefPriorityDTO();
		refPriorityDTO.setIdPriority(5);
		refPriorityDTO.setLibPriority("Normal");
        ppDTO1.setRefPriority(refPriorityDTO);
		ppDTO1.setPriority(5);
		List<ProgramDTO> programDTOs = new ArrayList<ProgramDTO>();
		ppDTO1.setPrograms(programDTOs);
		ppDTO1.setTemplate(new TemplateDTO());
		ppDTO1.setTemplateContent(new TemplateContentDTO());
		ppDTO1.setVEvent(null);
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		mockMvc.perform(post("/createProgParent/{id}", 1).contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(ppDTO1))).andExpect(status().isCreated());
		
	}
	
	   @Test
	    public void modifyProgramParent() throws Exception {
	        
	        ProgramParentDTO ppDTO1 = new ProgramParentDTO(); 

	        ppDTO1.setAccess(0);
	        ppDTO1.setCampaign(new CampaignDTO());
	        LocalDate dateEnd = null;
	        ppDTO1.setDateEnd(dateEnd);
	        LocalDate dateStart = null;
	        ppDTO1.setDateStart(dateStart);
	        ppDTO1.setIdProgramParent(1);
	        ppDTO1.setLocation(new LocationDTO());
	        RefPriorityDTO refPriorityDTO = new RefPriorityDTO();
	        refPriorityDTO.setIdPriority(5);
	        refPriorityDTO.setLibPriority("Normal");
	        ppDTO1.setRefPriority(refPriorityDTO);
	        ppDTO1.setPriority(5);
	        List<ProgramDTO> programDTOs = new ArrayList<ProgramDTO>();
	        ppDTO1.setPrograms(programDTOs);
	        ppDTO1.setTemplate(new TemplateDTO());
	        ppDTO1.setTemplateContent(new TemplateContentDTO());
	        ppDTO1.setVEvent(null);
	        
	        ObjectMapper objectMapper = new ObjectMapper();
	        
	        mockMvc.perform(post("/modifyProgParent/{id}", 1).contentType(MediaType.APPLICATION_JSON)
	                .content(objectMapper.writeValueAsString(ppDTO1))).andExpect(status().isOk());
	        
	    }
    
       @Test
        public void deleteProgramParent() throws Exception {
                       
            mockMvc.perform(delete("/deleteProgParent/{id}", 1)).andExpect(status().isOk());
        }


}
