package orange.omn.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import orange.omn.services.interfaces.ICustomerRouteService;
import orange.omn.web.dto.CustomerRouteDTO;
import orange.omn.web.dto.PartCustomerRouteDTO;
import orange.omn.web.dto.ProgramParentDTO;
import orange.omn.web.dto.QualificationExpressionDTO;
import orange.omn.web.mapper.ICustomerQualificationMapper;
import orange.omn.web.mapper.IPartCustomerQualificationMapper;

@SpringBootTest
@WebMvcTest({CustomerRouteController.class})

public class CustomerRouteControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	private CustomerRouteController custRouteController;

	@Mock
	private ICustomerQualificationMapper custQualMapper;

	@Mock
	private IPartCustomerQualificationMapper patyCustQualMapper;

	@Mock
	private ICustomerRouteService custRouteService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(custRouteController).build();
	}
	
	
	@Test
	public void putFullCustRoute() throws Exception {
		
		ObjectMapper objectMapper = new ObjectMapper();


		CustomerRouteDTO custRouteDTO = new CustomerRouteDTO("Parcours de test", 1);
		
		LocalDate ahora = LocalDate.now();
		ProgramParentDTO pp1 = new ProgramParentDTO();

		List<ProgramParentDTO> programParentsDTOs = new ArrayList<ProgramParentDTO>();
		List<QualificationExpressionDTO> qualificationExpressionDTOs = new ArrayList<QualificationExpressionDTO>();

		pp1.setDateStart(ahora);
		pp1.setDateEnd(ahora.plus(1, ChronoUnit.DECADES));

		pp1.setAccess(1);
		programParentsDTOs.add(pp1);

		custRouteDTO.setProgramParentDTOs(programParentsDTOs);
		custRouteDTO.setQualificationExpressionsDTOs(qualificationExpressionDTOs);

		

		mockMvc.perform(put("/updateRouteAll/{id}", custRouteDTO.getCustomerQualificationId())
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(custRouteDTO)).content(objectMapper.writeValueAsString(pp1)))
				.andExpect(status().isOk());

	}

	@Test
	public void patchPartCustRoute() throws Exception {

		PartCustomerRouteDTO partCustRouteDto = new PartCustomerRouteDTO("Parcours de test", 1);

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(patch("/updateRoute/{id}", partCustRouteDto.getCustomerQualificationId())
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(partCustRouteDto))).andExpect(status().isOk());
	}

	@Test
	public void postFullCustRoute() throws Exception {

		CustomerRouteDTO custRouteDTO = new CustomerRouteDTO("Parcours de test", 1);

		LocalDate ahora = LocalDate.now();
		ProgramParentDTO pp1 = new ProgramParentDTO();

		List<ProgramParentDTO> programParentsDTOs = new ArrayList<ProgramParentDTO>();
		List<QualificationExpressionDTO> qualificationExpressionDTOs = new ArrayList<QualificationExpressionDTO>();

		pp1.setDateStart(ahora);
		pp1.setDateEnd(ahora.plus(1, ChronoUnit.DECADES));

		pp1.setAccess(1);
		programParentsDTOs.add(pp1);

		custRouteDTO.setProgramParentDTOs(programParentsDTOs);
		custRouteDTO.setQualificationExpressionsDTOs(qualificationExpressionDTOs);

		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(post("/customerRoute").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(custRouteDTO)).content(objectMapper.writeValueAsString(pp1))).andExpect(status().isCreated());

	}

}
