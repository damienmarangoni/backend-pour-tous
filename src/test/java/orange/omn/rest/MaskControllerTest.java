package orange.omn.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import orange.omn.services.interfaces.IMaskService;
import orange.omn.web.dto.MaskDTO;
import orange.omn.web.mapper.IMaskMapper;

@SpringBootTest
@WebMvcTest(MaskController.class)
public class MaskControllerTest {
	
	private MockMvc mockMvc;

	@InjectMocks
	private MaskController maskController;

	@Mock
	private IMaskMapper maskMapper;

	@Mock
	private IMaskService maskService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(maskController).build();
	}
	
	@Test
	public void createMask() throws Exception {
		
		MaskDTO mDTO1 = new MaskDTO(); 

		mDTO1.setMaskDescription("Test unitaire");
		mDTO1.setMaskMaskedNb("0557290020");
		mDTO1.setMaskMessage("massage de mon test");
		mDTO1.setMaskPublicNb("0557290020");
		
				
		ObjectMapper objectMapper = new ObjectMapper();
		
		mockMvc.perform(post("/createMask/{id}", 1).contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(mDTO1))).andExpect(status().isCreated());
		
	}
	
	   @Test
	    public void modifyMask() throws Exception {
	        
	        MaskDTO mDTO1 = new MaskDTO(); 
	        
	        mDTO1.setMaskDescription("Test unitaire");
			mDTO1.setMaskMaskedNb("0557290020");
			mDTO1.setMaskMessage("massage de mon test");
			mDTO1.setMaskPublicNb("0557290020");
			
	        
	        ObjectMapper objectMapper = new ObjectMapper();
	        
	        mockMvc.perform(post("/modifyMask/{id}", 1).contentType(MediaType.APPLICATION_JSON)
	                .content(objectMapper.writeValueAsString(mDTO1))).andExpect(status().isOk());
	        
	    }
    
       @Test
        public void deleteMask() throws Exception {
    	   // on cre le mask
	        MaskDTO mDTO1 = new MaskDTO(); 
	        
	        mDTO1.setMaskDescription("Numéro à supprimer");
			mDTO1.setMaskMaskedNb("0557290020");
			mDTO1.setMaskMessage("massage de mon test de destruction");
			mDTO1.setMaskPublicNb("1187121010");
			ObjectMapper objectMapper = new ObjectMapper();
			
			mockMvc.perform(post("/createMask", 1).contentType(MediaType.APPLICATION_JSON)
					.content(objectMapper.writeValueAsString(mDTO1))).andExpect(status().isCreated());
    	   
           
			// Et suppression de celui-ci
            mockMvc.perform(delete("/deleteMask/1187121010", 1)).andExpect(status().isOk());
        }


}
