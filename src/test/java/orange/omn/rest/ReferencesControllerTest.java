package orange.omn.rest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import orange.omn.bean.RefCommunity;
import orange.omn.bean.RefPriority;
import orange.omn.bean.RefTemplateItem;
import orange.omn.bean.Service;
import orange.omn.services.interfaces.IReferencesService;
import orange.omn.web.mapper.IReferencesMapper;

@SpringBootTest
@WebMvcTest({ReferencesController.class, IReferencesMapper.class, IReferencesService.class})
public class ReferencesControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	private ReferencesController refPriorityController;

	@Mock
	private IReferencesMapper refPriorityMapper;
	
	@InjectMocks
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Mock
	private IReferencesService referenceService;
	
	

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
//		mockMvc = MockMvcBuilders.standaloneSetup(refPriorityController).build();
//		mockMvc = MockMvcBuilders.standaloneSetup(refPriorityController).setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build();
		mockMvc = MockMvcBuilders.standaloneSetup(refPriorityController).setCustomArgumentResolvers(pageableArgumentResolver).build();
		
	}

	@Test
	public void getRefPriorities() throws Exception {

		List<RefPriority> rFListBean = Arrays.asList(new RefPriority(1,"Normal"), new RefPriority(2,"Haut"));
		assertNotNull(rFListBean);
		assertFalse(rFListBean.isEmpty());
		
		Page<RefPriority> expectedPage = new PageImpl<RefPriority>(rFListBean);
		
		when(referenceService.getRefPriority(any(String.class), any(Pageable.class))).thenReturn(expectedPage);
		
		mockMvc.perform(get("/refPriority").contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
	}
	
	@Test
	public void getRefCommunities() throws Exception {
		
		List<RefCommunity> communitiesListBean = Arrays.asList(new RefCommunity(0, "Toutes communautés"), new RefCommunity(1, "Mouvango"));
		
		assertNotNull(communitiesListBean);
		assertFalse(communitiesListBean.isEmpty());
		
		Page<RefCommunity> expectedPage = new PageImpl<RefCommunity>(communitiesListBean);
		
		when(referenceService.getRefCommunity(any(String.class), any(Pageable.class))).thenReturn(expectedPage);
		
		mockMvc.perform(get("/refCommunity").contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
		
		
	}
	
	@Test
	public void getServicies() throws Exception {
		
		List<Service> servicesListBean = Arrays.asList(new Service("0","Tous réseaux"), new Service("1","118 710")); 
		assertNotNull(servicesListBean);
		assertFalse(servicesListBean.isEmpty());
		
		when(referenceService.getServicesList()).thenReturn(servicesListBean);
		
		mockMvc.perform(get("/services").contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

		
	}
	
	@Test
	public void getRefTemplateItems() throws Exception {
		
		List<RefTemplateItem> rtiListBean = Arrays.asList(new RefTemplateItem(0,"Tous réseaux"), new RefTemplateItem(1,"118 710")); 
		assertNotNull(rtiListBean);
		assertFalse(rtiListBean.isEmpty());
		
		when(referenceService.getRefTemplateItemList()).thenReturn(rtiListBean);
		
		mockMvc.perform(get("/refTemplateItem").contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
		
	}


}
