package orange.omn.rest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import orange.omn.bean.ContractCustomer;
import orange.omn.bean.TemplateContent;
import orange.omn.services.interfaces.IContractCustomerService;
import orange.omn.services.interfaces.ITemplateContentService;
import orange.omn.web.mapper.ITemplateContentMapper;

@SpringBootTest
@WebMvcTest({ TemplateContentController.class, ITemplateContentMapper.class, ITemplateContentService.class })
public class TemplateContentControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	private TemplateContentController templateContentController;

	@Mock
	private ITemplateContentMapper refPriorityMapper;

	@InjectMocks
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Mock
	private ITemplateContentService templateContentService;
	
	@Mock
	private IContractCustomerService constractCustomerService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(templateContentController)
				.setCustomArgumentResolvers(pageableArgumentResolver).build();

	}

	@Test
	public void getTemplateContents() throws Exception {

		List<TemplateContent> cTListBean = Arrays.asList(
				new TemplateContent("118 710 Heure d'été", "118 710 Heure d'été",
						"118 710 vous donne l'heure d'été (34ct/mn)"),
				new TemplateContent("118 710 Météo", "118 710 Météo WE", "La météo du week-end? 118 710 (34cts/mn)"));
		assertNotNull(cTListBean);
		assertFalse(cTListBean.isEmpty());

		Page<TemplateContent> expectedPage = new PageImpl<TemplateContent>(cTListBean);

		when(templateContentService.getTemplateContent(any(String.class), any(Pageable.class)))
				.thenReturn(expectedPage);

		mockMvc.perform(get("/templateContent").contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}

	@Test
	public void deleteTemplateContent() throws Exception {

		TemplateContent templateContent = new TemplateContent("118 710 Heure d'été", "118 710 Heure d'été",
				"118 710 vous donne l'heure d'été (34ct/mn)");
		when(templateContentService.getWrittenTemplateContentText()).thenReturn(Arrays.asList(templateContent));

		mockMvc.perform(delete("/deleteTemplateContent/{id}", templateContent.getTemplateContentId()))
				.andExpect(status().isOk());

	}

	@Test
	public void updateTemplateContent() throws Exception {
		TemplateContent templateContent = new TemplateContent("118 710 Heure d'été", "118 710 Heure d'été",
				"118 710 vous donne l'heure d'été (34ct/mn)");
		when(templateContentService.getWrittenTemplateContentText()).thenReturn(Arrays.asList(templateContent));
		doNothing().when(templateContentService).updateTemplateContent(templateContent,
				templateContent.getTemplateContentId());

		mockMvc.perform(put("/updateTemplateContent/{id}", templateContent.getTemplateContentId())
				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(templateContent)))
				.andExpect(status().isOk());

	}
	
	@Test
	public void createTemplateContent() throws Exception {
		TemplateContent templateContent = new TemplateContent("118 710 Heure d'été", "118 710 Heure d'été",
				"118 710 vous donne l'heure d'été (34ct/mn)");
		when(templateContentService.exists(templateContent)).thenReturn(false);
		doNothing().when(templateContentService).createTemplateContent(templateContent);
		
		mockMvc.perform(
	            post("/createTemplateContent")
	                    .contentType(MediaType.APPLICATION_JSON)
	                    .content(asJsonString(templateContent)))
	            .andExpect(status().isCreated());
	}
	
	@Test
	public void getContractCustomerLabels() throws Exception {
		
		List<ContractCustomer> contractCustomerBeanList = Arrays.asList(new ContractCustomer(1, "118712"), new ContractCustomer(2, "118218"));
		assertNotNull(contractCustomerBeanList);
		assertFalse(contractCustomerBeanList.isEmpty());
		when(constractCustomerService.getContractCustomerList()).thenReturn(contractCustomerBeanList);
		
		mockMvc.perform(get("/contractCustomerLabels").contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
		
		
	}

	/*
	 * converts a Java object into JSON representation
	 */
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
