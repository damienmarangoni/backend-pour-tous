package orange.omn.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import orange.omn.services.interfaces.IQualificationExpressionService;
import orange.omn.web.dto.QualificationExpressionDTO;
import orange.omn.web.dto.RefCategoryDTO;
import orange.omn.web.dto.ServiceDTO;
import orange.omn.web.mapper.IQualificationExpressionMapper;

@SpringBootTest
@WebMvcTest(QualificationExpressionController.class)
public class QualificationExpressionControllerTest {
	
	private MockMvc mockMvc;

	@InjectMocks
	private QualificationExpressionController qualifExpressController;

	@Mock
	private IQualificationExpressionMapper qualifExpressMapper;

	@Mock
	private IQualificationExpressionService qualifExpressService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(qualifExpressController).build();
	}
	
	@Test
	public void postQualifExpressList() throws Exception {
		
		List<QualificationExpressionDTO> qualificationExpressionDTOs = new ArrayList<QualificationExpressionDTO>();
		QualificationExpressionDTO qEDTO1 = new QualificationExpressionDTO(); 
		QualificationExpressionDTO qEDTO2 = new QualificationExpressionDTO();
		QualificationExpressionDTO qEDTO3 = new QualificationExpressionDTO();
		
		ServiceDTO service = new ServiceDTO();
		RefCategoryDTO refCategory = new RefCategoryDTO();

		qEDTO1.setCommunityFilter(32);
		qEDTO1.setIdCustomerQualification(1);
		qEDTO1.setCommunityResponse(13);
		qEDTO1.setService(service);
		qEDTO1.setRefCategory(refCategory);
		
		qEDTO2.setCommunityFilter(25);
		qEDTO2.setIdCustomerQualification(1);
		qEDTO2.setCommunityResponse(16);
		qEDTO2.setService(service);
		qEDTO2.setRefCategory(refCategory);
		
		qEDTO3.setCommunityFilter(10);
		qEDTO3.setIdCustomerQualification(1);
		qEDTO3.setCommunityResponse(15);
		qEDTO3.setService(service);
		qEDTO3.setRefCategory(refCategory);
		
		qualificationExpressionDTOs.add(qEDTO1);
		qualificationExpressionDTOs.add(qEDTO2);
		qualificationExpressionDTOs.add(qEDTO3);
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		mockMvc.perform(post("/createQualExpr/{id}", 1).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(qualificationExpressionDTOs))).andExpect(status().isCreated());
		
	}


}
