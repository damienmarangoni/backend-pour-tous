package orange.omn.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import orange.omn.Application;
import orange.omn.bean.CustomerQualification;

//@DbUnitConfiguration(dataSetLoader = FlatXmlDataSetLoader.class)

//@RunWith(SpringJUnit4ClassRunner.class)
//@DatabaseSetup("/dbunitXMLFiles/pomniws.xml")
//@DatabaseTearDown("/dbunitXMLFiles/pomniws.xml")
//@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class,DirtiesContextTestExecutionListener.class,
//		DbUnitTestExecutionListener.class })

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
public class CustomerRouteRepositoryTest {

	@Autowired
	private ICustomerRouteRepository custRouteRepo;


	@Test
	public void findByCustomerQualificationName() {

		final CustomerQualification custQual = custRouteRepo
				.findByCustomerQualificationName("Parcours numero noir");

		assertNotNull(custQual);
		assertEquals(2, custQual.getCustomerQualificationId());

	}

	@Test
	public void findByCustomerQualificationId() {
		final CustomerQualification custQual = custRouteRepo.findByCustomerQualificationId(1);

		assertNotNull(custQual);
		assertEquals("Parcours par defaut", custQual.getCustomerQualificationName());

	}

	@Test
	public void retrieveByStartDate() {

		LocalDate startDate = LocalDate.of(2014, 07, 21);
		Pageable pageable = null;
		final Page<CustomerQualification> custQualPage = custRouteRepo.retrieveByStartDate(startDate, pageable);
		assertNotNull(custQualPage);

	}

    @Test
    public void retrieveByStartDateAndEndDate1() {
        LocalDate startDate = LocalDate.of(2000, 07, 21);
        LocalDate endDate = LocalDate.of(2008, 07, 21);
        Pageable pageable = null;

        List<CustomerQualification> custQualPage = new ArrayList<CustomerQualification>();  
     
        custQualPage.addAll(custRouteRepo.retrieveByStartDateAndEndDate(startDate, endDate, pageable).getContent());
        custQualPage.addAll(custRouteRepo.retrieve(pageable).getContent());
     
        Page<CustomerQualification> page = new PageImpl<CustomerQualification>(custQualPage);
    
        assertNotNull(page);
        assertEquals(6, page.getContent().size());
    }

	@Test
	public void retrieveByStartDateAndEndDate2() {
		LocalDate startDate = LocalDate.of(2000, 07, 21);
		LocalDate endDate = LocalDate.of(2030, 07, 21);
		Pageable pageable = null;

	    List<CustomerQualification> custQualPage = new ArrayList<CustomerQualification>();	
     
        custQualPage.addAll(custRouteRepo.retrieveByStartDateAndEndDate(startDate, endDate, pageable).getContent());
        custQualPage.addAll(custRouteRepo.retrieve(pageable).getContent());
     
        Page<CustomerQualification> page = new PageImpl<CustomerQualification>(custQualPage);
    
	    assertNotNull(page);
        assertEquals(11, page.getContent().size());
	}

    @Test
    public void retrieveByStartDateAndEndDate3() {
        LocalDate startDate = LocalDate.of(2009, 07, 21);
        LocalDate endDate = LocalDate.of(2030, 07, 21);
        Pageable pageable = null;

        List<CustomerQualification> custQualPage = new ArrayList<CustomerQualification>();  
     
        custQualPage.addAll(custRouteRepo.retrieveByStartDateAndEndDate(startDate, endDate, pageable).getContent());
        custQualPage.addAll(custRouteRepo.retrieve(pageable).getContent());
     
        Page<CustomerQualification> page = new PageImpl<CustomerQualification>(custQualPage);
    
        assertNotNull(page);
        assertEquals(11, page.getContent().size());
    }

    @Test
    public void retrieve() {
        Pageable pageable = null;

        final Page<CustomerQualification> custQualPage = custRouteRepo.retrieve(pageable);
        assertNotNull(custQualPage);
        assertEquals(2, custQualPage.getContent().size());
        assertEquals("Parcours client test spec", custQualPage.getContent().get(0).getCustomerQualificationName());
        assertEquals("Parcours 118712 Orange Fixes", custQualPage.getContent().get(1).getCustomerQualificationName());
    }

//    @Test
//    public void retrieveAll() {
//        Pageable pageable = null;
//
//        final Page<CustomerQualification> custQualPage = custRouteRepo.retrieve(pageable);
//        
//        assertNotNull(custQualPage);
//        assertEquals(2, custQualPage.getContent().size());
//        assertEquals("Parcours client test spec", custQualPage.getContent().get(0).getCustomerQualificationName());
//        assertEquals("Parcours 118712 Orange Fixes", custQualPage.getContent().get(1).getCustomerQualificationName());
//    }

    @Test
    public void retrieveForSimulator() {
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String text = date.format(formatters);
        LocalDate parsedDate = LocalDate.parse(text, formatters);
        Pageable pageable = null;

        Page<CustomerQualification> custQualPage = custRouteRepo.retrieveForSimulator("ACC", parsedDate, "25", "Toutes catégories", 0, pageable);
        assertNotNull(custQualPage);
        assertEquals(1, custQualPage.getContent().size());
        assertEquals("Parcours 118712", custQualPage.getContent().get(0).getCustomerQualificationName());

        custQualPage = custRouteRepo.retrieveForSimulator("ACC", parsedDate, "25", "%", 0, pageable);
        assertNotNull(custQualPage);
        assertEquals(1, custQualPage.getContent().size());
        assertEquals("Parcours 118712", custQualPage.getContent().get(0).getCustomerQualificationName());

        custQualPage = custRouteRepo.retrieveForSimulator("ACC", parsedDate, "%", "Toutes catégories", 0, pageable);
        assertNotNull(custQualPage);
        assertEquals(2, custQualPage.getContent().size());
        assertEquals("Parcours 118712", custQualPage.getContent().get(1).getCustomerQualificationName());

        custQualPage = custRouteRepo.retrieveForSimulator("ACC", parsedDate, "25", "Toutes catégories", pageable);
        assertNotNull(custQualPage);
        assertEquals(3, custQualPage.getContent().size());
        assertEquals("Parcours 118712", custQualPage.getContent().get(0).getCustomerQualificationName());

        custQualPage = custRouteRepo.retrieveForSimulator("ACC", parsedDate, "25", "%", pageable);
        assertNotNull(custQualPage);
        assertEquals(4, custQualPage.getContent().size());
        assertEquals("Parcours 118712", custQualPage.getContent().get(0).getCustomerQualificationName());
    }


    @Test
    public void retrieveForSimulator2() {
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String text = date.format(formatters);
        LocalDate parsedDate = LocalDate.parse(text, formatters);
        Pageable pageable = null;

        Page<CustomerQualification> custQualPage = custRouteRepo.retrieveDefaultForSimulator("70", "Toutes catégories", 0, pageable);
        assertNotNull(custQualPage);
        assertEquals(2, custQualPage.getContent().size());
        assertEquals("Parcours 118712 Orange Fixes", custQualPage.getContent().get(0).getCustomerQualificationName());
        assertEquals("Parcours 118712", custQualPage.getContent().get(1).getCustomerQualificationName());
    }

}
