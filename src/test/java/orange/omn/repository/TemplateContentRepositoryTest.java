package orange.omn.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import orange.omn.Application;
import orange.omn.bean.TemplateContent;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { Application.class })
public class TemplateContentRepositoryTest {

	@Autowired
	private ITemplateContentRepository templateContentRepo;

	@Test
	public void findByTemplateContentId() {
		final TemplateContent templateContent = templateContentRepo.findByTemplateContentId("La course aux coffres");

		assertNotNull(templateContent);
		assertEquals("La course aux coffres", templateContent.getTemplateContentId());

	}

	@Test
	@Transactional
	public void deleteWrittenMessage() {

		final TemplateContent templateContent = templateContentRepo.findByTemplateContentId("La course aux coffres");

		assertNotNull(templateContent);

		templateContentRepo.deleteByTemplateContentId("La course aux coffres");

		// templateContentRepo.deleteAll();

		assertNull(templateContentRepo.findByTemplateContentId("La course aux coffres"));
	}

}
