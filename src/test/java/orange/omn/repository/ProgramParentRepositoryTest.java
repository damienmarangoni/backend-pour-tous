package orange.omn.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import orange.omn.Application;
import orange.omn.bean.ProgramParent;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
public class ProgramParentRepositoryTest {

	@Autowired
	private IProgramParentRepository programParentRepo;


	@Test
	public void retrieveByCodePhase() {
	    
	    Pageable pageable = null ;

		final Page<ProgramParent> progParent = programParentRepo
				.retrieveByCodePhase("ACC", 23, pageable);

		assertNotNull(progParent);
		assertEquals(1, progParent.getContent().size());

	}
	
    @Test
    public void retrieveByCodePhaseAndId() {

        final ProgramParent progParent = programParentRepo
                .retrieveByCodePhaseAndId("ACC", 23);

        assertNotNull(progParent);
        assertEquals(759, progParent.getIdProgramParent());

    }
    
    @Test
    public void retrieveById() {

        final ProgramParent progParent = programParentRepo
                .retrieveById(300);

        assertNotNull(progParent);
        assertEquals(0, progParent.getAccess());

    }
    
    @Test
    public void retrieveByCustomerQualifId() {
        
        Pageable pageable = null ;

        final Page<ProgramParent> progParent = programParentRepo
                .retrieveByCustomerQualifId(4, pageable);

        assertNotNull(progParent);
        assertEquals(4, progParent.getContent().size());

    }

}
