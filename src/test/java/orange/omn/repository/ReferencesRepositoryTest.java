package orange.omn.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import orange.omn.Application;
import orange.omn.bean.RefPriority;
import orange.omn.bean.RefTemplateItem;
import orange.omn.bean.Service;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { Application.class })
public class ReferencesRepositoryTest {

	@Autowired
	private IRefTemplateItemRepository refTemplateItemRepo;
	
	@Autowired
	private IServicesRepository servicesRepo;
	
	@Autowired
	private IRefPriorityRepository refPriorityRepo;

	@Test
	public void findRefTemplateItemByKeyword() {

		RefTemplateItem refTemplateItem = refTemplateItemRepo.findByKeyword("address(street)");

		assertNotNull(refTemplateItem);
		assertEquals("Rue des Pipompins", refTemplateItem.getExample());
	}

	@Test
	public void findAllRefTemplateItems() {

		List<RefTemplateItem> refTemplateItemList = refTemplateItemRepo.findAll();

		assertNotNull(refTemplateItemList);
		assertEquals(1, refTemplateItemList.size());
	}
	
	@Test
	public void findAllRefServices() {

		List<Service> servicesList = servicesRepo.findAll();

		assertNotNull(servicesList);
		assertEquals(62, servicesList.size());
	}
	
	@Test
	public void findAllRefPriority() {
		
		List<RefPriority> refPriorityList = refPriorityRepo.findAll();
		
		assertNotNull(refPriorityList);
		assertEquals(5, refPriorityList.size());
		
	}
}
