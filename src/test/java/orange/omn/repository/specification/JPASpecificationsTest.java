package orange.omn.repository.specification;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import orange.omn.Application;
import orange.omn.bean.CustomerQualification;
import orange.omn.repository.ICustomerRouteRepository;
import orange.omn.repository.common.SearchCriteria;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
public class JPASpecificationsTest {

	@Autowired
	private ICustomerRouteRepository custRouteRepo;


	@Test
	public void getCustQualFromName() {

		CustRouteSpecification spec = new CustRouteSpecification(new SearchCriteria("customerQualificationName", ":", "Parcours par defaut"));

		List<CustomerQualification> results = custRouteRepo.findAll(spec);

		assertFalse(results.isEmpty());
		assertEquals(results.get(0).getCustomerQualificationName(),"Parcours par defaut");

		

	}
	
	@Test
	public void getCustQualFromNameAndId() {
		
		CustRouteSpecification spec1 = new CustRouteSpecification(new SearchCriteria("customerQualificationName", ":", "Parcours par defaut"));
		CustRouteSpecification spec2 = new CustRouteSpecification(new SearchCriteria("customerQualificationId", ":", "1"));
		
		List<CustomerQualification> results = custRouteRepo.findAll(Specifications.where(spec1).and(spec2));
		
		assertFalse(results.isEmpty());
		assertEquals(results.get(0).getCustomerQualificationName(),"Parcours par defaut");
		assertEquals(results.get(0).getCustomerQualificationId(),1);
		
	}

}
