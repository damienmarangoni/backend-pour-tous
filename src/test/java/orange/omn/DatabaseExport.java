package orange.omn;
import java.io.FileOutputStream;
import java.sql.DriverManager;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.DatabaseSequenceFilter;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.FilteredDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.filter.ITableFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;

public class DatabaseExport {
	
	 public static void main(String[] args) throws Exception
	    {
	        // database connection
	        Class<?> driverClass = Class.forName("com.mysql.jdbc.Driver");
	        java.sql.Connection jdbcConnection = DriverManager.getConnection(
	                "jdbc:mysql://10.195.181.101:3307/pomniws", "pomniwsupdate", "pomniwsupdate");
	        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
	        ITableFilter filter = new DatabaseSequenceFilter(connection);

	        // partial database export
//	        QueryDataSet partialDataSet = new QueryDataSet(connection);
//	        partialDataSet.addTable("FOO", "SELECT * FROM TABLE WHERE COL='VALUE'");
//	        partialDataSet.addTable("BAR");
//	        FlatXmlDataSet.write(partialDataSet, new FileOutputStream("D:\\Orange\\Workspace\\omnwas\\omni-backend\\src\\test\\resources\\dbunitXMLFiles\\CustomerRouteRepositoryTest2.xml"));

	        // full database export
//	        IDataSet fullDataSet = connection.createDataSet();
	        IDataSet fullDataSet = new FilteredDataSet(filter, connection.createDataSet());
	        FlatXmlDataSet.write(fullDataSet, new FileOutputStream("D:\\Orange\\Workspace\\omnwas\\omni-backend\\src\\test\\resources\\dbunitXMLFiles\\CustomerRouteRepositoryTest2.xml"));
	        
	        // dependent tables database export: export table X and all tables that
	        // have a PK which is a FK on X, in the right order for insertion
//	        String[] depTableNames = 
//	          TablesDependencyHelper.getAllDependentTables( connection, "customer_qualification" );
//	        IDataSet depDataset = connection.createDataSet( depTableNames );
//	        FlatXmlDataSet.write(depDataset, new FileOutputStream("D:\\Orange\\Workspace\\omnwas\\omni-backend\\src\\test\\resources\\dbunitXMLFiles\\CustomerRouteRepositoryTest2.xml"));
//	        
	    }

}
