package orange.omn.web.mapper;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.mapstruct.factory.Mappers;

import orange.omn.bean.QualificationExpression;
import orange.omn.web.dto.QualificationExpressionDTO;
import orange.omn.web.dto.RefCategoryDTO;
import orange.omn.web.dto.ServiceDTO;

public class QualificationExpressionMapperTest {
	
	private IQualificationExpressionMapper qualExpressMapper = Mappers.getMapper(IQualificationExpressionMapper.class);
	
	@Test
	public void shouldMapQualExpressToDTO() {
		
	}
	
	@Test
	public void shouldMapQualExpressDTOToEntity() {
		
		QualificationExpressionDTO qualExprDTO = new QualificationExpressionDTO();
		
		// given
		qualExprDTO.setIdCustomerQualification(203);
		qualExprDTO.setCommunityResponse(45);
		qualExprDTO.setCommunityFilter(6);
		qualExprDTO.setRefCategory(new RefCategoryDTO());
		qualExprDTO.setService(new ServiceDTO());
		
		// when
		QualificationExpression qualifExpressEntity = qualExpressMapper.qualifExpressDTOToBean(qualExprDTO);
		
		// then
		assertNotNull(qualifExpressEntity);
		
	}

}
