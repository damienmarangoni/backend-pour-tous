package orange.omn.web.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.mapstruct.factory.Mappers;

import orange.omn.bean.RefDefaultAudioTrack;
import orange.omn.bean.RefDefaultAudioTrackPhase;
import orange.omn.bean.RefDefaultAudioTrackService;
import orange.omn.bean.RefPriority;
import orange.omn.web.dto.RefDefaultAudioTrackDTO;
import orange.omn.web.dto.RefDefaultAudioTrackPhaseDTO;
import orange.omn.web.dto.RefDefaultAudioTrackServiceDTO;
import orange.omn.web.dto.RefPriorityDTO;

public class ReferencesMapperTest {
	
	private IReferencesMapper referencesMapper = Mappers.getMapper(IReferencesMapper.class);
	
	
	@Test
	public void shouldMapRefPriorityToDTO() {
		
	}
	
	@Test
	public void shouldMapRefPriorityDTOToEntity() {
		
		RefPriorityDTO refPriorityDTO = new RefPriorityDTO();
		
		// given
		refPriorityDTO.setIdPriority(1);
		refPriorityDTO.setLibPriority("normal");
		
		// when
		RefPriority refPriorityEntity = referencesMapper.refPriorityDTOToBean(refPriorityDTO);
		
		// then
		assertNotNull(refPriorityEntity);
		assertEquals(refPriorityEntity.getIdPriority(), refPriorityDTO.getIdPriority());
		assertEquals(refPriorityEntity.getLibPriority(), refPriorityDTO.getLibPriority());
		
	}
	
	@Test
	public void shouldMapRefAudioTrackToDTO() {
		
		RefDefaultAudioTrack refDefaultAudioTrack = new RefDefaultAudioTrack();
		RefDefaultAudioTrackPhase refDefaultAudioTrackPhase = new RefDefaultAudioTrackPhase();
		RefDefaultAudioTrackService refDefaultAudioTrackService = new RefDefaultAudioTrackService();
		
		refDefaultAudioTrackPhase.setLibelle("Accueil");
		
		refDefaultAudioTrackService.setServiceName("118700");
		
		refDefaultAudioTrack.setFilename("118700_jour_f.a8k");
		refDefaultAudioTrack.setLibelle("Accueil femme jour");
		refDefaultAudioTrack.setRefDefaultAudioTrackPhase(refDefaultAudioTrackPhase);
		refDefaultAudioTrack.setRefDefaultAudioTrackService(refDefaultAudioTrackService);
		
		RefDefaultAudioTrackDTO refDefaultAudioTrackDTO = referencesMapper.refDefaultAudioTrackToDTO(refDefaultAudioTrack);
		
		assertNotNull(refDefaultAudioTrackDTO);
		assertEquals(refDefaultAudioTrackDTO.getFilename(), refDefaultAudioTrack.getFilename());
		assertEquals(refDefaultAudioTrackDTO.getLibelle(), refDefaultAudioTrack.getLibelle());
		assertEquals(refDefaultAudioTrackDTO.getPhaseLib(), refDefaultAudioTrack.getRefDefaultAudioTrackPhase().getLibelle());
		assertEquals(refDefaultAudioTrackDTO.getServiceName(), refDefaultAudioTrack.getRefDefaultAudioTrackService().getServiceName());
	}
	
	@Test
	public void shouldMapRefAudioTrackDTOToEntity() {
		
		RefDefaultAudioTrackDTO refDefaultAudioTrackDTO = new RefDefaultAudioTrackDTO();
		RefDefaultAudioTrackPhaseDTO refDefATPhaseDTO = new RefDefaultAudioTrackPhaseDTO();
		RefDefaultAudioTrackServiceDTO refDefATServiceDTO = new RefDefaultAudioTrackServiceDTO();
		
		
		refDefATPhaseDTO.setLibelle("Accueil");
		refDefATServiceDTO.setServiceName("118700");
		
		refDefaultAudioTrackDTO.setFilename("118700_jour_f.a8k");
		refDefaultAudioTrackDTO.setLibelle("Accueil femme jour");
		refDefaultAudioTrackDTO.setRefDefATPhaseDTO(refDefATPhaseDTO);
		refDefaultAudioTrackDTO.setRefDefATServiceDTO(refDefATServiceDTO);
		
		RefDefaultAudioTrack refDefaultAudioTrack = referencesMapper.refDefaultAudioTrackDTOToBean(refDefaultAudioTrackDTO);
		
		assertNotNull(refDefaultAudioTrack);
		assertEquals(refDefaultAudioTrackDTO.getFilename(), refDefaultAudioTrack.getFilename());
		assertEquals(refDefaultAudioTrackDTO.getLibelle(), refDefaultAudioTrack.getLibelle());
		assertEquals(refDefaultAudioTrackDTO.getPhaseLib(), refDefaultAudioTrack.getRefDefaultAudioTrackPhase().getLibelle());
		assertEquals(refDefaultAudioTrackDTO.getServiceName(), refDefaultAudioTrack.getRefDefaultAudioTrackService().getServiceName());
		
	}

}
