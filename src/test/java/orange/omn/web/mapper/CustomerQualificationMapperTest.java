package orange.omn.web.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mapstruct.factory.Mappers;

import orange.omn.bean.CustomerQualification;
import orange.omn.bean.ProgramParent;
import orange.omn.bean.QualificationExpression;
import orange.omn.web.dto.CustomerRouteDTO;
import orange.omn.web.dto.ProgramParentDTO;
import orange.omn.web.dto.QualificationExpressionDTO;

public class CustomerQualificationMapperTest {

	private ICustomerQualificationMapper custQualMapper = Mappers.getMapper(ICustomerQualificationMapper.class);

	@Test
	public void shouldMapCustQualToDTO() {

		// given
		LocalDate ahora = LocalDate.now();
		ProgramParent pp1 = new ProgramParent();

		List<ProgramParent> programParents = new ArrayList<ProgramParent>();
		List<QualificationExpression> qualificationExpressions = new ArrayList<QualificationExpression>();

		CustomerQualification singleCustQualEntity = new CustomerQualification("parcours client test mapper", 1);

		pp1.setDateStart(ahora);
		pp1.setDateEnd(ahora.plus(1, ChronoUnit.DECADES));

		pp1.setAccess(1);

		programParents.add(pp1);

		singleCustQualEntity.setProgramParents(programParents);
		singleCustQualEntity.setQualificationExpressions(qualificationExpressions);

		// when
		CustomerRouteDTO singleCustQualDTO = custQualMapper.customerQualificationToDTO(singleCustQualEntity);

		// then
		assertNotNull(singleCustQualDTO);
		assertEquals(singleCustQualDTO.getCustomerQualificationName(),
				singleCustQualEntity.getCustomerQualificationName());
		assertEquals(singleCustQualDTO.getCustomerQualificationId(), singleCustQualEntity.getCustomerQualificationId());
		assertEquals(singleCustQualDTO.getProgramParentDTOs().size(), singleCustQualEntity.getProgramParents().size());
	}

	@Test
	public void shouldMapCustQualDTOToEntity() {

		// given
		LocalDate ahora = LocalDate.now();
;
		ProgramParentDTO pp1 = new ProgramParentDTO();

		List<ProgramParentDTO> programParentsDTOs = new ArrayList<ProgramParentDTO>();
		List<QualificationExpressionDTO> qualificationExpressionDTOs = new ArrayList<QualificationExpressionDTO>();
		CustomerRouteDTO singleCustQualDTO = new CustomerRouteDTO("parcours client test mapper", 1);

		pp1.setDateStart(ahora);
		pp1.setDateEnd(ahora.plus(1, ChronoUnit.DECADES));

		pp1.setAccess(1);
		programParentsDTOs.add(pp1);

		singleCustQualDTO.setProgramParentDTOs(programParentsDTOs);
		singleCustQualDTO.setQualificationExpressionsDTOs(qualificationExpressionDTOs);

		// when
		CustomerQualification singleCustQualEntity = custQualMapper.customerQualificationDTOToBean(singleCustQualDTO);

		// then
		assertNotNull(singleCustQualEntity);
		assertEquals(singleCustQualEntity.getCustomerQualificationName(),
				singleCustQualDTO.getCustomerQualificationName());
		assertEquals(singleCustQualEntity.getCustomerQualificationId(), singleCustQualDTO.getCustomerQualificationId());
		assertEquals(singleCustQualEntity.getProgramParents().size(), singleCustQualDTO.getProgramParentDTOs().size());

	}

}
