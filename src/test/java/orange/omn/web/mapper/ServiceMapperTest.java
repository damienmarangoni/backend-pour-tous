package orange.omn.web.mapper;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.mapstruct.factory.Mappers;

import orange.omn.bean.Service;
import orange.omn.web.dto.ServiceDTO;

public class ServiceMapperTest {
	
	private IServiceMapper serviceMapper = Mappers.getMapper(IServiceMapper.class);
	
	@Test
	public void shouldMapServiceEntityToDTO() {
		
	}
	
	@Test
	public void shouldMapServiceDTOToEntity() {
		
		ServiceDTO serviceDTO = new ServiceDTO();
		
		serviceDTO.setServiceDescription("Recherche Inverse");
		serviceDTO.setServiceDu("32");
		serviceDTO.setServiceUser("dlfh0505");
		
		Service service = serviceMapper.serviceDTOToBean(serviceDTO);
		
		assertNotNull(service);
		
	}

}
