package orange.omn.web.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mapstruct.factory.Mappers;

import orange.omn.bean.Campaign;
import orange.omn.bean.Format;
import orange.omn.bean.Location;
import orange.omn.bean.Program;
import orange.omn.bean.ProgramParent;
import orange.omn.bean.RefPriority;
import orange.omn.bean.Template;
import orange.omn.bean.TemplateContent;
import orange.omn.web.dto.ProgramParentDTO;

public class ProgramParentMapperTest {

	private IProgramParentMapper progParMapper = Mappers.getMapper(IProgramParentMapper.class);

	@Test
	public void shouldMapProgParToDTO() {

		// given
		LocalDate ahora = LocalDate.now();
		LocalDate manana = ahora.plus(1, ChronoUnit.DECADES);


		ProgramParent progParEntity = new ProgramParent();
		Program program = new Program();
		Campaign campaign = new Campaign();
		Location location = new Location();
		RefPriority refPriority = new RefPriority();
		Template template = new Template();
		String refPhase = "";
		Format format = new Format();
		TemplateContent templateContent = new TemplateContent();

		List<Program> programs = new ArrayList<Program>();

		program.setAccess(10);
		program.setDateStart(ahora);
		program.setDateEnd(manana);
		program.setIdProgram(1);

		programs.add(program);

		refPriority.setIdPriority(3);
		refPriority.setLibPriority("Normal");

		template.setAccess(1);
		template.setTemplateName("Template numero noir");
		template.setDisplay(0);
		template.setIdTemplate(2);

		template.setCodePhase(refPhase);
		template.setTemplateContent(templateContent);
		template.setFormat(format);

		campaign.setCampaignName("get lost you nuts");
		campaign.setIdCampaign(47);
		campaign.setDateStart(ahora);
		campaign.setDateEnd(manana);

		location.setIdLocation(4);
		location.setLabel("Bordeaux");

		progParEntity.setCampaign(campaign);
		progParEntity.setLocation(location);
		progParEntity.setDateStart(ahora);
		progParEntity.setDateEnd(manana);
		progParEntity.setPrograms(programs);

		progParEntity.setRefPriority(refPriority);

		progParEntity.setTemplate(template);

		// when
		ProgramParentDTO progParDTO = progParMapper.programParentToDTO(progParEntity);

		// then
		assertNotNull(progParDTO);
		assertEquals(progParDTO.getAccess(), progParEntity.getAccess());
		assertEquals(progParDTO.getDateStart(), progParEntity.getDateStart());
		assertEquals(progParDTO.getDateEnd(), progParEntity.getDateEnd());
		assertEquals(progParDTO.getCampaign().getCampaignName(), progParEntity.getCampaign().getCampaignName());
		assertEquals(progParDTO.getLocation().getLabel(), progParEntity.getLocation().getLabel());
		assertEquals(progParDTO.getPrograms().size(), progParEntity.getPrograms().size());
		assertEquals(progParDTO.getPrograms().get(0).getIdProgram(), progParEntity.getPrograms().get(0).getIdProgram());
	}

}
