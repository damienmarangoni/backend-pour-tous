package orange.omn.web.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.mapstruct.factory.Mappers;

import orange.omn.bean.ContractCustomer;
import orange.omn.bean.RefChannel;
import orange.omn.bean.TemplateContent;
import orange.omn.web.dto.TemplateContentDTO;

public class TemplateContentMapperTest {
	
	private ITemplateContentMapper templateContentMapper = Mappers.getMapper(ITemplateContentMapper.class);
	
	@Test
	public void shouldMapServiceEntityToDTO() {
		
		TemplateContent templateContent = new TemplateContent();
		ContractCustomer contractCustomer = new ContractCustomer();
		RefChannel refChannel = new RefChannel();
		
		contractCustomer.setLabel("118712");
		contractCustomer.setContractCustomerId(1);
		
		refChannel.setIdChannel(2);
		refChannel.setLibChannel("vocal");
		
		
		templateContent.setContractCustomer(contractCustomer);
		templateContent.setRefChannel(refChannel);
		templateContent.setTemplateContentId("118 712 sorties ciné");
		templateContent.setTemplateContentName("118 712 sorties ciné");
		templateContent.setTemplateContentShortTxt("");
		templateContent.setTemplateContentTxt("118 712 vous souhaite une bonne rentrée");
		
		TemplateContentDTO tempContDTO = templateContentMapper.templateContentToDTO(templateContent);
		
		assertNotNull(tempContDTO);
		assertEquals(tempContDTO.getChannelId(), templateContent.getRefChannel().getIdChannel());
		assertEquals(tempContDTO.getContractCustomerId(),templateContent.getContractCustomer().getContractCustomerId());
		assertEquals(tempContDTO.getContractCustomerLabel(),templateContent.getContractCustomer().getLabel());
		assertEquals(tempContDTO.getChannelLib(),templateContent.getRefChannel().getLibChannel());
	}
	
	@Test
	public void shouldMapTemplateContentDTOToEntity() {
		
		TemplateContentDTO tempContDTO = new TemplateContentDTO();
		
		tempContDTO.setChannelId(1);
		tempContDTO.setContractCustomerId(5);
		tempContDTO.setContractCustomerLabel("118712");
		tempContDTO.setTemplateContentId("118 712 sorties ciné");
		tempContDTO.setTemplateContentName("118 712 sorties ciné");
		tempContDTO.setTemplateContentShortTxt("");
		tempContDTO.setTemplateContentTxt("118 712 vous souhaite une bonne rentrée");
		
		
		TemplateContent templateContent = templateContentMapper.templateContentDTOToBean(tempContDTO);
		
		assertNotNull(templateContent);
		
		assertEquals(templateContent.getContractCustomer().getContractCustomerId(),tempContDTO.getContractCustomerId());
		assertEquals(templateContent.getContractCustomer().getLabel(),tempContDTO.getContractCustomerLabel());
		
	}

}
