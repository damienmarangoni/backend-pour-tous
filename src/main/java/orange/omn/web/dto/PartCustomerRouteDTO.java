package orange.omn.web.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author XT0087920
 * 
 *         This DTO is a light version of CustomerRouteDTO, meant to be shipped
 *         from the client side, without overflowing the bandwidth
 *
 */
public class PartCustomerRouteDTO {

	private int customerQualificationId;
	private String customerQualificationName;

	public PartCustomerRouteDTO() {
	}

	public PartCustomerRouteDTO(String customerQualificationName, int customerQualificationId) {
		this.customerQualificationName = customerQualificationName;
		this.customerQualificationId = customerQualificationId;
	}

	public int getCustomerQualificationId() {
		return customerQualificationId;
	}

	public void setCustomerQualificationId(int customerQualificationId) {
		this.customerQualificationId = customerQualificationId;
	}

	public List<QualificationExpressionDTO> getQualificationExpressionsDTOs() {
		return qualificationExpressionsDTOs;
	}

	public void setQualificationExpressionsDTOs(List<QualificationExpressionDTO> qualificationExpressionsDTOs) {
		this.qualificationExpressionsDTOs = qualificationExpressionsDTOs;
	}

	public String getCustomerQualificationName() {
		return customerQualificationName;
	}

	public void setCustomerQualificationName(String customerQualificationName) {
		this.customerQualificationName = customerQualificationName;
	}

	private List<QualificationExpressionDTO> qualificationExpressionsDTOs = new ArrayList<QualificationExpressionDTO>();

}
