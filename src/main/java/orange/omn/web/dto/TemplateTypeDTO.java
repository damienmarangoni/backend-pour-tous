package orange.omn.web.dto;

public class TemplateTypeDTO {
	
    public String getTemplateTypeDescription() {
        return templateTypeDescription;
    }

    public void setTemplateTypeDescription(String templateTypeDescription) {
        this.templateTypeDescription = templateTypeDescription;
    }

    public int getTemplateTypeId() {
        return templateTypeId;
    }

    public void setTemplateTypeId(int templateTypeId) {
        this.templateTypeId = templateTypeId;
    }

    private String templateTypeDescription;

    private int templateTypeId;

}
