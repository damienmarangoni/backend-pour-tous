package orange.omn.web.dto;

public class RefDefaultAudioTrackDTO {
	
	private String filename;
	
	private String libelle;
	
	private RefDefaultAudioTrackPhaseDTO refDefATPhaseDTO = new RefDefaultAudioTrackPhaseDTO();
	
	private RefDefaultAudioTrackServiceDTO refDefATServiceDTO = new RefDefaultAudioTrackServiceDTO();

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	public void setPhaseLib(String phaseLib) {
		refDefATPhaseDTO.setLibelle(phaseLib);
	}
	
	public String getPhaseLib() {
		return refDefATPhaseDTO.getLibelle();
	}
	
	public void setServiceName(String serviceName) {
		refDefATServiceDTO.setServiceName(serviceName);
	}
	
	public String getServiceName() {
		return refDefATServiceDTO.getServiceName();
	}

	public RefDefaultAudioTrackPhaseDTO getRefDefATPhaseDTO() {
		return refDefATPhaseDTO;
	}

	public void setRefDefATPhaseDTO(RefDefaultAudioTrackPhaseDTO refDefATPhaseDTO) {
		this.refDefATPhaseDTO = refDefATPhaseDTO;
	}

	public RefDefaultAudioTrackServiceDTO getRefDefATServiceDTO() {
		return refDefATServiceDTO;
	}

	public void setRefDefATServiceDTO(RefDefaultAudioTrackServiceDTO refDefATServiceDTO) {
		this.refDefATServiceDTO = refDefATServiceDTO;
	}




	
	
}
