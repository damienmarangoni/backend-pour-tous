package orange.omn.web.dto;

import java.util.ArrayList;
import java.util.List;

public class CustomerRouteDTO {
	
	private String customerQualificationName;
	private int customerQualificationId;
	
	
	private List<ProgramParentDTO> programParentDTOs = new ArrayList<ProgramParentDTO> ();
	private List<QualificationExpressionDTO> qualificationExpressionsDTOs = new ArrayList<QualificationExpressionDTO> ();
	
	public CustomerRouteDTO() {}
	
	public CustomerRouteDTO(String customerQualificationName, int customerQualificationId) {
		this.customerQualificationName = customerQualificationName;
		this.customerQualificationId = customerQualificationId;
	}

	public String getCustomerQualificationName() {
		return customerQualificationName;
	}

	public void setCustomerQualificationName(String customerQualificationName) {
		this.customerQualificationName = customerQualificationName;
	}

	public int getCustomerQualificationId() {
		return customerQualificationId;
	}

	public void setCustomerQualificationId(int customerQualificationId) {
		this.customerQualificationId = customerQualificationId;
	}

	public List<ProgramParentDTO> getProgramParentDTOs() {
		return programParentDTOs;
	}

	public void setProgramParentDTOs(List<ProgramParentDTO> programParentDTOs) {
		this.programParentDTOs = programParentDTOs;
	}

	public List<QualificationExpressionDTO> getQualificationExpressionsDTOs() {
		return qualificationExpressionsDTOs;
	}

	public void setQualificationExpressionsDTOs(List<QualificationExpressionDTO> qualificationExpressionsDTOs) {
		this.qualificationExpressionsDTOs = qualificationExpressionsDTOs;
	}

	
	
	

}
