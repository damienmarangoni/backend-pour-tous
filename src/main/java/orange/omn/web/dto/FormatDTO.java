package orange.omn.web.dto;

public class FormatDTO {

	private String formatDescription;

	private String formatId;

	public String getFormatDescription() {
		return formatDescription;
	}

	public void setFormatDescription(String formatDescription) {
		this.formatDescription = formatDescription;
	}

	public String getFormatId() {
		return formatId;
	}

	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}
	
	

}
