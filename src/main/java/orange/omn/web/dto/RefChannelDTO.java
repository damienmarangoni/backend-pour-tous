package orange.omn.web.dto;

public class RefChannelDTO {
	
    private int idChannel;

    private String libChannel;

	public int getIdChannel() {
		return idChannel;
	}

	public void setIdChannel(int idChannel) {
		this.idChannel = idChannel;
	}

	public String getLibChannel() {
		return libChannel;
	}

	public void setLibChannel(String libChannel) {
		this.libChannel = libChannel;
	}
    
    

}
