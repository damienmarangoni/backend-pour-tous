package orange.omn.web.dto;

public class TemplateDTO {
	
	 private int access;
	 
	 private int display;
	 
	 private int idTemplate;
	 
	 private String templateTemplate;
	 
	 private FormatDTO format;

    //	 private RefPhaseDTO refPhase;
	 private String codePhase;
	 
	 private TemplateContentDTO templateContent;
     
     private TemplateTypeDTO templateType;
	 
	 public TemplateTypeDTO getTemplateType() {
        return templateType;
    }

    public void setTemplateType(TemplateTypeDTO templateType) {
        this.templateType = templateType;
    }

    private String templateName;
     
//     private String templateContentId;
//
//     public String getTemplateContentId() {
//         return templateContentId;
//     }
//
//     public void setTemplateContentId(String templateContentId) {
//         this.templateContentId = templateContent.getTemplateContentId();
//     }


    public int getAccess() {
		return access;
	}

	public void setAccess(int access) {
		this.access = access;
	}

	public int getDisplay() {
		return display;
	}

	public void setDisplay(int display) {
		this.display = display;
	}

	public int getIdTemplate() {
		return idTemplate;
	}

	public void setIdTemplate(int idTemplate) {
		this.idTemplate = idTemplate;
	}

	public FormatDTO getFormat() {
		return format;
	}

	public void setFormat(FormatDTO format) {
		this.format = format;
	}

//	public RefPhaseDTO getRefPhase() {
//		return refPhase;
//	}
//
//	public void setRefPhase(RefPhaseDTO refPhase) {
//		this.refPhase = refPhase;
//	}	
   public String getCodePhase() {
        return codePhase;
    }

    public void setCodePhase(String codePhase) {
        this.codePhase = codePhase;
    }

	public TemplateContentDTO getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(TemplateContentDTO templateContent) {
		this.templateContent = templateContent;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

    public String getTemplateTemplate() {
        return templateTemplate;
    }

    public void setTemplateTemplate(String templateTemplate) {
        this.templateTemplate = templateTemplate;
    }
	 
	 
	 


}
