package orange.omn.web.dto;

public class RefDefaultAudioTrackServiceDTO {
	
	public String serviceName;

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

}
