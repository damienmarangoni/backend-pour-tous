package orange.omn.web.dto;

public class RefPhaseDTO {
	
	private String codePhase;

    private String libPhase;

    private int maxTrack;

//    private RefChannel refChannel;

	public String getCodePhase() {
		return codePhase;
	}

	public void setCodePhase(String codePhase) {
		this.codePhase = codePhase;
	}

	public String getLibPhase() {
		return libPhase;
	}

	public void setLibPhase(String libPhase) {
		this.libPhase = libPhase;
	}

	public int getMaxTrack() {
		return maxTrack;
	}

	public void setMaxTrack(int maxTrack) {
		this.maxTrack = maxTrack;
	}

//	public RefChannel getRefChannel() {
//		return refChannel;
//	}
//
//	public void setRefChannel(RefChannel refChannel) {
//		this.refChannel = refChannel;
//	}
    
    

}
