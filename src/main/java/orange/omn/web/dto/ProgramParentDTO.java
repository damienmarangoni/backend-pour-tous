package orange.omn.web.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;


public class ProgramParentDTO {
    

        private int idProgramParent;

        private int access;  
        
        private String vEvent;
	    
	    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
		private LocalDate dateEnd;
	    
	    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	    private LocalDate dateStart;	    

        private LocationDTO location;

	    private CampaignDTO campaign;
	    
	    private List<ProgramDTO> programs = new ArrayList<ProgramDTO> ();
	    
	    private TemplateDTO template;
	    
	    private TemplateContentDTO templateContent;

        private RefPriorityDTO refPriority;   
        
        private int priority;

	    public int getPriority() {
            return priority;
        }

        public void setPriority(int priority) {
            this.priority = refPriority.getIdPriority();
        }

        public String getVEvent() {
            return vEvent;
        }

        public void setVEvent(String vEvent) {
            this.vEvent = vEvent;
        }

        public int getAccess() {
			return access;
		}

		public void setAccess(int access) {
			this.access = access;
		}
    
        public int getIdProgramParent() {
            return idProgramParent;
        }

        public void setIdProgramParent(int idProgramParent) {
            this.idProgramParent = idProgramParent;
        }

		public LocalDate getDateEnd() {
			return dateEnd;
		}

		public void setDateEnd(LocalDate dateEnd) {
			this.dateEnd = dateEnd;
		}

		public LocalDate getDateStart() {
			return dateStart;
		}

		public void setDateStart(LocalDate dateStart) {
			this.dateStart = dateStart;
		}

		public LocationDTO getLocation() {
			return location;
		}

		public void setLocation(LocationDTO location) {
			this.location = location;
		}

		public CampaignDTO getCampaign() {
			return campaign;
		}

		public void setCampaign(CampaignDTO campaign) {
			this.campaign = campaign;
		}

		public List<ProgramDTO> getPrograms() {
            return programs;
        }

        public void setPrograms(List<ProgramDTO> programs) {
            this.programs = programs;
        }

        public TemplateDTO getTemplate() {
			return template;
		}

		public void setTemplate(TemplateDTO template) {
			this.template = template;
		}
        
        public RefPriorityDTO getRefPriority() {
            return refPriority;
        }

        public void setRefPriority(RefPriorityDTO refPriority) {
            this.refPriority = refPriority;
        }

        public TemplateContentDTO getTemplateContent() {
            return templateContent;
        }

        public void setTemplateContent(TemplateContentDTO templateContent) {
            this.templateContent = templateContent;
        }
}
