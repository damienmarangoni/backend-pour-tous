package orange.omn.web.dto;

public class QualificationExpressionDTO {

	private int communityFilter;
	
	private RefCategoryDTO refCategory;
	
    private int communityResponse;
	
    private int idCustomerQualification;
	
    private ServiceDTO service;

	
    public ServiceDTO getService() {
        return service;
    }
    
    public int getCommunityFilter() {
        return communityFilter;
    }

    public void setCommunityFilter(int communityFilter) {
        this.communityFilter = communityFilter;
    }

    public void setService(ServiceDTO service) {
        this.service = service;
    }
	
	public int getCommunityResponse() {
		return communityResponse;
	}
	public void setCommunityResponse(int communityResponse) {
		this.communityResponse = communityResponse;
	}
    
    public RefCategoryDTO getRefCategory() {
        return refCategory;
    }

    public void setRefCategory(RefCategoryDTO refCategory) {
        this.refCategory = refCategory;
    }

    public int getIdCustomerQualification() {
		return idCustomerQualification;
	}

	public void setIdCustomerQualification(int idCustomerQualification) {
        this.idCustomerQualification = idCustomerQualification;
    }
}
