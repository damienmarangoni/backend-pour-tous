package orange.omn.web.dto;

public class LocationDTO {
	
	  private int idLocation;
	  
	  private String label;

	public int getIdLocation() {
		return idLocation;
	}

	public void setIdLocation(int idLocation) {
		this.idLocation = idLocation;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	  
	  

}
