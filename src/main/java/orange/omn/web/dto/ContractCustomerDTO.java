package orange.omn.web.dto;

public class ContractCustomerDTO {
	
    private int contractCustomerId;

    private String label;

	public int getContractCustomerId() {
		return contractCustomerId;
	}

	public void setContractCustomerId(int contractCustomerId) {
		this.contractCustomerId = contractCustomerId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
