package orange.omn.web.dto;


public class RefPriorityDTO {    

    private int idPriority;
    private String libPriority;

    public RefPriorityDTO() {}
    
    public RefPriorityDTO(int idPriority, String libPriority) {
		this.idPriority = idPriority;
		this.libPriority = libPriority;
	}
	public int getIdPriority() {
        return idPriority;
    }
    public void setIdPriority(int idPriority) {
        this.idPriority = idPriority;
    }
    public String getLibPriority() {
        return libPriority;
    }
    public void setLibPriority(String libPriority) {
        this.libPriority = libPriority;
    }
}
