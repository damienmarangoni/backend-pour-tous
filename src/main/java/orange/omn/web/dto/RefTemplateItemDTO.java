package orange.omn.web.dto;

public class RefTemplateItemDTO {    

    private int contentSent;

    private String description;

    private boolean enabled;

    private String example;

    private int id;

    private int infoaddId;

    private String keyword;

    public int getContentSent() {
        return contentSent;
    }

    public void setContentSent(int contentSent) {
        this.contentSent = contentSent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInfoaddId() {
        return infoaddId;
    }

    public void setInfoaddId(int infoaddId) {
        this.infoaddId = infoaddId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public boolean isPriorisable() {
        return priorisable;
    }

    public void setPriorisable(boolean priorisable) {
        this.priorisable = priorisable;
    }

    private boolean priorisable;
}
