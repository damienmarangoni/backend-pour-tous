package orange.omn.web.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ProgramDTO {

	private int access;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate dateEnd;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate dateStart;

	private int idProgram;

    public int getAccess() {
		return access;
	}

	public void setAccess(int access) {
		this.access = access;
	}

	public LocalDate getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(LocalDate dateEnd) {
		this.dateEnd = dateEnd;
	}

	public LocalDate getDateStart() {
		return dateStart;
	}

	public void setDateStart(LocalDate dateStart) {
		this.dateStart = dateStart;
	}

	public int getIdProgram() {
		return idProgram;
	}

	public void setIdProgram(int idProgram) {
		this.idProgram = idProgram;
	}

	// public ProgramParentDTO getProgramParentDTO() {
	// return programParentDTO;
	// }
	//
	// public void setProgramParentDTO(ProgramParentDTO programParentDTO) {
	// this.programParentDTO = programParentDTO;
	// }

}
