package orange.omn.web.dto;

public class MaskDTO {
	
	 private String maskDescription;
	 
	 private String maskMaskedNb;
	 
	 private String maskMessage;
	 
	 private String maskPublicNb;

	public String getMaskDescription() {
		return maskDescription;
	}

	public void setMaskDescription(String maskDescription) {
		this.maskDescription = maskDescription;
	}

	public String getMaskMaskedNb() {
		return maskMaskedNb;
	}

	public void setMaskMaskedNb(String maskMaskedNb) {
		this.maskMaskedNb = maskMaskedNb;
	}

	public String getMaskMessage() {
		return maskMessage;
	}

	public void setMaskMessage(String maskMessage) {
		this.maskMessage = maskMessage;
	}

	public String getMaskPublicNb() {
		return maskPublicNb;
	}

	public void setMaskPublicNb(String maskPublicNb) {
		this.maskPublicNb = maskPublicNb;
	}

 


}
