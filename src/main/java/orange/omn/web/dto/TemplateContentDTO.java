package orange.omn.web.dto;

public class TemplateContentDTO {

    private String templateContentId;

    private String templateContentName;

    private String templateContentShortTxt;

    private String templateContentTxt;
    
    private ContractCustomerDTO contractCustomerDTO = new ContractCustomerDTO();

	private RefChannelDTO refChannelDTO = new RefChannelDTO();
    
    public String getTemplateContentId() {
        return templateContentId;
    }

    public void setTemplateContentId(String templateContentId) {
        this.templateContentId = templateContentId;
    }

    public String getTemplateContentName() {
        return templateContentName;
    }

    public void setTemplateContentName(String templateContentName) {
        this.templateContentName = templateContentName;
    }

    public String getTemplateContentShortTxt() {
        return templateContentShortTxt;
    }

    public void setTemplateContentShortTxt(String templateContentShortTxt) {
        this.templateContentShortTxt = templateContentShortTxt;
    }

    public String getTemplateContentTxt() {
        return templateContentTxt;
    }

    public void setTemplateContentTxt(String templateContentTxt) {
        this.templateContentTxt = templateContentTxt;
    }

	public int getContractCustomerId() {
		return contractCustomerDTO.getContractCustomerId();
	}

	public void setContractCustomerId(int contractCustomerId) {
		this.contractCustomerDTO.setContractCustomerId(contractCustomerId);
	}

	public String getContractCustomerLabel() {
		return contractCustomerDTO.getLabel();
	}

	public void setContractCustomerLabel(String contractCustomerLabel) {
		contractCustomerDTO.setLabel(contractCustomerLabel);;
	}

	public int getChannelId() {
		return refChannelDTO.getIdChannel();
	}

	public void setChannelId(int channelId) {
		refChannelDTO.setIdChannel(channelId);
	}
	
	public void setChannelLib(String channelLib) {
		refChannelDTO.setLibChannel(channelLib);
	}
	
	public String getChannelLib() {
		return refChannelDTO.getLibChannel();
	}

	public RefChannelDTO getRefChannelDTO() {
		return refChannelDTO;
	}

	public void setRefChannelDTO(RefChannelDTO refChannelDTO) {
		this.refChannelDTO = refChannelDTO;
	}

	public ContractCustomerDTO getContractCustomerDTO() {
		return contractCustomerDTO;
	}

	public void setContractCustomerDTO(ContractCustomerDTO contractCustomerDTO) {
		this.contractCustomerDTO = contractCustomerDTO;
	}
	
}
