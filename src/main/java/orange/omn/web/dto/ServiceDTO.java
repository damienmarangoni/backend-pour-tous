package orange.omn.web.dto;

public class ServiceDTO {
    private String serviceDescription;
    
    private String serviceDu;
    
    private String serviceUser;

    public String getServiceUser() {
		return serviceUser;
	}

	public void setServiceUser(String serviceUser) {
		this.serviceUser = serviceUser;
	}

	public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getServiceDu() {
        return serviceDu;
    }

    public void setServiceDu(String serviceDu) {
        this.serviceDu = serviceDu;
    }
}
