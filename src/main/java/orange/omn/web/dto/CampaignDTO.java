package orange.omn.web.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class CampaignDTO {

	private String campaignName;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm a z")
	private Date dateEnd;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm a z")
	private Date dateStart;

	private int idCampaign;

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public int getIdCampaign() {
		return idCampaign;
	}

	public void setIdCampaign(int idCampaign) {
		this.idCampaign = idCampaign;
	}

//	public List<ProgramParentDTO> getProgramParentDTOs() {
//		return programParentDTOs;
//	}
//
//	public void setProgramParentDTOs(List<ProgramParentDTO> programParentDTOs) {
//		this.programParentDTOs = programParentDTOs;
//	}
	
	

}
