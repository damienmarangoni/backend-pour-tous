package orange.omn.web.dto;

public class RefCategoryDTO {    

    private int codeCategory;

    private String libCategory;

    public int getCodeCategory() {
        return codeCategory;
    }

    public void setCodeCategory(int codeCategory) {
        this.codeCategory = codeCategory;
    }

    public String getLibCategory() {
        return libCategory;
    }

    public void setLibCategory(String libCategory) {
        this.libCategory = libCategory;
    }



}
