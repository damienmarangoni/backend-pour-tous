package orange.omn.web.dto;


public class RefCommunityDTO {    

    private int codeCommunity;
    private String libCommunity;

    public int getCodeCommunity() {
        return codeCommunity;
    }
    public void setCodeCommunity(int codeCommunity) {
        this.codeCommunity = codeCommunity;
    }
    public String getLibCommunity() {
        return libCommunity;
    }
    public void setLibCommunity(String libCommunity) {
        this.libCommunity = libCommunity;
    }



}
