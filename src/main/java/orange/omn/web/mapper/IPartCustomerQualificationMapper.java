package orange.omn.web.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import orange.omn.bean.CustomerQualification;
import orange.omn.bean.QualificationExpression;
import orange.omn.bean.RefCategory;
import orange.omn.bean.Service;
import orange.omn.web.dto.PartCustomerRouteDTO;
import orange.omn.web.dto.QualificationExpressionDTO;
import orange.omn.web.dto.RefCategoryDTO;
import orange.omn.web.dto.ServiceDTO;

@Mapper(componentModel = "spring")
public interface IPartCustomerQualificationMapper {

	@Mappings({
			@Mapping(target = "qualificationExpressionsDTOs", source = "custQualifEntity.qualificationExpressions") })

	PartCustomerRouteDTO partCustomerQualificationToDTO(CustomerQualification custQualifEntity);

	@Mappings({
			@Mapping(target = "qualificationExpressions", source = "partCustomerQualificationDTO.qualificationExpressionsDTOs") })
	CustomerQualification customerQualificationDTOToBean(PartCustomerRouteDTO partCustomerQualificationDTO);

	QualificationExpressionDTO qualificationExpressionToDTO(QualificationExpression qualificationExpression);

	QualificationExpression qualificationExpressionToBean(QualificationExpressionDTO qualificationExpressionDTO);

	ServiceDTO serviceToDTO(Service service);

	Service serviceDTOToBean(ServiceDTO serviceDTO);

	RefCategoryDTO refCategoryToDTO(RefCategory refCategory);

	RefCategory refCategoryDTOToBean(RefCategoryDTO refCategoryDTO);

	List<PartCustomerRouteDTO> customerQualificationToDTOs(List<CustomerQualification> customerQualifications);

	List<CustomerQualification> customerQualifiatcionDTOstoBeans(List<PartCustomerRouteDTO> customerQualifaictionDTOs);

	CustomerQualification customerQualificationDTOToBean(PartCustomerRouteDTO partialCustRoute, String id);

}
