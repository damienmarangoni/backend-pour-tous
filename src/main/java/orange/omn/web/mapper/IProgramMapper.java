package orange.omn.web.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import orange.omn.bean.Program;
import orange.omn.web.dto.ProgramDTO;

@Mapper(componentModel = "spring")
public interface IProgramMapper {
	
	//IProgramParentMapper INSTANCE  = Mappers.getMapper(IProgramParentMapper.class);
    
 
  ProgramDTO programToDTO(Program ProgramEntity); 
  Program programDTOToBean(ProgramDTO ProgramDTO);
 
  List<ProgramDTO> programsToDTOs(List<Program>programs);
  List<Program> programDTOsToBeans(List<ProgramDTO>programDTOs);
	

}
