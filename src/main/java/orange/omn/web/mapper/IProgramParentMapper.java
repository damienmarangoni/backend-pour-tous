package orange.omn.web.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import orange.omn.bean.Campaign;
import orange.omn.bean.Format;
import orange.omn.bean.Location;
import orange.omn.bean.Program;
import orange.omn.bean.ProgramParent;
import orange.omn.bean.RefPhase;
import orange.omn.bean.RefPriority;
import orange.omn.bean.Template;
import orange.omn.bean.TemplateContent;
import orange.omn.bean.TemplateType;
import orange.omn.web.dto.CampaignDTO;
import orange.omn.web.dto.FormatDTO;
import orange.omn.web.dto.LocationDTO;
import orange.omn.web.dto.ProgramDTO;
import orange.omn.web.dto.ProgramParentDTO;
import orange.omn.web.dto.RefPhaseDTO;
import orange.omn.web.dto.RefPriorityDTO;
import orange.omn.web.dto.TemplateContentDTO;
import orange.omn.web.dto.TemplateDTO;
import orange.omn.web.dto.TemplateTypeDTO;

@Mapper(componentModel = "spring")
public interface IProgramParentMapper {
	
	//IProgramParentMapper INSTANCE  = Mappers.getMapper(IProgramParentMapper.class);
 
    //@Mapping(target="idCustomerQualification", source="programParent.customerQualification.customerQualificationId")
	ProgramParentDTO programParentToDTO(ProgramParent programParent);
	
	//@Mapping(target="template.codePhase", source="programParentDTO.codePhase")
	ProgramParent programParentDTOToBean(ProgramParentDTO programParentDTO); 
	
	CampaignDTO campaignToDTO(Campaign campaign);
	Campaign campaignDTOToBean(CampaignDTO campaignDTO);
	
	LocationDTO locationToDTO(Location location); 
	Location locationDTOToBean(LocationDTO locationDTO);
	
	ProgramDTO programToDTO(Program program);
	Program programDTOToBean(ProgramDTO programDTO);
	
	TemplateDTO templateToDTO(Template template);
	Template templateDTOToBean(TemplateDTO templateDTO);
	
	FormatDTO formatToDTO(Format format);
	Format formatDTOToBean(FormatDTO formatDTO); 
	
	RefPhaseDTO refPhaseToDTO(RefPhase refPhase);
	RefPhase refPhaseDTOToBean(RefPhaseDTO refPhaseDTO);
    
    RefPriorityDTO refPriorityToDTO(RefPriority refPriority);
    RefPriority refPriorityDTOToBean(RefPriorityDTO refPriorityDTO);
   
	TemplateContentDTO templateContentToDTO(TemplateContent templateContent);
	TemplateContent templateContentDTOToBean(TemplateContentDTO templateContentDTO);    
    
    TemplateTypeDTO templateTypeToDTO(TemplateType templateType);
    TemplateType templateTypeDTOToBean(TemplateTypeDTO templateTypeDTO); 
	
	List<ProgramParentDTO> programParentToDTOs(List<ProgramParent>programParents);
	List<ProgramParent> programParentDTOsToBeans(List<ProgramParentDTO>programParentDTOs);
	
    List<ProgramDTO> programsToDTOs(List<Program>programs);
    List<Program> programDTOsToBeans(List<ProgramDTO>programDTOs);
}
