package orange.omn.web.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import orange.omn.bean.RefCategory;
import orange.omn.bean.RefChannel;
import orange.omn.bean.RefCommunity;
import orange.omn.bean.RefDefaultAudioTrack;
import orange.omn.bean.RefDefaultAudioTrackPhase;
import orange.omn.bean.RefDefaultAudioTrackService;
import orange.omn.bean.RefPhase;
import orange.omn.bean.RefPriority;
import orange.omn.bean.RefTemplateItem;
import orange.omn.bean.Service;
import orange.omn.web.dto.RefCategoryDTO;
import orange.omn.web.dto.RefChannelDTO;
import orange.omn.web.dto.RefCommunityDTO;
import orange.omn.web.dto.RefDefaultAudioTrackDTO;
import orange.omn.web.dto.RefDefaultAudioTrackPhaseDTO;
import orange.omn.web.dto.RefDefaultAudioTrackServiceDTO;
import orange.omn.web.dto.RefPhaseDTO;
import orange.omn.web.dto.RefPriorityDTO;
import orange.omn.web.dto.RefTemplateItemDTO;
import orange.omn.web.dto.ServiceDTO;

@Mapper(componentModel = "spring")
public interface IReferencesMapper {

	// IProgramParentMapper INSTANCE =
	// Mappers.getMapper(IProgramParentMapper.class);

	RefPriorityDTO refPriorityToDTO(RefPriority refPriorityEntity);

	RefPriority refPriorityDTOToBean(RefPriorityDTO refPriorityDTO);

	List<RefPriorityDTO> refPriorityToDTOs(List<RefPriority> refPriority);

	List<RefPriority> refPriorityDTOsToBeans(List<RefPriorityDTO> refPriorityDTOs);

	RefCommunityDTO refCommunityToDTO(RefCommunity refCommunity);

	RefCommunity refCommunityDTOToBean(RefCommunityDTO refCommunityDTO);

	List<RefCommunityDTO> refCommunityToDTOs(List<RefCommunity> refCommunity);

	List<RefCommunity> refCommunityDTOsToBeans(List<RefCommunityDTO> refCommunityDTOs);

	RefCategoryDTO refCategoryToDTO(RefCategory refCategory);

	RefCategory RefCategoryDTOToBean(RefCategoryDTO refCategoryDTO);

	List<RefCategoryDTO> refCategoryToDTOs(List<RefCategory> refCategory);

	List<RefCategory> refCategoryDTOsToBeans(List<RefCategoryDTO> refCategoryDTOs);

	RefPhaseDTO refPhaseToDTO(RefPhase refPhase);

	RefPhase RefPhaseDTOToBean(RefPhaseDTO refPhaseDTO);

	List<RefPhaseDTO> refPhaseToDTOs(List<RefPhase> refPhase);

	List<RefPhase> refPhaseDTOsToBeans(List<RefPhaseDTO> refPhaseDTOs);

	RefTemplateItemDTO refTemplateItemToDTO(RefTemplateItem refTemplateItem);

	RefTemplateItem refTemplateItemDTOToBean(RefTemplateItemDTO refTemplateItemDTO);

	List<RefTemplateItemDTO> refTemplateItemToDTOs(List<RefTemplateItem> refTemplateItem);

	List<RefTemplateItem> refTemplateItemDTOsToBeans(List<RefTemplateItemDTO> refTemplateItemDTOs);

	Service refServiceDTOToBean(ServiceDTO serviceDTO);

	ServiceDTO refServiceToDTO(Service service);

	List<ServiceDTO> refServiceToDTOs(List<Service> services);

	List<Service> refServiceDTOsToBeans(List<ServiceDTO> serviceDTOs);

	RefChannelDTO refChannelToDTO(RefChannel refChannel);

	RefChannel refChannelDTOToBean(RefChannelDTO refChannelDTO);
	
	// Before mapping entities and DTO, as they are other object's attributes, we first need to map them as object themselves
	RefDefaultAudioTrackPhaseDTO refDefaultAudioTrackPhaseToDTO(RefDefaultAudioTrackPhase refDefaultAudioTrackPhaseEntity);
	
	RefDefaultAudioTrackPhase refDefaultAudioTrackPhaseDTOToBean(RefDefaultAudioTrackPhaseDTO refDefaultAudioTrackPhaseDTO);

    List<RefDefaultAudioTrackPhaseDTO> refDefaultAudioTrackPhaseToDTOs(List<RefDefaultAudioTrackPhase> refDefaultAudioTracks);

    List<RefDefaultAudioTrackPhase> refDefaultAudioTrackPhaseDTOToBeans(List<RefDefaultAudioTrackPhaseDTO> refDefaultAudioTrackDTOs);
	
	RefDefaultAudioTrackServiceDTO refDefaultAudioTrackServiceToDTO(RefDefaultAudioTrackService refDefaultAudioTrackServiceEntity);
	
	RefDefaultAudioTrackService refDefaultAudioTrackServiceDTOToBean(RefDefaultAudioTrackServiceDTO refDefaultAudioTrackServiceDTO);

	@Mappings({ @Mapping(target = "refDefATPhaseDTO", source = "refDefaultAudioTrack.refDefaultAudioTrackPhase"),
			@Mapping(target = "refDefATServiceDTO", source = "refDefaultAudioTrack.refDefaultAudioTrackService") })
	RefDefaultAudioTrackDTO refDefaultAudioTrackToDTO(RefDefaultAudioTrack refDefaultAudioTrack);

	@Mappings({ @Mapping(target = "refDefaultAudioTrackPhase", source = "refDefaultAudioTrackDTO.refDefATPhaseDTO"),
			@Mapping(target = "refDefaultAudioTrackService", source = "refDefaultAudioTrackDTO.refDefATServiceDTO") })
	RefDefaultAudioTrack refDefaultAudioTrackDTOToBean(RefDefaultAudioTrackDTO refDefaultAudioTrackDTO);

	List<RefDefaultAudioTrackDTO> refDefaultAudioTrackToDTOs(List<RefDefaultAudioTrack> refDefaultAudioTracks);

	List<RefDefaultAudioTrack> refDefaultAudioTrackDTOToBeans(List<RefDefaultAudioTrackDTO> refDefaultAudioTrackDTOs);

}
