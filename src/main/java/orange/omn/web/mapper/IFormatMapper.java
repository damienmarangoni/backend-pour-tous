package orange.omn.web.mapper;

import org.mapstruct.Mapper;

import orange.omn.bean.Format;
import orange.omn.web.dto.FormatDTO;

@Mapper(componentModel = "spring")
public interface IFormatMapper {
	
    FormatDTO formatToDTO(Format format);
    Format formatDTOToBean(FormatDTO formatDTO); 

}
