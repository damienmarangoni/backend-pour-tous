package orange.omn.web.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import orange.omn.bean.QualificationExpression;
import orange.omn.bean.RefCategory;
import orange.omn.bean.Service;
import orange.omn.web.dto.QualificationExpressionDTO;
import orange.omn.web.dto.RefCategoryDTO;
import orange.omn.web.dto.ServiceDTO;

@Mapper(componentModel = "spring")
public interface IQualificationExpressionMapper {

	QualificationExpression qualifExpressDTOToBean(QualificationExpressionDTO qualExpr);
	QualificationExpressionDTO qualificationExpressToDTO(QualificationExpression qualExpr);


	List<QualificationExpression> qualifExpressDTOToBean(List<QualificationExpressionDTO> qualExpr);
	List<QualificationExpressionDTO> qualificationExpressListToDTO(List<QualificationExpression> qualExpr);
	
	ServiceDTO serviceToDTO(Service service);
	Service serviceDTOToBean(ServiceDTO serviceDTO);

	RefCategoryDTO refCategoryToDTO(RefCategory refCategory);
	RefCategory refCategoryDTOToBean(RefCategoryDTO refCategoryDTO);

}
