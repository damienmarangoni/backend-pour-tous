package orange.omn.web.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import orange.omn.bean.Format;
import orange.omn.bean.Template;
import orange.omn.bean.TemplateContent;
import orange.omn.bean.TemplateType;
import orange.omn.web.dto.FormatDTO;
import orange.omn.web.dto.TemplateContentDTO;
import orange.omn.web.dto.TemplateDTO;
import orange.omn.web.dto.TemplateTypeDTO;

@Mapper(componentModel = "spring")
public interface ITemplateMapper {
	
	//IProgramParentMapper INSTANCE  = Mappers.getMapper(IProgramParentMapper.class);
    
 
  TemplateDTO templateToDTO(Template templateEntity); 
  Template templateDTOToBean(TemplateDTO templateDTO);
  
  FormatDTO formatToDTO(Format format);
  Format formatDTOToBean(FormatDTO formatDTO); 
  
  TemplateTypeDTO templateTypeToDTO(TemplateType templateType);
  TemplateType templateTypeDTOToBean(TemplateTypeDTO templateTypeDTO); 
  
  TemplateContentDTO templateContentToDTO(TemplateContent template);
  TemplateContent templateContentDTOToBean(TemplateContentDTO templateDTO);

  List<TemplateDTO> templateToDTOs(List<Template> template);
  List<Template> templateDTOsToBeans(List<TemplateDTO> templateDTOs);
	

}
