package orange.omn.web.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import orange.omn.bean.Mask;
import orange.omn.web.dto.MaskDTO;

@Mapper(componentModel = "spring")
public interface IMaskMapper {
	
	//IProgramParentMapper INSTANCE  = Mappers.getMapper(IProgramParentMapper.class);
    
  MaskDTO maskToDTO(Mask maskEntity); 
  Mask maskDTOToBean(MaskDTO maskDTO);
  
  List<MaskDTO> maskToDTOs(List<Mask> mask);
  List<Mask> maskDTOsToBeans(List<MaskDTO> maskDTOs);
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
