package orange.omn.web.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import orange.omn.bean.ContractCustomer;
import orange.omn.bean.RefChannel;
import orange.omn.bean.TemplateContent;
import orange.omn.web.dto.ContractCustomerDTO;
import orange.omn.web.dto.RefChannelDTO;
import orange.omn.web.dto.TemplateContentDTO;

@Mapper(componentModel = "spring")
public interface ITemplateContentMapper {

	// IProgramParentMapper INSTANCE =
	// Mappers.getMapper(IProgramParentMapper.class);

	@Mappings({ @Mapping(target = "contractCustomerDTO", source = "templateContentEntity.contractCustomer"),
			@Mapping(target = "refChannelDTO", source = "templateContentEntity.refChannel") })
	TemplateContentDTO templateContentToDTO(TemplateContent templateContentEntity);

	@Mappings({ @Mapping(target = "contractCustomer", source = "templateContentDTO.contractCustomerDTO"),
			@Mapping(target = "refChannel", source = "templateContentDTO.refChannelDTO") })
	TemplateContent templateContentDTOToBean(TemplateContentDTO templateContentDTO);

	List<TemplateContentDTO> templateContentToDTOs(List<TemplateContent> templateContent);

	List<TemplateContent> templateContentDTOsToBeans(List<TemplateContentDTO> templateContentDTOs);

	RefChannelDTO refChannelToDTO(RefChannel refChannel);

	RefChannel refChannelDTOToBean(RefChannelDTO refChannelDTO);
	
	ContractCustomerDTO constractCustomerToDTO(ContractCustomer contractCustomer);
	
	ContractCustomer constractCustomerDTOToBean(ContractCustomerDTO contractCustomerDTO);
	
	List<ContractCustomerDTO> constractCustomerToDTOs(List<ContractCustomer> contractCustomers);
	
	List<ContractCustomer> constractCustomerDTOToBeans(List<ContractCustomerDTO> contractCustomerDTOs);

}
