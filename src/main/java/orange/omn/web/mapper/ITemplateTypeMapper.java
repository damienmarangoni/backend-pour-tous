package orange.omn.web.mapper;

import org.mapstruct.Mapper;

import orange.omn.bean.TemplateType;
import orange.omn.web.dto.TemplateTypeDTO;

@Mapper(componentModel = "spring")
public interface ITemplateTypeMapper {
	
	//IProgramParentMapper INSTANCE  = Mappers.getMapper(IProgramParentMapper.class);
    
 
  TemplateTypeDTO templateTypeToDTO(TemplateType templateType); 
  TemplateType templateTypeDTOToBean(TemplateTypeDTO templateTypeDTO);

}
