package orange.omn.web.mapper;

import org.mapstruct.Mapper;

import orange.omn.bean.Service;
import orange.omn.web.dto.ServiceDTO;

@Mapper(componentModel = "spring")
public interface IServiceMapper {
	
	ServiceDTO serviceToDTO(Service serviceEntity);
	Service serviceDTOToBean(ServiceDTO serviceDTO); 

}
