package orange.omn.web.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import orange.omn.bean.Campaign;
import orange.omn.bean.CustomerQualification;
import orange.omn.bean.Format;
import orange.omn.bean.Location;
import orange.omn.bean.Program;
import orange.omn.bean.ProgramParent;
import orange.omn.bean.QualificationExpression;
import orange.omn.bean.RefCategory;
import orange.omn.bean.RefPriority;
import orange.omn.bean.Service;
import orange.omn.bean.Template;
import orange.omn.bean.TemplateContent;
import orange.omn.bean.TemplateType;
import orange.omn.web.dto.CampaignDTO;
import orange.omn.web.dto.CustomerRouteDTO;
import orange.omn.web.dto.FormatDTO;
import orange.omn.web.dto.LocationDTO;
import orange.omn.web.dto.ProgramDTO;
import orange.omn.web.dto.ProgramParentDTO;
import orange.omn.web.dto.QualificationExpressionDTO;
import orange.omn.web.dto.RefCategoryDTO;
import orange.omn.web.dto.RefPriorityDTO;
import orange.omn.web.dto.ServiceDTO;
import orange.omn.web.dto.TemplateContentDTO;
import orange.omn.web.dto.TemplateDTO;
import orange.omn.web.dto.TemplateTypeDTO;

@Mapper(componentModel = "spring")
public interface ICustomerQualificationMapper {
	
//	ICustomerQualificationMapper INSTANCE  = Mappers.getMapper(ICustomerQualificationMapper.class);
	
	@Mappings({
	      @Mapping(target="programParentDTOs", source="custQualifEntity.programParents"),
	      @Mapping(target="qualificationExpressionsDTOs", source="custQualifEntity.qualificationExpressions")
	    })
	
	CustomerRouteDTO customerQualificationToDTO(CustomerQualification custQualifEntity);
	
	
	@Mappings({
	      @Mapping(target="programParents", source="customerQualificationDTO.programParentDTOs"),
	      @Mapping(target="qualificationExpressions", source="customerQualificationDTO.qualificationExpressionsDTOs")
	    })
	CustomerQualification customerQualificationDTOToBean(CustomerRouteDTO customerQualificationDTO);
	CustomerQualification customerQualificationDTOToBean(CustomerRouteDTO fullCustRoute, String id);
	
	ProgramParentDTO programParentToDTO(ProgramParent progarmParent);
	ProgramParent programParentDTOToBean(ProgramParentDTO progarmParentDTO);
	
	QualificationExpressionDTO qualificationExpressionToDTO(QualificationExpression qualificationExpression);
	QualificationExpression qualificationExpressionToBean(QualificationExpressionDTO qualificationExpressionDTO);	
	
	CampaignDTO campaignToDTO(Campaign campaign);
	Campaign campaignDTOToBean(CampaignDTO campaignDTO);
    
    TemplateDTO templateToDTO(Template template);
    Template templateDTOToBean(TemplateDTO templateDTO);
    
    ProgramDTO programToDTO(Program program);
    Program programDTOToBean(ProgramDTO programDTO);
	
	LocationDTO locationToDTO(Location location); 
	Location locationDTOToBean(LocationDTO locationDTO);
    
    ServiceDTO serviceToDTO(Service service);
    Service serviceDTOToBean(ServiceDTO serviceDTO);
       
    RefCategoryDTO refCategoryToDTO(RefCategory refCategory);
    RefCategory refCategoryDTOToBean(RefCategoryDTO refCategoryDTO);
    
    RefPriorityDTO refPriorityToDTO(RefPriority refPriority);
    RefPriority refPriorityDTOToBean(RefPriorityDTO refPriorityDTO);
   
    TemplateContentDTO templateContentToDTO(TemplateContent templateContent);
    TemplateContent templateContentDTOToBean(TemplateContentDTO templateContentDTO);
    
    FormatDTO formatToDTO(Format format);
    Format formatDTOToBean(FormatDTO formatDTO); 
    
    
    TemplateTypeDTO templateTypeToDTO(TemplateType templateType);
    TemplateType templateTypeDTOToBean(TemplateTypeDTO templateTypeDTO); 
	

	List<CustomerRouteDTO> customerQualificationToDTOs(List<CustomerQualification> customerQualifications);
	List<CustomerQualification> customerQualifiatcionDTOstoBeans(List<CustomerRouteDTO> customerQualifaictionDTOs);
	
	List<ProgramParentDTO> programParentsToDTOs(List<ProgramParent> programParents);
	List<ProgramParent> programParentDTOsToBeans(List<ProgramParentDTO> programParentDTOs);
    
    List<ProgramDTO> programsToDTOs(List<Program> programs);
    List<Program> programDTOsToBeans(List<ProgramDTO> programDTOs);


	

}
