package orange.omn.common;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

//import fr.orange.omni.core.abstraction.service.ServiceException;
//import fr.orange.omni.core.common.CodeErrorMessage;
//import fr.orange.omni.core.common.Constante;
//import fr.orange.omni.core.common.Log;

public final class AudioUtils {

	/**
	 * Classe utilitaire : constructeur privé
	 */
	private AudioUtils() {
	}     

	/**
	 * Convertit un fichier wav au format a8k avec sox et l'enregistre dans le
	 * dossier filePath sous le nom fileName.
	 * 
	 * @param filePath
	 *            Chemin de destination
	 * @param filename
	 *            Nom du fichier de destination
	 * @param is
	 *            InputStream contenant le fichier original
	 * @return true si tout s'est bien passé
	 * @throws IOException
	 *             Exception d'entrée sortie
	 * @throws FileNotFoundException
	 *             Fichier non trouv�
	 */
	public static boolean convertAndSaveFileA8K(final String filePath,
			final String fileName, final InputStream is, String soxCmd) throws IOException,
			FileNotFoundException {
		boolean res = true;
		final String filename = fileName.replaceAll("\\.\\w{3}$", "");
		if (is != null) {

			final String in_wav = filePath + filename + Constante.WAV_EXTENSION;
			final String out_a8k = filePath + filename
					+ Constante.DIALOG_AUDIO_FILE_EXT;
			String converted_wav;

			// Ecriture du fichier dans un fichier wav
			final FileOutputStream fos = new FileOutputStream(in_wav);
			int lg = 0;
			try {
				// On utilise un tableau comme buffer
				final byte[] buf = new byte[8192];
				// Et on utilise une variable pour connaitre le nombre
				// de bytes lus, et donc le nombres qu'il faudra écrire :
				int c;
				while ((c = is.read(buf)) >= 0) {
					fos.write(buf, 0, c);
					lg += c;
				}

			} finally {
				// On ferme le fichier quoi qu'il arrive :
				fos.flush();
				fos.close();
			}

//			Log.printInfo(AudioUtils.class, "saveFile", "Saving file: "
//					+ in_wav + " de taille: " + lg + " byte(s)");

            InputStream bufferedIn = new BufferedInputStream(is);
			if (!AudioUtils.isA8K(bufferedIn)) {
				final String out_wav_convert = filePath + filename + "_temp"
						+ Constante.WAV_EXTENSION;

				// Conversion a8k
				String cmd = soxCmd + Constante.COMMANDE_SOX;
				cmd = cmd.replaceAll(Constante.AUDIO_NAME_SERV_ID, in_wav);
				cmd = cmd
						.replaceAll(Constante.FILE_CONVERT_ID, out_wav_convert);
				try {
					if (Utils.run(cmd, true) != 0) {
						res = false;
//						Log.printErreur(AudioUtils.class, "saveFile",
//								CodeErrorMessage.exception,
//								"conversion du fichier " + in_wav
//										+ " en erreur");
					}
				} catch (final Exception e) {

					// Normal sous windows
					// sous linux ajouté le problème
					res = false;
//					Log.printErreur(AudioUtils.class, "saveFile",
//							CodeErrorMessage.exception,
//							"conversion du fichier " + in_wav + " impossible",
//							e);
				}

				// Suppression du fichier original
				if (res) {
					final File fileWav = new File(in_wav);
					res &= fileWav.delete();
					if (res) {
//						Log.printInfo(AudioUtils.class, "saveFile",
//								"Suppression du fichier: " + in_wav);
					} else {
//						Log.printInfo(AudioUtils.class, "saveFile",
//								"Suppression du fichier: " + in_wav + " KO");
					}
				}

				converted_wav = out_wav_convert;
			} else {
				converted_wav = in_wav;
			}

			if (res) {
				final File fileWav = new File(converted_wav);
				final File dest = new File(out_a8k);
				if (dest.exists()) {
					dest.delete();
				}
				res &= fileWav.renameTo(dest);
				if (res) {
//					Log.printInfo(AudioUtils.class, "saveFile",
//							"Renommage du fichier: " + converted_wav + " en "
//									+ out_a8k);
				} else {
//					Log.printInfo(AudioUtils.class, "saveFile",
//							"Renommage du fichier: " + converted_wav + " en "
//									+ out_a8k + " KO");
				}
			}

		}

		return res;
	}

	public static boolean isA8K(final InputStream in) {
		try {
			if (in.available() > 0) {
				final AudioInputStream streamAudio = AudioSystem
						.getAudioInputStream(in);
				final AudioFormat formatAudio = streamAudio.getFormat();
				streamAudio.close();

				return formatAudio.getChannels() == 1
						&& formatAudio.getSampleSizeInBits() == 8
						&& formatAudio.getSampleRate() == 8000.0
						&& formatAudio.getEncoding() == AudioFormat.Encoding.ALAW;
			} else {
				return false;
			}
		} catch (final UnsupportedAudioFileException e) {
//			Log.printWarning(AudioUtils.class, "validMediaType",
//					"Format sonore non supporté", ".", e);
			return false;
		} catch (final IOException e) {
//			Log.printWarning(AudioUtils.class, "validMediaType",
//					"Erreur de lecture/écriture", ".", e);
			return false;
		} finally {

		}
	}

	/**
	 * Déclenche la lecture d'un fichier son sur le flux HTTP
	 * 
	 * @param response
	 *            Réponse Servlet HTTP
	 * @param listenFile
	 *            Fichier à diffuser
	 * @param nomFichierWav
	 *            Nom du fichier à afficher à l'utilisateur
	 * @throws IOException
	 *             Exception d'entrée-sortie
	 */
	public static boolean listenHttpFile(final HttpServletResponse response,
			final File listenFile, String nomFichierWav, String soxCmd) throws IOException {

		final InputStream is = new FileInputStream(listenFile);

		final byte[] message;
		if (!AudioUtils.isA8K(is)) {
			message = AudioUtils.convertToWav(listenFile, soxCmd);
			nomFichierWav += ".wav";
		} else {
			message = Utils.getBytesFromFile(listenFile);
		}

		if (message != null) {
			response.setContentType("audio/x-wav");
			response.setContentLength(message.length);

			response.setHeader("Content-Disposition", "inline; filename=\""
					+ nomFichierWav + "\"");

			response.setHeader("Accept-Ranges", "bytes");

			final ServletOutputStream sos = response.getOutputStream();
			sos.write(message);
			sos.flush();
			sos.close();

			return true;
		} else {
			return false;
		}
	}
	
	private static byte[] convertToWav(final File file, String soxCmd) throws IOException {
		boolean res = true;

		final String input = file.getAbsolutePath().replaceAll("\\\\", "/");
		final String output = input + ".wav";

		// Conversion wav
		String cmd = soxCmd + Constante.COMMANDE_SOX_TO_WAV;
		cmd = cmd.replaceAll(Constante.FILE_CONVERT_ID, input);
		cmd = cmd.replaceAll(Constante.AUDIO_NAME_SERV_ID, output);
		try {
			if (Utils.run(cmd, true) != 0) {
				res = false;
//				Log.printErreur(AudioUtils.class, "saveFile",
//						CodeErrorMessage.exception, "conversion du fichier "
//								+ input + " en erreur");
			}
		} catch (final Exception e) {
			// Normal sous windows
			// sous linux ajouté le problème
			res = false;
//			Log.printErreur(AudioUtils.class, "saveFile",
//					CodeErrorMessage.exception, "conversion du fichier "
//							+ input + " impossible", e);
		}

		if (res) {
			final File fileWav = new File(output);
			final byte[] bytes = Utils.getBytesFromFile(fileWav);
			fileWav.delete();
			return bytes;
		}

		return null;
	}
}
