package orange.omn.common;

final public class Constante {

	final public static String SYS_LINUX = "linux";

	final public static String TYPE_OS = System.getProperty("os.name").toLowerCase();

	final public static String FILE_SEPARATOR = System.getProperty("file.separator");

	final public static String VIDE = "";

	final public static String BLANC = " ";

	final public static int NA = -1;

	final public static int INSERT = 0;

	final public static int UPDATE = 1;

	public static String PATH_CONF = ".";

	/*
	 * *********************************************** ************** FICHIER *
	 * AUDIO ****************** ***********************************************
	 */

	public static String AUDIO_FILE_PATH;

	public static String AUDIO_DEFAULT_FILE_PATH;

	public static String AUDIO_TMP_FILE_PATH;

	final public static String DIALOG_AUDIO_FILE_EXT = ".a8k";

	final public static String WAV_EXTENSION = ".wav";

	final public static String AUDIO_NAME_SERV_ID = "nomFichierServeur";

	final public static String FILE_CONVERT_ID = "nomFichierConverti";

	// Version >=14 de sox : remplacer -b par -1.
	final public static String COMMANDE_SOX = "sox -t wav " + Constante.AUDIO_NAME_SERV_ID + " -t raw -A -c 1 -r 8000 " + Constante.FILE_CONVERT_ID;

	final public static String COMMANDE_SOX_TO_WAV = "sox -t al -c 1 -r 8000 " + Constante.FILE_CONVERT_ID + " -a " + Constante.AUDIO_NAME_SERV_ID;

	/*
	 * Champs GASSI
	 */

	final public static String GASSI_SESSION_ID = "sm_serversessionid";

	final public static String GASSI_SESS_SPEC = "sm_serversessionspec";

	final public static String GASSI_EXPIRE_SESS = "sm_timetoexpire";

	final public static String GASSI_ID = Constante.GASSI_SESSION_ID;

}
