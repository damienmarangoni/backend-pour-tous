package orange.omn.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


final public class Utils {

	private static Utils singl;

	private Utils() {

	}

	public static Utils getInstance() {
		if (Utils.singl == null) {
			Utils.singl = new Utils();
		}

		return Utils.singl;
	}

	public boolean deleteFile(final String filePath, final String filename) {
		boolean res = false;

		final String file_name = filePath + filename;

		final File f = new File(file_name);
		if (f.exists()) {
			res = f.delete();
		}

		return res;
	}

	public boolean renameFile(final String filePath, final String filename, final String destfilePath, final String destfilename) {
		boolean res = false;

		final String file_name = filePath + filename;
		final String dest_file_name = destfilePath + destfilename;

		final File oldFile = new File(file_name);
		if (oldFile.exists()) {
			final File newFile = new File(dest_file_name);
			if (oldFile.canRead() && (!newFile.exists() || newFile.canWrite())) {
				if (!(res = oldFile.renameTo(newFile))) {
//					Log.printInfo(this, "saveOrUpdateForm", "Erreur renommage : Le fichier " + oldFile.getAbsolutePath() + " n'a pas été renommé en "
//							+ newFile.getAbsolutePath());
				}
			} else if (!oldFile.canRead()) {
//				Log.printInfo(this, "saveOrUpdateForm", "Erreur renommage : Le fichier " + oldFile.getAbsolutePath() + " n'est pas lisible");
			} else {
//				Log.printInfo(this, "saveOrUpdateForm", "Erreur renommage : Le fichier " + newFile.getAbsolutePath() + " n'est pas inscriptible");
			}
		} else {
//			Log.printInfo(this, "saveOrUpdateForm", "Erreur renommage : Le fichier " + oldFile.getAbsolutePath() + " n'existe pas");
		}

		return res;
	}

	static int run(final String cmd, final boolean wait) throws Exception {
		try {
			String[] args = null;

			if (Constante.SYS_LINUX.equals(Constante.TYPE_OS)) {
				args = new String[] { "/bin/sh", "-c", cmd };

			} else {
				args = new String[] { "cmd.exe", "/C", cmd };
			}

//			Log.printInfo(Utils.class, "run", args[0] + " " + args[1] + " " + args[2]);
			final Runtime r = Runtime.getRuntime();
			final Process p = r.exec(args);
			final InputStream in = p.getInputStream();
			final InputStream err = p.getErrorStream();

			if (wait) {
				p.waitFor();// si l'application doit attendre a ce que ce
			}
			// process
			// fini
			int c = -1;

			String res = "";

			while ((c = err.read()) != -1) {
				res += (char) c + "";
			}

//			Log.printInfo(Utils.class, "run", "retour d'erreur cmd: " + res);

			res = "";

			while ((c = in.read()) != -1) {
				res += (char) c + "";
			}

//			Log.printInfo(Utils.class, "run", "retour de cmd: " + res);

			in.close();

			final int exitValue = p.exitValue();
//			Log.printInfo(Utils.class, "run", "code de retour: " + exitValue);

			p.destroy();
//			Log.printInfo(Utils.class, "run", ">>Fin du RUN<<");

			return exitValue;
		} catch (final InterruptedException ex) {
			throw new Exception(ex.getLocalizedMessage());
		} catch (final IOException ioex) {
			throw new Exception(ioex.getLocalizedMessage());
		}
	}

	// Returns the contents of the file in a byte array.
	static byte[] getBytesFromFile(final File file) throws IOException {
		final InputStream is = new FileInputStream(file);

		// Get the size of the file
		final long length = file.length();

		// You cannot create an array using a long type.
		// It needs to be an int type.
		// Before converting to an int type, check
		// to ensure that file is not larger than Integer.MAX_VALUE.
		if (length > Integer.MAX_VALUE) {
			// File is too large
			return null;
		}

		// Create the byte array to hold the data
		final byte[] bytes = new byte[(int) length];

		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		// Ensure all the bytes have been read in
		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}

		// Close the input stream and return bytes
		is.close();
		return bytes;
	}
}