package orange.omn.common;

public enum RefChanEnum {
	ECRIT(1, "Ecrit"), 
	VOCAL(2, "Vocal");
	
	private int channelId;
	private String channelLib;
	
	RefChanEnum(int id, String lib) {
		this.channelId = id;
		this.channelLib = lib;
		
	}

	public int getChannelId() {
		return channelId;
	}

	public String getChannelLib() {
		return channelLib;
	}


}
