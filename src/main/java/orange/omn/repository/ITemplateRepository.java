/**
 * 
 */
package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import orange.omn.bean.Template;

/**
 * @author XT0087920
 *
 */
public interface ITemplateRepository extends JpaRepository<Template, String>,  JpaSpecificationExecutor<Template> {


    public Template findByIdTemplate(int idTemplate);
    
    public Template findByTemplateName(String templateName);
	

}
