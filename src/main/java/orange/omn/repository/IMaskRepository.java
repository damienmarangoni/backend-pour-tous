/**
 * 
 */
package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import orange.omn.bean.Mask;

/**
 * @author XT0087920
 *
 */
public interface IMaskRepository extends JpaRepository<Mask, String>,  JpaSpecificationExecutor<Mask> {


    public Mask findByMaskMaskedNb(String maskMaskedNb);
    
    public Mask findByMaskPublicNb(String maskPublicNb);
	

}
