package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import orange.omn.bean.RefDefaultAudioTrackPhase;

public interface IRefDefaultAudioTrackPhaseListRepository extends JpaRepository<RefDefaultAudioTrackPhase, String>,  JpaSpecificationExecutor<RefDefaultAudioTrackPhase> {

}
