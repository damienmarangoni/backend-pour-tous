/**
 * 
 */
package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import orange.omn.bean.CustomerQualification;
import orange.omn.bean.ProgramParent;
import orange.omn.bean.ProgramParentActivity;

/**
 * @author XT0087920
 *
 */
public interface IProgramParentActivityRepository extends JpaRepository<ProgramParentActivity, String>,  JpaSpecificationExecutor<ProgramParentActivity> {
	   
    public ProgramParentActivity findByIdProgramParent(int idProgramParent);
}
