/**
 * 
 */
package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import orange.omn.bean.RefActivity;

/**
 * @author XT0087920
 *
 */
public interface IRefActivityRepository extends JpaRepository<RefActivity, String>,  JpaSpecificationExecutor<RefActivity> {
	
    
    @Query(value  ="SELECT MIN(codeActivity) FROM RefActivity")
    public String retrieveMinId();
}
