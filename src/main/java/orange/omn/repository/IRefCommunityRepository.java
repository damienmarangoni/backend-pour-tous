/**
 * 
 */
package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import orange.omn.bean.RefCommunity;

/**
 * @author XT0087920
 *
 */
public interface IRefCommunityRepository extends JpaRepository<RefCommunity, String>,  JpaSpecificationExecutor<RefCommunity> {
	

}
