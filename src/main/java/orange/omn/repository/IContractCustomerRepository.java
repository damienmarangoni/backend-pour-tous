package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import orange.omn.bean.ContractCustomer;

public interface IContractCustomerRepository extends JpaRepository<ContractCustomer, String>,  JpaSpecificationExecutor<ContractCustomer>{

}
