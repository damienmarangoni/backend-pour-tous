/**
 * 
 */
package orange.omn.repository;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import orange.omn.bean.CustomerQualification;

/**
 * @author XT0087920
 *
 */
public interface ICustomerRouteRepository extends JpaRepository<CustomerQualification, String>,  JpaSpecificationExecutor<CustomerQualification> {
	
	@Query(value  ="SELECT DISTINCT cq FROM CustomerQualification cq "
	             + "RIGHT JOIN cq.programParents pp "
	             + "WHERE pp.dateStart <= :endDate "
	             +    "AND pp.dateEnd >= :startDate")
	public Page<CustomerQualification> retrieveByStartDateAndEndDate(@Param("startDate") LocalDate startDate, @Param("endDate")LocalDate endDate,  @Param("pageable")Pageable pageable );

    @Query(value  ="SELECT DISTINCT cq FROM CustomerQualification cq "
            + "LEFT JOIN cq.programParents pp "
            + "WHERE pp.customerQualification is null")
    public Page<CustomerQualification> retrieve(@Param("pageable")Pageable pageable );

    
    @Query(value  ="SELECT DISTINCT cq FROM CustomerQualification cq "
                 + "JOIN cq.programParents pp "
                 + "JOIN cq.qualificationExpressions qe "
                 + "WHERE pp.template.codePhase = :codePhase"
                 + "  AND pp.dateStart <= :date"
                 + "  AND pp.dateEnd >= :date"
                 + "  AND qe.service.serviceDu like :serviceDu"
                 + "  AND qe.refCategory.libCategory like :libCategory"
                 + "  AND qe.communityFilter like :communityFilter")
    public Page<CustomerQualification> retrieveForSimulator(@Param("codePhase") String codePhase, 
            @Param("date") LocalDate date,
            @Param("serviceDu") String serviceDu,
            @Param("libCategory") String libCategory,
            @Param("communityFilter") int communityFilter,
            @Param("pageable")Pageable pageable);
        
    @Query(value  ="SELECT DISTINCT cq FROM CustomerQualification cq "
                + "JOIN cq.programParents pp "
                + "JOIN cq.qualificationExpressions qe "
                + "WHERE pp.template.codePhase = :codePhase"
                + "  AND pp.dateStart <= :date"
                + "  AND pp.dateEnd >= :date"
                + "  AND qe.service.serviceDu like :serviceDu"
                + "  AND qe.refCategory.libCategory like :libCategory")
    public Page<CustomerQualification> retrieveForSimulator(@Param("codePhase") String codePhase, 
           @Param("date") LocalDate date,
           @Param("serviceDu") String serviceDu,
           @Param("libCategory") String libCategory,
           @Param("pageable")Pageable pageable);
    
    @Query(value  ="SELECT DISTINCT cq FROM CustomerQualification cq "
            + "JOIN cq.qualificationExpressions qe "
            + "WHERE qe.service.serviceDu = :serviceDu"
            + "  AND qe.refCategory.libCategory = :libCategory"
            + "  AND qe.communityFilter = :communityFilter")
    public Page<CustomerQualification> retrieveDefaultForSimulator(@Param("serviceDu") String serviceDu,
           @Param("libCategory") String libCategory,
           @Param("communityFilter") int communityFilter,
           @Param("pageable")Pageable pageable);
    
    @Query(value  ="SELECT DISTINCT cq FROM CustomerQualification cq "
            + "JOIN cq.qualificationExpressions qe "
            + "WHERE qe.service.serviceDu = :serviceDu"
            + "  AND qe.refCategory.libCategory = :libCategory")
    public Page<CustomerQualification> retrieveDefaultForSimulator(@Param("serviceDu") String serviceDu,
           @Param("libCategory") String libCategory,
           @Param("pageable")Pageable pageable);

	
//	@Query(value  ="SELECT  cq FROM CustomerQualification cq JOIN cq.programParents pp "
//			+ "WHERE EXTRACT(YEAR from pp.dateStart) = EXTRACT(YEAR from:startDate)")
//	public Page<CustomerQualification> retrieveByStartDate(@Param("startDate") LocalDate startDate, @Param("pageable")Pageable pageable );
	
	@Query(value  ="SELECT DISTINCT cq FROM CustomerQualification cq JOIN cq.programParents pp WHERE pp.dateStart >= :startDate")
	public Page<CustomerQualification> retrieveByStartDate(@Param("startDate") LocalDate startDate, @Param("pageable")Pageable pageable);
	
	public CustomerQualification findByCustomerQualificationName (String customerQualificationName);

	public CustomerQualification findByCustomerQualificationId(int idCustomerQualification);

	public void deleteByCustomerQualificationId(int idCustomerQualification);
	

}
