package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import orange.omn.bean.RefDefaultAudioTrack;

public interface IRefDefaultAudioTrackListRepository extends JpaRepository<RefDefaultAudioTrack, String>,  JpaSpecificationExecutor<RefDefaultAudioTrack> {

}
