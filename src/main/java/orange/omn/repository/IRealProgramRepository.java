/**
 * 
 */
package orange.omn.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import orange.omn.bean.RealProgram;

/**
 * @author XT0087920
 *
 */
public interface IRealProgramRepository extends JpaRepository<RealProgram, String>,  JpaSpecificationExecutor<RealProgram> {
    
    @Query(value  ="SELECT rp "
            + "FROM RealProgram rp "
            + "WHERE rp.program.idProgram = :idProgram")   
    public List<RealProgram> findByIdProgram(@Param("idProgram") int idProgram);
}
