/**
 * 
 */
package orange.omn.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import orange.omn.bean.ProgramParent;

/**
 * @author XT0087920
 *
 */
public interface IProgramParentRepository extends JpaRepository<ProgramParent, String>,  JpaSpecificationExecutor<ProgramParent> {

    @Query(value  ="SELECT DISTINCT pp "
            + "FROM ProgramParent pp "
            + "WHERE pp.template.codePhase = :codePhase and pp.customerQualification.customerQualificationId = :customerQualificationId")
    Page<ProgramParent> retrieveByCodePhase(@Param("codePhase") String codePhase, 
            @Param("customerQualificationId") int customerQualificationId, 
            @Param("pageable") Pageable pageable);
    
//    @Query(value  ="SELECT DISTINCT pp "
//            + "FROM ProgramParent pp "
//            + "WHERE pp.template.codePhase = :codePhase and pp.customerQualification.customerQualificationId = :customerQualificationId")
//    Page<ProgramParent> retrieveForSimulator(@Param("codeService") String codeService,  
//            @Param("codeCommunity") String codeCommunity, 
//            @Param("codeCategory") String codeCategory,
//            @Param("codePhase") String codePhase, 
//            @Param("date") Date date, 
//            @Param("pageable") Pageable pageable);
    
    @Query(value  ="SELECT DISTINCT pp "
            + "FROM ProgramParent pp "
            + "WHERE pp.template.codePhase = :codePhase and pp.customerQualification.customerQualificationId = :customerQualificationId")
    ProgramParent retrieveByCodePhaseAndId(@Param("codePhase") String codePhase, 
            @Param("customerQualificationId") int customerQualificationId);    
    
    @Query(value  ="SELECT DISTINCT pp "
            + "FROM ProgramParent pp "
            + "WHERE pp.idProgramParent = :idProgramParent")
    ProgramParent retrieveById(@Param("idProgramParent") int idProgramParent);    

    @Query(value  ="SELECT DISTINCT pp "
            + "FROM ProgramParent pp "
            + "WHERE pp.customerQualification.customerQualificationId = :customerQualificationId")
    Page<ProgramParent> retrieveByCustomerQualifId(@Param("customerQualificationId") int customerQualificationId, 
            @Param("pageable") Pageable pageable);  
    
    @Query(value  ="DELETE FROM ProgramParent pp WHERE pp.idProgramParent = :idProgramParent")
    ProgramParent deleteProgramParent(@Param("idProgramParent") int idProgramParent);
   }
