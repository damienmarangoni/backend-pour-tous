/**
 * 
 */
package orange.omn.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import orange.omn.bean.TemplateContent;

/**
 * @author XT0087920
 *
 */
public interface ITemplateContentRepository extends JpaRepository<TemplateContent, String>,  JpaSpecificationExecutor<TemplateContent> {


    public TemplateContent findByTemplateContentId(String templateContentId);

    @Query(value  ="SELECT tc FROM TemplateContent tc, RefChannel rf where tc.refChannel = rf.idChannel and rf.idChannel = :channelId")
	public Page<TemplateContent> findByChannelId(Pageable pageable, @Param("channelId") int channelId);

    @Query(value  ="SELECT tc FROM TemplateContent tc, RefChannel rf where tc.refChannel = rf.idChannel and rf.idChannel = :channelId")
	public List<TemplateContent> findByChannelId( @Param("channelId") int channelId);
    
    public void deleteByTemplateContentId(String templateContentId);

}
