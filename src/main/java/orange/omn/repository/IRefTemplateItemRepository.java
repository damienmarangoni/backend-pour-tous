/**
 * 
 */
package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import orange.omn.bean.RefTemplateItem;

/**
 * @author XT0087920
 *
 */
public interface IRefTemplateItemRepository extends JpaRepository<RefTemplateItem, String>,  JpaSpecificationExecutor<RefTemplateItem> {

    public RefTemplateItem findByKeyword(String keyword);

	

}
