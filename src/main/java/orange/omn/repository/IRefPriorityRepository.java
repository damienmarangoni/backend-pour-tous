/**
 * 
 */
package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import orange.omn.bean.RefPriority;

/**
 * @author XT0087920
 *
 */
public interface IRefPriorityRepository extends JpaRepository<RefPriority, String>,  JpaSpecificationExecutor<RefPriority> {
	
}
