/**
 * 
 */
package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import orange.omn.bean.RefPhase;

/**
 * @author XT0087920
 *
 */
public interface IRefPhaseRepository extends JpaRepository<RefPhase, String>,  JpaSpecificationExecutor<RefPhase> {
	

}
