/**
 * 
 */
package orange.omn.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import orange.omn.bean.Program;

/**
 * @author XT0087920
 *
 */
public interface IProgramRepository extends JpaRepository<Program, String>,  JpaSpecificationExecutor<Program> {
	
    @Query(value  ="SELECT MAX(idProgram) from Program")
    int retrieveByMaxId();
    
    @Query(value  ="DELETE from Program WHERE idProgramParent = :progParentId")
    int deleteWithProgParentId(@Param("progParentId") int progParentId);
    
    @Query(value  ="SELECT p "
            + "FROM Program p "
            + "WHERE p.programParent.idProgramParent = :idProgramParent")   
    public List<Program> findByIdProgramParent(@Param("idProgramParent") int idProgramParent);   
    
}
