/**
 * 
 */
package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import orange.omn.bean.RefCategory;

/**
 * @author XT0087920
 *
 */
public interface IRefCategoryRepository extends JpaRepository<RefCategory, String>,  JpaSpecificationExecutor<RefCategory> {
	

}
