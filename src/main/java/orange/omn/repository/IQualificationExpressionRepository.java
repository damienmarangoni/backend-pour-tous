package orange.omn.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import orange.omn.bean.QualificationExpression;

public interface IQualificationExpressionRepository extends JpaRepository<QualificationExpression, String>,  JpaSpecificationExecutor<QualificationExpression> {
	
	@Query(value  ="SELECT qe FROM QualificationExpression qe, CustomerQualification cq WHERE qe.customerQualification = cq.customerQualificationId AND cq.customerQualificationId = :custCalId ")
	public List<QualificationExpression> retrieveQualifExpressFromCustCalId(@Param("custCalId") int custCalId);

}
