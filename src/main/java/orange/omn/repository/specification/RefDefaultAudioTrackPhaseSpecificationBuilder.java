package orange.omn.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import orange.omn.bean.RefDefaultAudioTrackPhase;
import orange.omn.repository.common.SearchCriteria;

public class RefDefaultAudioTrackPhaseSpecificationBuilder {
	
private final List<SearchCriteria> params;
	
	public RefDefaultAudioTrackPhaseSpecificationBuilder() {
		params = new ArrayList<SearchCriteria>();
		
	}
	
	public RefDefaultAudioTrackPhaseSpecificationBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteria(key, operation, value));
		return this;
	}
	
	public Specification<RefDefaultAudioTrackPhase> build() {
		if (params.size() == 0) {
			return null;
		}
		
		List<Specification<RefDefaultAudioTrackPhase>> specs =  new ArrayList<Specification<RefDefaultAudioTrackPhase>>();
		for (SearchCriteria param : params) {
			specs.add(new RefDefaultAudioTrackPhaseSpecification(param));
		}

		Specification<RefDefaultAudioTrackPhase> result = specs.get(0);
		for (int i = 1; i < specs.size(); i++) {
			result = Specifications.where(result).and(specs.get(i));
		}
		return result;
		
	}


}
