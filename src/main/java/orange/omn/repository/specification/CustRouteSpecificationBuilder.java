package orange.omn.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import orange.omn.bean.CustomerQualification;
import orange.omn.repository.common.SearchCriteria;

public class CustRouteSpecificationBuilder {
	
	private final List<SearchCriteria> params;
	
	public CustRouteSpecificationBuilder() {
		params = new ArrayList<SearchCriteria>();
		
	}
	
	public CustRouteSpecificationBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteria(key, operation, value));
		return this;
	}
	
	public Specification<CustomerQualification> build() {
		if (params.size() == 0) {
			return null;
		}
		
		List<Specification<CustomerQualification>> specs =  new ArrayList<Specification<CustomerQualification>>();
		for (SearchCriteria param : params) {
			specs.add(new CustRouteSpecification(param));
		}

		Specification<CustomerQualification> result = specs.get(0);
		for (int i = 1; i < specs.size(); i++) {
			result = Specifications.where(result).and(specs.get(i));
		}
		return result;
		
	}

}
