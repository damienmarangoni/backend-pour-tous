package orange.omn.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import orange.omn.bean.RefPriority;
import orange.omn.repository.common.SearchCriteria;

public class RefPrioritySpecificationBuilder {
	
	private final List<SearchCriteria> params;
	
	public RefPrioritySpecificationBuilder() {
		params = new ArrayList<SearchCriteria>();
		
	}
	
	public RefPrioritySpecificationBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteria(key, operation, value));
		return this;
	}
	
	public Specification<RefPriority> build() {
		if (params.size() == 0) {
			return null;
		}
		
		List<Specification<RefPriority>> specs =  new ArrayList<Specification<RefPriority>>();
		for (SearchCriteria param : params) {
			specs.add(new RefPrioritySpecification(param));
		}

		Specification<RefPriority> result = specs.get(0);
		for (int i = 1; i < specs.size(); i++) {
			result = Specifications.where(result).and(specs.get(i));
		}
		return result;
		
	}

}
