package orange.omn.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import orange.omn.bean.TemplateContent;
import orange.omn.repository.common.SearchCriteria;

public class TemplateContentSpecificationBuilder {
	
	private final List<SearchCriteria> params;
	
	public TemplateContentSpecificationBuilder() {
		params = new ArrayList<SearchCriteria>();
		
	}
	
	public TemplateContentSpecificationBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteria(key, operation, value));
		return this;
	}
	
	public Specification<TemplateContent> build() {
		if (params.size() == 0) {
			return null;
		}
		
		List<Specification<TemplateContent>> specs =  new ArrayList<Specification<TemplateContent>>();
		for (SearchCriteria param : params) {
			specs.add(new TemplateContentSpecification(param));
		}

		Specification<TemplateContent> result = specs.get(0);
		for (int i = 1; i < specs.size(); i++) {
			result = Specifications.where(result).and(specs.get(i));
		}
		return result;
		
	}

}
