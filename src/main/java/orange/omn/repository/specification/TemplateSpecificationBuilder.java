package orange.omn.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import orange.omn.bean.Template;
import orange.omn.repository.common.SearchCriteria;

public class TemplateSpecificationBuilder {
	
	private final List<SearchCriteria> params;
	
	public TemplateSpecificationBuilder() {
		params = new ArrayList<SearchCriteria>();
		
	}
	
	public TemplateSpecificationBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteria(key, operation, value));
		return this;
	}
	
	public Specification<Template> build() {
		if (params.size() == 0) {
			return null;
		}
		
		List<Specification<Template>> specs =  new ArrayList<Specification<Template>>();
		for (SearchCriteria param : params) {
			specs.add(new TemplateSpecification(param));
		}

		Specification<Template> result = specs.get(0);
		for (int i = 1; i < specs.size(); i++) {
			result = Specifications.where(result).and(specs.get(i));
		}
		return result;
		
	}

}
