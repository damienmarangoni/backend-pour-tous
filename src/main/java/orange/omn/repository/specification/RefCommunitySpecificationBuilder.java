package orange.omn.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import orange.omn.bean.RefCommunity;
import orange.omn.repository.common.SearchCriteria;

public class RefCommunitySpecificationBuilder {
	
	private final List<SearchCriteria> params;
	
	public RefCommunitySpecificationBuilder() {
		params = new ArrayList<SearchCriteria>();
		
	}
	
	public RefCommunitySpecificationBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteria(key, operation, value));
		return this;
	}
	
	public Specification<RefCommunity> build() {
		if (params.size() == 0) {
			return null;
		}
		
		List<Specification<RefCommunity>> specs =  new ArrayList<Specification<RefCommunity>>();
		for (SearchCriteria param : params) {
			specs.add(new RefCommunitySpecification(param));
		}

		Specification<RefCommunity> result = specs.get(0);
		for (int i = 1; i < specs.size(); i++) {
			result = Specifications.where(result).and(specs.get(i));
		}
		return result;
		
	}

}
