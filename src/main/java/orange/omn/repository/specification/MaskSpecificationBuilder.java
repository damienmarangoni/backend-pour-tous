package orange.omn.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import orange.omn.bean.Mask;
import orange.omn.repository.common.SearchCriteria;

public class MaskSpecificationBuilder {
	
	private final List<SearchCriteria> params;
	
	public MaskSpecificationBuilder() {
		params = new ArrayList<SearchCriteria>();
		
	}
	
	public MaskSpecificationBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteria(key, operation, value));
		return this;
	}
	
	public Specification<Mask> build() {
		if (params.size() == 0) {
			return null;
		}
		
		List<Specification<Mask>> specs =  new ArrayList<Specification<Mask>>();
		for (SearchCriteria param : params) {
			specs.add(new MaskSpecification(param));
		}

		Specification<Mask> result = specs.get(0);
		for (int i = 1; i < specs.size(); i++) {
			result = Specifications.where(result).and(specs.get(i));
		}
		return result;
		
	}

}
