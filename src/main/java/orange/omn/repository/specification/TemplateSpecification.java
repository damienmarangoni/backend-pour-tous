package orange.omn.repository.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import orange.omn.bean.Template;
import orange.omn.repository.common.SearchCriteria;

public class TemplateSpecification implements Specification<Template> {
	
	private SearchCriteria criteria;
	
	public TemplateSpecification(SearchCriteria criteria) {
		super();
		this.criteria = criteria;
		
	}

	@Override
	public Predicate toPredicate(Root<Template> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
		
		if (criteria.getOperation().equalsIgnoreCase(">")) {
			return builder.greaterThanOrEqualTo(
					root.<String> get(criteria.getKey()), criteria.getValue().toString());
		}
		else if (criteria.getOperation().equalsIgnoreCase("<")) {
			return builder.lessThanOrEqualTo(
					root.<String> get(criteria.getKey()), criteria.getValue().toString());
		}
		else if (criteria.getOperation().equalsIgnoreCase(":")) {
			Class<? extends Object> keyJavaType = root.get(criteria.getKey()).getJavaType();
			if (keyJavaType == String.class) {
				return builder.like(
						root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
			} else if (keyJavaType == boolean.class || keyJavaType == Boolean.class) {
				return builder.equal(root.get(criteria.getKey()), Boolean.parseBoolean(criteria.getValue().toString()));
			} else {
				return builder.equal(root.get(criteria.getKey()), criteria.getValue());
			}
		}
		return null;
	}

}
