package orange.omn.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import orange.omn.bean.RefCategory;
import orange.omn.repository.common.SearchCriteria;

public class RefCategorySpecificationBuilder {
	
	private final List<SearchCriteria> params;
	
	public RefCategorySpecificationBuilder() {
		params = new ArrayList<SearchCriteria>();
		
	}
	
	public RefCategorySpecificationBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteria(key, operation, value));
		return this;
	}
	
	public Specification<RefCategory> build() {
		if (params.size() == 0) {
			return null;
		}
		
		List<Specification<RefCategory>> specs =  new ArrayList<Specification<RefCategory>>();
		for (SearchCriteria param : params) {
			specs.add(new RefCategorySpecification(param));
		}

		Specification<RefCategory> result = specs.get(0);
		for (int i = 1; i < specs.size(); i++) {
			result = Specifications.where(result).and(specs.get(i));
		}
		return result;
		
	}

}
