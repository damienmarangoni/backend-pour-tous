package orange.omn.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import orange.omn.bean.RefDefaultAudioTrack;
import orange.omn.repository.common.SearchCriteria;

public class RefDefaultAudioTrackSpecificationBuilder {
	
private final List<SearchCriteria> params;
	
	public RefDefaultAudioTrackSpecificationBuilder() {
		params = new ArrayList<SearchCriteria>();
		
	}
	
	public RefDefaultAudioTrackSpecificationBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteria(key, operation, value));
		return this;
	}
	
	public Specification<RefDefaultAudioTrack> build() {
		if (params.size() == 0) {
			return null;
		}
		
		List<Specification<RefDefaultAudioTrack>> specs =  new ArrayList<Specification<RefDefaultAudioTrack>>();
		for (SearchCriteria param : params) {
			specs.add(new RefDefaultAudioTrackSpecification(param));
		}

		Specification<RefDefaultAudioTrack> result = specs.get(0);
		for (int i = 1; i < specs.size(); i++) {
			result = Specifications.where(result).and(specs.get(i));
		}
		return result;
		
	}


}
