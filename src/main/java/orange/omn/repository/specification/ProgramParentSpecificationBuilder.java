package orange.omn.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import orange.omn.bean.ProgramParent;
import orange.omn.repository.common.SearchCriteria;

public class ProgramParentSpecificationBuilder {
	
	private final List<SearchCriteria> params;
	
	public ProgramParentSpecificationBuilder() {
		params = new ArrayList<SearchCriteria>();
		
	}
	
	public ProgramParentSpecificationBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteria(key, operation, value));
		return this;
	}
	
	public Specification<ProgramParent> build() {
		if (params.size() == 0) {
			return null;
		}
		
		List<Specification<ProgramParent>> specs =  new ArrayList<Specification<ProgramParent>>();
		for (SearchCriteria param : params) {
			specs.add(new ProgramParentSpecification(param));
		}

		Specification<ProgramParent> result = specs.get(0);
		for (int i = 1; i < specs.size(); i++) {
			result = Specifications.where(result).and(specs.get(i));
		}
		return result;
		
	}

}
