/**
 * 
 */
package orange.omn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import orange.omn.bean.Service;

/**
 * @author XT0087920
 *
 */
public interface IServicesRepository extends JpaRepository<Service, String>,  JpaSpecificationExecutor<Service> {
	
}
