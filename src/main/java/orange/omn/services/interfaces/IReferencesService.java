package orange.omn.services.interfaces;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import orange.omn.bean.RefCategory;
import orange.omn.bean.RefCommunity;
import orange.omn.bean.RefDefaultAudioTrack;
import orange.omn.bean.RefDefaultAudioTrackPhase;
import orange.omn.bean.RefPhase;
import orange.omn.bean.RefPriority;
import orange.omn.bean.RefTemplateItem;
import orange.omn.bean.Service;
import orange.omn.web.dto.RefDefaultAudioTrackDTO;

public interface IReferencesService {

	Page<RefPriority> getRefPriority(String search, Pageable pageable);

	Page<RefCommunity> getRefCommunity(String search, Pageable pageable);

    Page<RefPhase> getRefPhase(String search, Pageable pageable);
	
	List<RefTemplateItem> getRefTemplateItemList();

	List<Service> getServicesList();

	List<RefDefaultAudioTrack> getRefDefaultAudioTrackList();

	Page<RefDefaultAudioTrack> getRefDefaultAudioTrackList(String search, Pageable pageable);

    Page<RefCategory> getRefCategory(String search, Pageable pageable);

    void modifyRefDefAudioTrack(RefDefaultAudioTrackDTO templateDTO, String id);

    Page<RefDefaultAudioTrackPhase> getRefDefaultAudioTrackPhaseList(
            String search, Pageable pageable);


}
