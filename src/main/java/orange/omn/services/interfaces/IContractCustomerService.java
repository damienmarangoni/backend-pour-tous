package orange.omn.services.interfaces;

import java.util.List;

import orange.omn.bean.ContractCustomer;

public interface IContractCustomerService {

	List<ContractCustomer> getContractCustomerList();

}
