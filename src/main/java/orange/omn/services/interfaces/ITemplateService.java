package orange.omn.services.interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import orange.omn.bean.Template;
import orange.omn.rest.errors.BadInput;
import orange.omn.web.dto.TemplateDTO;


public interface ITemplateService {

	public Page<Template> getTemplate(String search, Pageable pageable);
	
	public Template getTemplateByIdTemplate(int idTemplate);
	
	public void modifyWrittenTemplate(TemplateDTO template, int templateId) throws BadInput;
    
    public void modifyAudioTemplate(TemplateDTO template, int templateId) throws BadInput;
    
    public void createTemplate(TemplateDTO template) throws BadInput;

    public void deleteTemplate(int templateId) throws BadInput;

}
