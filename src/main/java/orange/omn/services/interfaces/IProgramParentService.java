package orange.omn.services.interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import orange.omn.bean.ProgramParent;
import orange.omn.rest.errors.BadInput;
import orange.omn.web.dto.ProgramParentDTO;


public interface IProgramParentService {

	public Page<ProgramParent> getProgramParentByCrit(String search, Pageable pageable);

    public Page<ProgramParent> getProgramParents(String codePhase, int customerQualificationId, Pageable pageable);

    public Page<ProgramParent> getProgramParentsByCustQualifId(int customerQualificationId, Pageable pageable);
    
//    public Page<ProgramParent> getProgramParentsForSimulator(String[] message,String codePhase, Date date, Pageable pageable);
    
    void createProgParent(ProgramParentDTO progParent, String customerRouteId) throws BadInput;
    
    void modifyProgParent(ProgramParentDTO progParent, String customerRouteId) throws BadInput;
    
    void deleteProgParent(ProgramParentDTO progParent) throws BadInput;
    
    void deleteProgrammation(int idProgramParent) throws BadInput;
}
