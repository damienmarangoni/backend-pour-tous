package orange.omn.services.interfaces;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import orange.omn.bean.CustomerQualification;
import orange.omn.rest.errors.BadInput;


public interface ICustomerRouteService {

	public Page<CustomerQualification> getCustomerRoutes(LocalDate startDate, LocalDate endDate, Pageable pageable);

	public Page<CustomerQualification> getCustomerRoutesFromCrit(Specification<CustomerQualification> spec,
			Pageable pageable);

	public Page<CustomerQualification> getAllCustomerRoutes(Pageable pageable);

	public Page<CustomerQualification> getCustomerRouteDetails(String search, Pageable pageable);

	public void validateCustomerRoute(CustomerQualification customerQualification) throws BadInput;

	public void update(CustomerQualification partialCustRoute) throws BadInput;
	
	public void deleteByIdCustomerQualification(int idCustomerQualication) throws BadInput;

    public Page<CustomerQualification> getCustomerRoutesForSimulator(String codePhase,
            LocalDate date, String[] message, Pageable pageable);


}
