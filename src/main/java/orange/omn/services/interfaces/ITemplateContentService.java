package orange.omn.services.interfaces;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import orange.omn.bean.TemplateContent;
import orange.omn.rest.errors.BadInput;

public interface ITemplateContentService {


	public Page<TemplateContent> getTemplateContent(String search, Pageable pageable);

	public Page<TemplateContent> getTemplateContent(Pageable pageable, int channelId);

	public void deleteTemplateContent(String templateContentId) throws BadInput;
	
	public void updateTemplateContent(TemplateContent templateContent, String idTemplateContent) throws BadInput;

	public void createTemplateContent(TemplateContent templateContent) throws BadInput;
	
	public boolean exists(TemplateContent templateContent);

	public List<TemplateContent> getWrittenTemplateContentText();

}
