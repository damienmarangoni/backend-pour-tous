package orange.omn.services.interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import orange.omn.bean.Mask;
import orange.omn.bean.TemplateContent;
import orange.omn.rest.errors.BadInput;
import orange.omn.web.dto.MaskDTO;


public interface IMaskService {

	public Page<Mask> getMask(String search, Pageable pageable);
	
	public Mask getMaskByNbPublic(String maskPublicNb);
	
	public void modifyMask(MaskDTO mask, String maskedNb) throws BadInput;
    
    //public void createMask(MaskDTO mask) throws BadInput;
    public void createMask(Mask mask) throws BadInput;

    public void deleteMask(String maskPublicNb) throws BadInput;


}
