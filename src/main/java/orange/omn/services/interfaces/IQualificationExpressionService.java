package orange.omn.services.interfaces;

import java.util.List;

import orange.omn.rest.errors.BadInput;
import orange.omn.web.dto.QualificationExpressionDTO;

public interface IQualificationExpressionService {

//	void createQualifExpress(QualificationExpression qualifExpress, String id)  throws BadInput;

	void createQualifExpress(List<QualificationExpressionDTO> qualExprList, String customerRouteId) throws BadInput;

}
