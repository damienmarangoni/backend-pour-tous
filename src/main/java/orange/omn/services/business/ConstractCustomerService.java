package orange.omn.services.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import orange.omn.bean.ContractCustomer;
import orange.omn.repository.IContractCustomerRepository;
import orange.omn.services.interfaces.IContractCustomerService;



@Service
public class ConstractCustomerService implements IContractCustomerService{
	
	@Autowired
	IContractCustomerRepository contractCustomerRepo;




	@Override
	public List<ContractCustomer> getContractCustomerList() {
		return contractCustomerRepo.findAll();
	}


}
