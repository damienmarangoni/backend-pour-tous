package orange.omn.services.business;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import orange.omn.bean.TemplateContent;
import orange.omn.common.RefChanEnum;
import orange.omn.repository.ITemplateContentRepository;
import orange.omn.repository.common.SearchTools;
import orange.omn.repository.specification.TemplateContentSpecificationBuilder;
import orange.omn.rest.errors.BadInput;
import orange.omn.services.interfaces.ITemplateContentService;



@Service
public class TemplateContentService implements ITemplateContentService{
	
	@Autowired
	ITemplateContentRepository templateContentRepo;



	@Override
	public Page<TemplateContent> getTemplateContent(String search, Pageable pageable) {
		
		if (search != null) {
			Specification<TemplateContent> spec = specTemplateContent(search);
			return  templateContentRepo.findAll(spec, new PageRequest(0, 1000)); 
		} else {
			return templateContentRepo.findAll(new PageRequest(0, 1000));
		}
	}



	@Override
	public Page<TemplateContent> getTemplateContent(Pageable pageable, int channelId) {
			return  templateContentRepo.findByChannelId(pageable, channelId); 
	}
	
	@Override
	public List<TemplateContent> getWrittenTemplateContentText() {
			return  templateContentRepo.findByChannelId(RefChanEnum.ECRIT.getChannelId()); 
	}



	private Specification<TemplateContent> specTemplateContent(String search) {
		TemplateContentSpecificationBuilder builder = new TemplateContentSpecificationBuilder();

		Pattern pattern = Pattern.compile(SearchTools.SEARCH_CRITERIA_REGEXP);
		Matcher matcher = pattern.matcher(search + ",");
		while (matcher.find()) {
			builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
		}

		Specification<TemplateContent> spec = builder.build();
		return spec;
	}



	@Override
	public void deleteTemplateContent(String templateContentId) throws BadInput {
		templateContentRepo.deleteByTemplateContentId(templateContentId);
	}



	@Override
	public void updateTemplateContent(TemplateContent templateContent, String templateContentId) throws BadInput {
		
		templateContent.setTemplateContentId(templateContentId);
		templateContentRepo.save(templateContent);		
	}



	@Override
	public void createTemplateContent(TemplateContent templateContent) throws BadInput {
		templateContent.setTemplateContentId(getGeneratedId(templateContent));
		templateContentRepo.save(templateContent);		
	}



	@Override
	public boolean exists(TemplateContent templateContent) {
		return templateContentRepo.exists(templateContent.getTemplateContentId());
	}
	
	
	private String getGeneratedId (TemplateContent templateContent) {
		
		long numbOcc = templateContentRepo.count();
		long current=System.currentTimeMillis();
		
		String generatedId = templateContent.getTemplateContentName().substring(0,  2) + Long.toString(numbOcc) + "_" + current;
		
		return generatedId;
		
	}




}
