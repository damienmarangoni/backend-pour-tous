package orange.omn.services.business;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import orange.omn.bean.CustomerQualification;
import orange.omn.bean.QualificationExpression;
import orange.omn.repository.ICustomerRouteRepository;
import orange.omn.repository.IQualificationExpressionRepository;
import orange.omn.repository.common.SearchTools;
import orange.omn.repository.specification.CustRouteSpecificationBuilder;
import orange.omn.rest.errors.BadInput;
import orange.omn.services.interfaces.ICustomerRouteService;
import orange.omn.services.interfaces.IQualificationExpressionService;
import orange.omn.web.mapper.IQualificationExpressionMapper;



@Service
public class CustomerRouteService implements ICustomerRouteService{
    
    @Autowired
    ICustomerRouteRepository custRouteRepo;
    
    @Autowired
    IQualificationExpressionService qualifExprService;
    
    @Autowired
    IQualificationExpressionRepository qualifExpressRepo;

    @Autowired
    IQualificationExpressionMapper qualifExprMapper;
        

    @Override
    public Page<CustomerQualification> getCustomerRoutes(LocalDate startDate, LocalDate endDate, Pageable pageable) {
        
        if (startDate != null && endDate != null) {
            
            List<CustomerQualification> custQualPage = new ArrayList<CustomerQualification>();               
            custQualPage.addAll(custRouteRepo.retrieveByStartDateAndEndDate(startDate, endDate, pageable).getContent());
            custQualPage.addAll(custRouteRepo.retrieve(pageable).getContent());
         
            Page<CustomerQualification> page = new PageImpl<CustomerQualification>(custQualPage);
            
            return page;
        }
            
        else if (endDate == null) {
                return custRouteRepo.retrieveByStartDate(startDate.atStartOfDay().toLocalDate(), pageable);
        
        } else {
            return getAllCustomerRoutes(pageable);
        }
    } 


    @Override
    public Page<CustomerQualification> getCustomerRoutesFromCrit(Specification<CustomerQualification> spec,
            Pageable pageable) {
        return custRouteRepo.findAll(spec, pageable);
    }


    @Override
    public Page<CustomerQualification> getAllCustomerRoutes(Pageable pageable) {
        return custRouteRepo.findAll(pageable);
    }


    @Override
    public Page<CustomerQualification> getCustomerRouteDetails(String search, Pageable pageable) {
        
        if (search != null) {
            CustRouteSpecificationBuilder builder = new CustRouteSpecificationBuilder();

            Pattern pattern = Pattern.compile(SearchTools.SEARCH_CRITERIA_REGEXP);
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }

            Specification<CustomerQualification> spec = builder.build();
            return  custRouteRepo.findAll(spec, pageable); 
        } else {
            return custRouteRepo.findAll(pageable);
        }
    }


    @Override
    public void validateCustomerRoute(CustomerQualification custRoute) throws BadInput {
        
        custRouteRepo.save(custRoute);
        
    }


    @Override
    public void update(CustomerQualification partialCustRoute) throws BadInput {
        custRouteRepo.save(partialCustRoute);
        
    }


    @Override
    public void deleteByIdCustomerQualification(int idCustomerQualification)
            throws BadInput {
        List <QualificationExpression> qualifExpressDeleteList = qualifExpressRepo.retrieveQualifExpressFromCustCalId(idCustomerQualification);

        //Delete QualificationExpression (détails du parcours client)
        qualifExpressRepo.delete(qualifExpressDeleteList);
        
        //Delete CustomerRoute
        custRouteRepo.deleteByCustomerQualificationId(idCustomerQualification);
        
    }
    
    @Value("${number.possibility.qualification}")
    private int maxCursor;
    
    @Autowired
    Environment environment;

    @Override
    public Page<CustomerQualification> getCustomerRoutesForSimulator(String codePhase, LocalDate date, String[] message, Pageable pageable) {
      
        Page<CustomerQualification> returnList = null;
        
        int isDu;
        int isCat;
        int isComm;

        String du;
        String cat;
        int comm;
        
        int cursor = 0;
        int resultSize = 0;
        
        if(date!=null) {            
            
            while((resultSize==0)&&(cursor<maxCursor-1)) {
                
                isDu = Integer.parseInt(environment.getProperty("qualification.possibility." + (cursor+1)).split(";")[0]);
                isCat = Integer.parseInt(environment.getProperty("qualification.possibility." + (cursor+1)).split(";")[1]);
                isComm = Integer.parseInt(environment.getProperty("qualification.possibility." + (cursor+1)).split(";")[2]);
                               
                if(isDu==1) {
                    du = message[0];
                }
                else {
                    du = "%";
                }

                if(isCat==1) {
                    cat = message[1];
                }
                else {
                    cat = "%";
                }
                
                if(isComm==1) {
                    comm = Integer.parseInt(message[2]);
                    returnList =  custRouteRepo.retrieveDefaultForSimulator(du, cat, comm, pageable);
                }
                else {
                    returnList =  custRouteRepo.retrieveDefaultForSimulator(du, cat, pageable);
                }
                
                resultSize = returnList.getContent().size();
                cursor++;
            }
        }
        else {
            returnList =  custRouteRepo.retrieveDefaultForSimulator(message[0], message[1], Integer.parseInt(message[2]), pageable);
        }
        
        return returnList;
    }

}
