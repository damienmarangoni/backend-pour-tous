package orange.omn.services.business;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import orange.omn.bean.RefCategory;
import orange.omn.bean.RefCommunity;
import orange.omn.bean.RefDefaultAudioTrack;
import orange.omn.bean.RefDefaultAudioTrackPhase;
import orange.omn.bean.RefPhase;
import orange.omn.bean.RefPriority;
import orange.omn.bean.RefTemplateItem;
import orange.omn.repository.IRefCategoryRepository;
import orange.omn.repository.IRefCommunityRepository;
import orange.omn.repository.IRefDefaultAudioTrackListRepository;
import orange.omn.repository.IRefDefaultAudioTrackPhaseListRepository;
import orange.omn.repository.IRefPhaseRepository;
import orange.omn.repository.IRefPriorityRepository;
import orange.omn.repository.IRefTemplateItemRepository;
import orange.omn.repository.IServicesRepository;
import orange.omn.repository.common.SearchTools;
import orange.omn.repository.specification.RefCategorySpecificationBuilder;
import orange.omn.repository.specification.RefCommunitySpecificationBuilder;
import orange.omn.repository.specification.RefDefaultAudioTrackPhaseSpecificationBuilder;
import orange.omn.repository.specification.RefDefaultAudioTrackSpecificationBuilder;
import orange.omn.repository.specification.RefPhaseSpecificationBuilder;
import orange.omn.repository.specification.RefPrioritySpecificationBuilder;
import orange.omn.services.interfaces.IReferencesService;
import orange.omn.web.dto.RefDefaultAudioTrackDTO;
import orange.omn.web.mapper.IReferencesMapper;



@Service
public class ReferencesService implements IReferencesService{
	
	@Autowired
	IRefPriorityRepository refPriorityRepo;
	
	@Autowired
	IRefCommunityRepository refCommunityRepo;
    
    @Autowired
    IRefCategoryRepository refCategoryRepo;
    
    @Autowired
    IRefPhaseRepository refPhaseRepo;
	
	@Autowired
	IRefTemplateItemRepository refTemplateItemRepo;
	
	@Autowired
	IServicesRepository servicesRepo;
	
	@Autowired
	IRefDefaultAudioTrackListRepository refDefaultAudioTrackListRepo;
    
    @Autowired
    IRefDefaultAudioTrackPhaseListRepository refDefaultAudioTrackPhaseListRepo;
	
	@Autowired
    IReferencesMapper referencesMapper;



	@Override
	public Page<RefPriority> getRefPriority(String search, Pageable pageable) {
		
		if (search != null) {
			RefPrioritySpecificationBuilder builder = new RefPrioritySpecificationBuilder();

			Pattern pattern = Pattern.compile(SearchTools.SEARCH_CRITERIA_REGEXP);
			Matcher matcher = pattern.matcher(search + ",");
			while (matcher.find()) {
				builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
			}

			Specification<RefPriority> spec = builder.build();
			return  refPriorityRepo.findAll(spec, pageable); 
		} else {
			return refPriorityRepo.findAll(pageable);
		}
	}

	@Override
	public Page<RefCommunity> getRefCommunity(String search, Pageable pageable) {
		if (search != null) {
			RefCommunitySpecificationBuilder builder = new RefCommunitySpecificationBuilder();

			Pattern pattern = Pattern.compile(SearchTools.SEARCH_CRITERIA_REGEXP);
			Matcher matcher = pattern.matcher(search + ",");
			while (matcher.find()) {
				builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
			}

			Specification<RefCommunity> spec = builder.build();
			return  refCommunityRepo.findAll(spec, pageable); 
		} else {
			return refCommunityRepo.findAll(pageable);
		}
	}

	@Override
	public List<RefTemplateItem> getRefTemplateItemList() {
		return refTemplateItemRepo.findAll();
	}

	@Override
	public List<orange.omn.bean.Service> getServicesList() {
		return servicesRepo.findAll();
	}



	@Override
	public List<RefDefaultAudioTrack> getRefDefaultAudioTrackList() {
		return refDefaultAudioTrackListRepo.findAll();
	}



	@Override
	public Page<RefDefaultAudioTrack> getRefDefaultAudioTrackList(String search, Pageable pageable) {
		if (search != null) {
			RefDefaultAudioTrackSpecificationBuilder builder = new RefDefaultAudioTrackSpecificationBuilder();

			Pattern pattern = Pattern.compile(SearchTools.SEARCH_CRITERIA_REGEXP);
			Matcher matcher = pattern.matcher(search + ",");
			while (matcher.find()) {
				builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
			}

			Specification<RefDefaultAudioTrack> spec = builder.build();
			return  refDefaultAudioTrackListRepo.findAll(spec, pageable); 
		} else {
			return refDefaultAudioTrackListRepo.findAll(pageable);
		}
	}

    @Override
    public Page<RefDefaultAudioTrackPhase> getRefDefaultAudioTrackPhaseList(String search, Pageable pageable) {
        if (search != null) {
            RefDefaultAudioTrackPhaseSpecificationBuilder builder = new RefDefaultAudioTrackPhaseSpecificationBuilder();

            Pattern pattern = Pattern.compile(SearchTools.SEARCH_CRITERIA_REGEXP);
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }

            Specification<RefDefaultAudioTrackPhase> spec = builder.build();
            return  refDefaultAudioTrackPhaseListRepo.findAll(spec, pageable); 
        } else {
            return refDefaultAudioTrackPhaseListRepo.findAll(pageable);
        }
    }

    @Override
    public Page<RefCategory> getRefCategory(String search, Pageable pageable) {
        if (search != null) {
            RefCategorySpecificationBuilder builder = new RefCategorySpecificationBuilder();

            Pattern pattern = Pattern.compile(SearchTools.SEARCH_CRITERIA_REGEXP);
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }

            Specification<RefCategory> spec = builder.build();
            return  refCategoryRepo.findAll(spec, pageable); 
        } else {
            return refCategoryRepo.findAll(pageable);
        }
    }

    @Override
    public Page<RefPhase> getRefPhase(String search, Pageable pageable) {
        if (search != null) {
            RefPhaseSpecificationBuilder builder = new RefPhaseSpecificationBuilder();

            Pattern pattern = Pattern.compile(SearchTools.SEARCH_CRITERIA_REGEXP);
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }

            Specification<RefPhase> spec = builder.build();
            return  refPhaseRepo.findAll(spec, pageable); 
        } else {
            return refPhaseRepo.findAll(pageable);
        }
    }    
    
    @Override
    public void modifyRefDefAudioTrack(RefDefaultAudioTrackDTO refDefaultAudioTrackDTO, String id) {

        RefDefaultAudioTrack refDef = referencesMapper.refDefaultAudioTrackDTOToBean(refDefaultAudioTrackDTO);        
        refDefaultAudioTrackListRepo.save(refDef);
    }

}
