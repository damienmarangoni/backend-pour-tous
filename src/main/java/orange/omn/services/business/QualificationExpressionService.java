package orange.omn.services.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import orange.omn.bean.CustomerQualification;
import orange.omn.bean.QualificationExpression;
import orange.omn.repository.ICustomerRouteRepository;
import orange.omn.repository.IQualificationExpressionRepository;
import orange.omn.rest.errors.BadInput;
import orange.omn.services.interfaces.IQualificationExpressionService;
import orange.omn.web.dto.QualificationExpressionDTO;
import orange.omn.web.mapper.IQualificationExpressionMapper;

@Service
@Transactional(rollbackFor = Throwable.class)
public class QualificationExpressionService implements IQualificationExpressionService {
	
	@Autowired
	IQualificationExpressionRepository qualifExpressRepo;
	
	@Autowired
	ICustomerRouteRepository custRouteRepo;
	
	@Autowired
	IQualificationExpressionMapper qualifExprMapper;


	@Override
	public void createQualifExpress(List<QualificationExpressionDTO> qualifExpressDTO, String customerRouteId) throws BadInput {
		
		// retrieve the idCustomerQualification to populate QualificationExpression bean
		//int idCustomerQualification = qualifExpressDTO.get(0).getCommunityFilter();
		CustomerQualification custQual = custRouteRepo.findByCustomerQualificationId(Integer.valueOf(customerRouteId));
		
		List <QualificationExpression> qualifExpressDeleteList = qualifExpressRepo.retrieveQualifExpressFromCustCalId(Integer.valueOf(customerRouteId));
		
		List <QualificationExpression> qualifExpressInsertList = qualifExprMapper.qualifExpressDTOToBean(qualifExpressDTO);
		
		for (QualificationExpression qualifExpress : qualifExpressInsertList) {
			qualifExpress.setCustomerQualification(custQual);
		}
		
		// delete QualificationExpression in db
		qualifExpressRepo.delete(qualifExpressDeleteList);
		// save new ones
		qualifExpressRepo.save(qualifExpressInsertList);
		
		
				
		
		
	}




}
