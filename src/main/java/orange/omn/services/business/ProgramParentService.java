package orange.omn.services.business;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import orange.omn.bean.CustomerQualification;
import orange.omn.bean.Program;
import orange.omn.bean.ProgramParent;
import orange.omn.bean.ProgramParentActivity;
import orange.omn.bean.RealProgram;
import orange.omn.bean.RefActivity;
import orange.omn.bean.Template;
import orange.omn.bean.TemplateContent;
import orange.omn.repository.ICustomerRouteRepository;
import orange.omn.repository.IProgramParentActivityRepository;
import orange.omn.repository.IProgramParentRepository;
import orange.omn.repository.IProgramRepository;
import orange.omn.repository.IRealProgramRepository;
import orange.omn.repository.IRefActivityRepository;
import orange.omn.repository.ITemplateContentRepository;
import orange.omn.repository.ITemplateRepository;
import orange.omn.repository.common.SearchTools;
import orange.omn.repository.specification.ProgramParentSpecificationBuilder;
import orange.omn.rest.errors.BadInput;
import orange.omn.services.interfaces.IProgramParentService;
import orange.omn.web.dto.ProgramParentDTO;
import orange.omn.web.mapper.IProgramMapper;
import orange.omn.web.mapper.IProgramParentMapper;



@Service
public class ProgramParentService implements IProgramParentService{
	
	@Autowired
	IProgramParentRepository programParentRepo;
    
    @Autowired
    ICustomerRouteRepository custRouteRepo;
    
    @Autowired
    ITemplateRepository templateRepo;
    
    @Autowired
    ITemplateContentRepository templateContentRepo;
	
	@Autowired
    IProgramParentMapper programParentMapper;
	
    @Autowired
    IProgramRepository programRepo;
	
    @Autowired
    IProgramMapper programMapper;
    
    @Autowired
    IRealProgramRepository realProgramRepo;
    
    @Autowired
    IProgramParentActivityRepository programParentActivityRepo;
    
    @Autowired
    IRefActivityRepository refActivityRepo;
    
	
    @Override
    public Page<ProgramParent> getProgramParents(String codePhase, int customerQualificationId, Pageable pageable) {	        
        
        
            return programParentRepo.retrieveByCodePhase(codePhase, customerQualificationId, pageable);	        

    }
		
    @Override
    public Page<ProgramParent> getProgramParentByCrit(String search, Pageable pageable) {
        
        if (search != null) {
            ProgramParentSpecificationBuilder builder = new ProgramParentSpecificationBuilder();

            Pattern pattern = Pattern.compile(SearchTools.SEARCH_CRITERIA_REGEXP);
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }

            Specification<ProgramParent> spec = builder.build();
            return  programParentRepo.findAll(spec, pageable); 
        } else {
            return programParentRepo.findAll(pageable);
        }
    }

    @Override
    public void createProgParent(ProgramParentDTO progParentDTO, String customerRouteId) throws BadInput {

        //Sauvegarde du programParent
        CustomerQualification custQual = custRouteRepo.findByCustomerQualificationId(Integer.valueOf(customerRouteId));
        
        Template tpl = null;
        if(progParentDTO.getTemplate()!=null) {
            tpl = templateRepo.findByTemplateName(progParentDTO.getTemplate().getTemplateName());
        }
                
        ProgramParent programParent = programParentMapper.programParentDTOToBean(progParentDTO);
        programParent.setCustomerQualification(custQual);
        programParent.setTemplate(tpl);
        programParentRepo.save(programParent);
        
        //Récupération du programParent créé
        ProgramParent programParentSaved = programParentRepo.retrieveByCodePhaseAndId(programParent.getTemplate().getCodePhase(),
                Integer.valueOf(customerRouteId));      
        
        //Sauvegarde du programParentActivity
        List<RefActivity> refActivityList = refActivityRepo.findAll();
        ProgramParentActivity progParentActivity = new ProgramParentActivity();
        progParentActivity.setIdProgramParent(programParentSaved.getIdProgramParent());
        progParentActivity.setCodeActivity(refActivityList.get(0).getCodeActivity());
        programParentActivityRepo.save(progParentActivity);  
        
        //Sauvegarde des programs (children)
        List<Program> progs = programMapper.programDTOsToBeans(progParentDTO.getPrograms());
        progs.forEach(prog-> {
            prog.setProgramParent(programParentSaved);
            
            if(prog.getProgramParent()!=null&&prog.getProgramParent().getIdProgramParent()!=0) {
                programRepo.save(prog);
            }
            else {
                programParentRepo.delete(programParent);
            }
            
            //Sauvegarde des realPrograms
            int idProgramSaved = programRepo.retrieveByMaxId();
            
            Program newProg = new Program();
            newProg.setIdProgram(idProgramSaved);
            newProg.setDateStart(prog.getDateStart());
            newProg.setDateEnd(prog.getDateEnd());
            newProg.setProgramParent(programParentSaved);
            
            RealProgram realProg = new RealProgram();            
            realProg.setProgram(newProg);
            realProg.setDateStart(newProg.getDateStart());
            realProg.setDateEnd(newProg.getDateEnd());  
            
            if(realProg.getProgram()!=null&&realProg.getProgram().getIdProgram()!=0) {
                realProgramRepo.save(realProg);
            }
            else {
                programRepo.delete(prog);
                programParentRepo.delete(programParent);
            }
        });                           
    }

    @Override
    public void modifyProgParent(ProgramParentDTO progParentDTO,
            String customerRouteId) throws BadInput {
        
        //Sauvegarde du programParent
        CustomerQualification custQual = custRouteRepo.findByCustomerQualificationId(Integer.valueOf(customerRouteId));
        
        Template tpl = null;
        if(progParentDTO.getTemplate()!=null) {
            tpl = templateRepo.findByTemplateName(progParentDTO.getTemplate().getTemplateName());
        }
        
        TemplateContent tplCont = null;
        if(progParentDTO.getTemplateContent()!=null) {
            tplCont = templateContentRepo.findByTemplateContentId(progParentDTO.getTemplateContent().getTemplateContentId());
        }
                
        ProgramParent programParent = programParentMapper.programParentDTOToBean(progParentDTO);
        programParent.setCustomerQualification(custQual);
        programParent.setTemplate(tpl);
        programParent.setTemplateContent(tplCont);
        
        //Suppression des programs et realPrograms précédents
        List<Program> progs =  programRepo.findByIdProgramParent(programParent.getIdProgramParent());
        progs.forEach(prog ->{
            List<RealProgram> realProgs = realProgramRepo.findByIdProgram(prog.getIdProgram());
            realProgs.forEach(realProg -> {
                realProgramRepo.delete(realProg);
            });
            programRepo.delete(prog);
        });        
        
        //Sauvegarde des programs (children)     
        progs = programMapper.programDTOsToBeans(progParentDTO.getPrograms());
        List<Program> progList = new ArrayList<Program>();
        List<RealProgram> realProgList = new ArrayList<RealProgram>();
        progs.forEach(prog-> {
            
            prog.setProgramParent(programParent);
            
            if(prog.getProgramParent()!=null&&prog.getProgramParent().getIdProgramParent()!=0) {
                programRepo.save(prog);
            }
            else {
                programParentRepo.delete(programParent);
            }
            
            //Sauvegarde des realPrograms
            int idProgramSaved = programRepo.retrieveByMaxId();
            
            Program newProg = new Program();
            newProg.setIdProgram(idProgramSaved);
            newProg.setDateStart(prog.getDateStart());
            newProg.setDateEnd(prog.getDateEnd());
            newProg.setProgramParent(programParent);
            
            progList.add(newProg);
            
            RealProgram realProg = new RealProgram();            
            realProg.setProgram(newProg);
            realProg.setDateStart(newProg.getDateStart());
            realProg.setDateEnd(newProg.getDateEnd());  
            
            realProgList.add(realProg);
            
            if(realProg.getProgram()!=null&&realProg.getProgram().getIdProgram()!=0) {
                realProgramRepo.save(realProg);
            }
            else {
                programRepo.delete(prog);
                programParentRepo.delete(programParent);
            }
        });  
        
        programParent.setPrograms(progList);        
        programParentRepo.save(programParent);        
    }

    @Override
    public void deleteProgParent(ProgramParentDTO progParentDTO) {
        
        ProgramParent programParent = programParentMapper.programParentDTOToBean(progParentDTO);
        
        programParentRepo.delete(programParent);
    }

    @Override
    public Page<ProgramParent> getProgramParentsByCustQualifId(
            int customerQualificationId, Pageable pageable) {
        
        return programParentRepo.retrieveByCustomerQualifId(customerQualificationId, pageable);
    }

    @Override
    public void deleteProgrammation(int idProgramParent) throws BadInput {

        //Suppression des programs et realPrograms
        List<Program> progs =  programRepo.findByIdProgramParent(idProgramParent);
        progs.forEach(prog ->{
            List<RealProgram> realProgs = realProgramRepo.findByIdProgram(prog.getIdProgram());
            realProgs.forEach(realProg -> {
                realProgramRepo.delete(realProg);
            });
            programRepo.delete(prog);
        });    
        
        //Suppression de la programParentActivity
        ProgramParentActivity progParentActivity = programParentActivityRepo.findByIdProgramParent(idProgramParent);
        programParentActivityRepo.delete(progParentActivity);
        
        //Suppression du programParent
        ProgramParent programParent = programParentRepo.retrieveById(idProgramParent);        
        programParentRepo.delete(programParent);
    }

//    @Override
//    public Page<ProgramParent> getProgramParentsForSimulator(String[] message,
//            String codePhase, Date date, Pageable pageable) {
//        
//        //return custRouteRepo.retrieveForSimulator(pageable);
//        return programParentRepo.retrieveForSimulator(message[0], message[1], message[2], codePhase, date, pageable);
//    }
}
