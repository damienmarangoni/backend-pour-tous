package orange.omn.services.business;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import orange.omn.bean.ContractCustomer;
import orange.omn.bean.RefChannel;
import orange.omn.bean.Template;
import orange.omn.bean.TemplateContent;
import orange.omn.repository.ITemplateContentRepository;
import orange.omn.repository.ITemplateRepository;
import orange.omn.repository.common.SearchTools;
import orange.omn.repository.specification.TemplateSpecificationBuilder;
import orange.omn.rest.errors.BadInput;
import orange.omn.services.interfaces.ITemplateService;
import orange.omn.web.dto.TemplateDTO;
import orange.omn.web.mapper.ITemplateMapper;



@Service
public class TemplateService implements ITemplateService{
	
	@Autowired
	ITemplateRepository templateRepo;
	
   @Autowired
    ITemplateContentRepository templateContentRepo;
    
    @Autowired
    ITemplateMapper templateMapper;
		
    @Override
    public Page<Template> getTemplate(String search, Pageable pageable) {
        
        if (search != null) {
            TemplateSpecificationBuilder builder = new TemplateSpecificationBuilder();

            Pattern pattern = Pattern.compile(SearchTools.SEARCH_CRITERIA_REGEXP);
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }

            Specification<Template> spec = builder.build();
            return  templateRepo.findAll(spec, new PageRequest(0, 100)); 
        } else {
            return templateRepo.findAll(new PageRequest(0, 100));
        }
    }

    @Override
    public Template getTemplateByIdTemplate(int idTemplate) {

        return templateRepo.findByIdTemplate(idTemplate);
    }

    @Override
    public void modifyWrittenTemplate(TemplateDTO templateDTO, int templateId) throws BadInput {

        Template tpl = templateRepo.findByIdTemplate(templateId);        
        tpl.setTemplateTemplate(templateDTO.getTemplateTemplate());
        
        TemplateContent tplContent = templateContentRepo.findByTemplateContentId(templateDTO.getTemplateContent().getTemplateContentId());
        tpl.setTemplateContent(tplContent);
        
        templateRepo.save(tpl);
    }

    @Override
    public void modifyAudioTemplate(TemplateDTO templateDTO, int templateId) throws BadInput {

        Template tpl = templateMapper.templateDTOToBean(templateDTO);
        
        ContractCustomer cc = new ContractCustomer();
        cc.setContractCustomerId(1);
        cc.setLabel("118 712");
        
        RefChannel rc = new RefChannel();
        rc.setIdChannel(2);
        rc.setLibChannel("Ecrit");
        
        tpl.getTemplateContent().setContractCustomer(cc);
        tpl.getTemplateContent().setRefChannel(rc);
        
        templateRepo.save(tpl);
    }

    @Override
    public void deleteTemplate(int templateId) throws BadInput {
        
        Template tpl = templateRepo.findByIdTemplate(templateId);     
        templateRepo.delete(tpl);
    }

    @Override
    public void createTemplate(TemplateDTO templateDTO)
            throws BadInput {
        
        Template template = templateMapper.templateDTOToBean(templateDTO);
        
        if(template.getTemplateContent().getTemplateContentName()==null||
                template.getTemplateContent().getTemplateContentName()=="") {
            TemplateContent templateContent = templateContentRepo.findByTemplateContentId(template.getTemplateContent().getTemplateContentId());
            template.setTemplateContent(templateContent);
        }
        else {
            ContractCustomer cc = new ContractCustomer();
            cc.setContractCustomerId(1);
            cc.setLabel("118 712");
            
            RefChannel rc = new RefChannel();
            rc.setIdChannel(2);
            rc.setLibChannel("Ecrit");
            
            template.getTemplateContent().setContractCustomer(cc);
            template.getTemplateContent().setRefChannel(rc);
        }
        
        templateRepo.saveAndFlush(template);
    }

}
