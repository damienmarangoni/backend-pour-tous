package orange.omn.services.business;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import orange.omn.bean.Mask;
import orange.omn.repository.IMaskRepository;
import orange.omn.repository.common.SearchTools;
import orange.omn.repository.specification.MaskSpecificationBuilder;
import orange.omn.rest.errors.BadInput;
import orange.omn.services.interfaces.IMaskService;
import orange.omn.web.dto.MaskDTO;
import orange.omn.web.mapper.IMaskMapper;



@Service
public class MaskService implements IMaskService{
	
	@Autowired
	IMaskRepository maskRepo;
	
 
    @Autowired
    IMaskMapper maskMapper;
		
    @Override
    public Page<Mask> getMask(String search, Pageable pageable) {
        
        if (search != null) {
            MaskSpecificationBuilder builder = new MaskSpecificationBuilder();

            Pattern pattern = Pattern.compile(SearchTools.SEARCH_CRITERIA_REGEXP);
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }

            Specification<Mask> spec = builder.build();
            return  maskRepo.findAll(spec, pageable); 
        } else {
            return maskRepo.findAll(pageable);
        }
    }

	@Override
	public void modifyMask(MaskDTO maskDTO, String maskedNb) throws BadInput {
        Mask mask = maskRepo.findByMaskMaskedNb(maskedNb);
        mask.setMaskMaskedNb(maskDTO.getMaskMaskedNb());
        mask.setMaskDescription(maskDTO.getMaskDescription());
        mask.setMaskMessage(maskDTO.getMaskMessage());
        mask.setMaskPublicNb(maskDTO.getMaskPublicNb());
       
        maskRepo.save(mask);
        
	}


	@Override
	public void deleteMask(String maskedNb) throws BadInput {
		
	      Mask mask = maskRepo.findByMaskMaskedNb(maskedNb);     
	      maskRepo.delete(mask);		
	}

	@Override
	public Mask getMaskByNbPublic(String maskPublicNb) {
		
		Mask mask = maskRepo.findByMaskPublicNb(maskPublicNb);
		return mask;
	}

	@Override
	public void createMask(Mask mask) throws BadInput {
		System.out.println(mask.getMaskPublicNb());
		maskRepo.save(mask);
		
	}

}
