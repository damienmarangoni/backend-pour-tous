package orange.omn.rest;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import orange.omn.bean.CustomerQualification;
import orange.omn.rest.errors.BadInput;
import orange.omn.rest.utils.HeaderUtil;
import orange.omn.rest.utils.PaginationUtil;
import orange.omn.services.interfaces.ICustomerRouteService;
import orange.omn.web.dto.CustomerRouteDTO;
import orange.omn.web.dto.PartCustomerRouteDTO;
import orange.omn.web.mapper.ICustomerQualificationMapper;
import orange.omn.web.mapper.IPartCustomerQualificationMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author XT0087920
 *
 */
@CrossOrigin(maxAge = 3600)
@RestController
public class CustomerRouteController {

	@Autowired
	ICustomerRouteService custRtService;

	@Autowired
	ICustomerQualificationMapper custQualMapper;
	
	@Autowired
	IPartCustomerQualificationMapper partCustQualMapper;

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * @param custRoute
	 *            the customer route to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new customer route, or with status 400 (Bad Request) if the
	 *         customer route already exists
	 * @throws URISyntaxException
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/customerRoute")
	public ResponseEntity<?> createCustomerRoute(@Valid @RequestBody CustomerRouteDTO custRoute)
			throws URISyntaxException {

		try {
			custRtService.validateCustomerRoute(custQualMapper.customerQualificationDTOToBean(custRoute));
		} catch (BadInput e) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("customerRoute", "BadInput", e.getMessage())).body(null);
		}
//		return ResponseEntity.ok("customer route created");
		log.info("CREATE customer route : " + custRoute.getCustomerQualificationName());
		return ResponseEntity.status(HttpStatus.CREATED).body("customer route created");

	}
	
	@PutMapping("/updateRouteAll/{id}")
	public ResponseEntity<?> fullUpdateCustomer(@RequestBody CustomerRouteDTO fullCustRoute, @PathVariable("id") String id){
		
		try {
			custRtService.update(custQualMapper.customerQualificationDTOToBean(fullCustRoute, id));
			
		} catch (BadInput e) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("customerRoute", "BadInput", e.getMessage())).body(null);
		}
		
		log.info("customer route fully UPDATED : " + fullCustRoute.getCustomerQualificationName());
		return ResponseEntity.ok("customer route fully updated");
	
	}
	
	/**
	 * @param partialCustRoute : the customer route partially updated with its qualification expression list
	 * @return
	 */
	@PatchMapping("/updateRoute/{id}")
	public ResponseEntity<?> partialUpdateCustomer(@RequestBody PartCustomerRouteDTO partialCustRoute, @PathVariable("id") String id){
		
		try {
			custRtService.update(partCustQualMapper.customerQualificationDTOToBean(partialCustRoute, id));
			
		} catch (BadInput e) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("customerRoute", "BadInput", e.getMessage())).body(null);
		}
		
		log.info("customer route UPDATED : " + partialCustRoute.getCustomerQualificationName());
		return ResponseEntity.ok("customer route updated");
		
	}

	

	/**
	 * GET /customerRoutesOnDates?startDate=... : get n contract customer(s), aka
	 * "parcours client" from criteria, all of them if none
	 * 
	 * @param pageable
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws URISyntaxException
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/customerRoutesOnDates")
	public ResponseEntity<List<CustomerRouteDTO>> search(Pageable pageable,
			@RequestParam(value = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
			@RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate)
			throws URISyntaxException {

		Page<CustomerQualification> page = null;
		
		page = custRtService.getCustomerRoutes(startDate, endDate, pageable);

		try {
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/customerRoutesOnDates");
			// from this bunch of entities we've been retrieving, let's map it
			// all to DTOs and send it back to the client
			return new ResponseEntity<>(custQualMapper.customerQualificationToDTOs(page.getContent()), headers,
					HttpStatus.OK);
		} catch (InvalidDataAccessApiUsageException e) {
			// en cas d'erreur sur le champ de requête, ne pas informer le
			// client pour éviter le brute force
			return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
		}

	}

    

    /**
     * GET /customerRoutesForSimulator: get n contract customer(s), aka
     * "parcours client" from criteria, all of them if none
     * 
     * @param pageable
     * @param startDate
     * @param endDate
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/customerRoutesForSimulator")
    public ResponseEntity<List<CustomerRouteDTO>> searchForSimulator(
            @RequestParam(value = "codePhase", required = false) String codePhase,
            @RequestParam(value = "date", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            @RequestParam(value = "message", required = false) String[] message,
            Pageable pageable)
            throws URISyntaxException {

        Page<CustomerQualification> page = null;
        
        page = custRtService.getCustomerRoutesForSimulator(codePhase, date, message, pageable);

        try {
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/customerRoutesForSimulator");
            // from this bunch of entities we've been retrieving, let's map it
            // all to DTOs and send it back to the client
            return new ResponseEntity<>(custQualMapper.customerQualificationToDTOs(page.getContent()), headers,
                    HttpStatus.OK);
        } catch (InvalidDataAccessApiUsageException e) {
            // en cas d'erreur sur le champ de requête, ne pas informer le
            // client pour éviter le brute force
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
        }

    }

	/**
	 * GET /customerRoutes?search=customerQualificationName:service, -> get n
	 * contract customer(s), aka "parcours client" from customer qualification
	 * dto criteria, all of them if none
	 * 
	 * @param pageable
	 * @param search
	 * @return
	 * @throws URISyntaxException
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/customerRoutes")
	public ResponseEntity<List<CustomerRouteDTO>> search(Pageable pageable,
			@RequestParam(value = "search", required = false) String search) throws URISyntaxException {

		Page<CustomerQualification> page = null;
		
		page = custRtService.getCustomerRouteDetails(search, pageable);

		try {
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/customerRoutes");
			// from this bunch of entities we've been retrieving, let's map it
			// all to DTOs and send it back to the client
			return new ResponseEntity<>(custQualMapper.customerQualificationToDTOs(page.getContent()), headers,
					HttpStatus.OK);
		} catch (InvalidDataAccessApiUsageException e) {
			// en cas d'erreur sur le champ de requête, ne pas informer le
			// client pour éviter le brute force
			return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
		}

	}
    
   
   /**
 * @param idCustomerQualication, which customer route to delete 
 * @return
 * @throws URISyntaxException
 */
@Transactional
   @RequestMapping(method = RequestMethod.DELETE, value = "/deleteCustomerRoute/{id}")
   public ResponseEntity<?> deleteProgParent(@Valid @PathVariable("id") int idCustomerQualication) throws URISyntaxException {

       try {
           custRtService.deleteByIdCustomerQualification(idCustomerQualication);
       } catch (BadInput e) {
           return ResponseEntity.badRequest()
                   .headers(HeaderUtil.createFailureAlert("customerQualification", "BadInput", e.getMessage())).body(null);
       }
       log.info("DELETE customer route : " + idCustomerQualication );
       return ResponseEntity.ok("customer route deleted");
   }

}
