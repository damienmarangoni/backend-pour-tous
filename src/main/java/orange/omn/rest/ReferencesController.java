package orange.omn.rest;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import orange.omn.bean.RefCategory;
import orange.omn.bean.RefCommunity;
import orange.omn.bean.RefDefaultAudioTrack;
import orange.omn.bean.RefDefaultAudioTrackPhase;
import orange.omn.bean.RefPhase;
import orange.omn.bean.RefPriority;
import orange.omn.bean.RefTemplateItem;
import orange.omn.bean.Service;
import orange.omn.rest.utils.HeaderUtil;
import orange.omn.rest.utils.PaginationUtil;
import orange.omn.services.interfaces.IReferencesService;
import orange.omn.web.dto.RefCategoryDTO;
import orange.omn.web.dto.RefCommunityDTO;
import orange.omn.web.dto.RefDefaultAudioTrackDTO;
import orange.omn.web.dto.RefDefaultAudioTrackPhaseDTO;
import orange.omn.web.dto.RefPhaseDTO;
import orange.omn.web.dto.RefPriorityDTO;
import orange.omn.web.dto.RefTemplateItemDTO;
import orange.omn.web.dto.ServiceDTO;
import orange.omn.web.mapper.IReferencesMapper;


/**
 * @author XT0087920
 *
 */
@CrossOrigin(maxAge = 3600)
@RestController
public class ReferencesController {

	@Autowired
	IReferencesService referencesService;

	@Autowired
	IReferencesMapper referencesMapper;
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * @param pageable
	 * @param search
	 * @return a collection of priorities
	 * @throws URISyntaxException
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/refPriority")
	public ResponseEntity<List<RefPriorityDTO>> searchPriorities(Pageable pageable,
			@RequestParam(value = "search", required = false) String search) throws URISyntaxException {

		Page<RefPriority> page = null;
		
		page = referencesService.getRefPriority(search, pageable);

		try {
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/refPriority");
			// from this bunch of entities we've been retrieving, let's map it
			// all to DTOs and send it back to the client
			return new ResponseEntity<>(referencesMapper.refPriorityToDTOs(page.getContent()), headers,
					HttpStatus.OK);
		} catch (InvalidDataAccessApiUsageException e) {
			// en cas d'erreur sur le champ de requête, ne pas informer le
			// client pour éviter le brute force
			return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
		}

	}
	
	/**
	 * @param pageable
	 * @param search
	 * @return ref community collection (i.e Mouvango, Trusted profile, Opt out)
	 * @throws URISyntaxException
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/refCommunity")
	public ResponseEntity<List<RefCommunityDTO>> searchCommunities(Pageable pageable,
			@RequestParam(value = "search", required = false) String search) throws URISyntaxException {

		Page<RefCommunity> page = null;
		
		page = referencesService.getRefCommunity(search, pageable);

		try {
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/refCommunity");
			// from this bunch of entities we've been retrieving, let's map it
			// all to DTOs and send it back to the client
			return new ResponseEntity<>(referencesMapper.refCommunityToDTOs(page.getContent()), headers,
					HttpStatus.OK);
		} catch (InvalidDataAccessApiUsageException e) {
			// en cas d'erreur sur le champ de requête, ne pas informer le
			// client pour éviter le brute force
			return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
		}

	}
	
	
     /**
     * @return ref templates list
     * @throws URISyntaxException
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/refTemplateItem")
     public ResponseEntity<List<RefTemplateItemDTO>> search()
             throws URISyntaxException {
         
         List<RefTemplateItem> refTemplateItems = referencesService.getRefTemplateItemList();
    
         try {
             // from this bunch of entities we've been retrieving, let's map it
             // all to DTOs and send it back to the client
             return new ResponseEntity<>(referencesMapper.refTemplateItemToDTOs(refTemplateItems), HttpStatus.OK);
         } catch (InvalidDataAccessApiUsageException e) {
             // en cas d'erreur sur le champ de requête, ne pas informer le
             // client pour éviter le brute force
             return new ResponseEntity<>(null, HttpStatus.OK);
         }    
     }
    
    /**
     * @return services list
     * @throws URISyntaxException
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/services")
    public ResponseEntity<List<ServiceDTO>> searchService()
            throws URISyntaxException {
        
        List<Service> services = referencesService.getServicesList();
   
        try {
            // from this bunch of entities we've been retrieving, let's map it
            // all to DTOs and send it back to the client
            return new ResponseEntity<>(referencesMapper.refServiceToDTOs(services), HttpStatus.OK);
        } catch (InvalidDataAccessApiUsageException e) {
            // en cas d'erreur sur le champ de requête, ne pas informer le
            // client pour éviter le brute force
            return new ResponseEntity<>(null, HttpStatus.OK);
        }    
    }
    
    /**
     * @return a list of default audio tracks, played when customers call
     * @throws URISyntaxException
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/refDefAudioTracks")
    public ResponseEntity<List<RefDefaultAudioTrackDTO>> getRefDefaultAudioTracks()
            throws URISyntaxException {
        
        List<RefDefaultAudioTrack> refDefAudioTracks = referencesService.getRefDefaultAudioTrackList();
   
        try {
            // from this bunch of entities we've been retrieving, let's map it
            // all to DTOs and send it back to the client
            return new ResponseEntity<>(referencesMapper.refDefaultAudioTrackToDTOs(refDefAudioTracks), HttpStatus.OK);
        } catch (InvalidDataAccessApiUsageException e) {
            // en cas d'erreur sur le champ de requête, ne pas informer le
            // client pour éviter le brute force
            return new ResponseEntity<>(null, HttpStatus.OK);
        }    
    }
    
    
    /**
     * Upload custom audio file
     * @param filename : uploaded file
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/updateRefDefAudioTrack/{id}")
    public ResponseEntity<?> updateRefDefAudioTrack(@Valid @RequestBody RefDefaultAudioTrackDTO refDefAudioTrackDTO, @PathVariable("id") String id){
        
      try {
                     
          referencesService.modifyRefDefAudioTrack(refDefAudioTrackDTO, id);
          log.info("UPDATE RefDefAudioTracks : " + refDefAudioTrackDTO.getFilename());
          return ResponseEntity.ok("RefDefAudioTracks updated");  
          
      } catch (Exception e) {
              
          return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert("updateRefDefAudioTracks", "IOException", e.getMessage())).body(null);            
      }             
    }  
    
    
    /**
     * @param pageable /refDefaultAudioTracks?search=libelle:Femme
     * @param search let's get some items from specific attributes 
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/refDefaultAudioTracks")
	public ResponseEntity<List<RefDefaultAudioTrackDTO>> searchRefDefaultAudioTracks(Pageable pageable,
			@RequestParam(value = "search", required = false) String search) throws URISyntaxException {

		Page<RefDefaultAudioTrack> page = null;
		
		page = referencesService.getRefDefaultAudioTrackList(search, pageable);

		try {
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/refDefaultAudioTracks");
			// from this bunch of entities we've been retrieving, let's map it
			// all to DTOs and send it back to the client
			return new ResponseEntity<>(referencesMapper.refDefaultAudioTrackToDTOs(page.getContent()), headers,
					HttpStatus.OK);
		} catch (InvalidDataAccessApiUsageException e) {
			// en cas d'erreur sur le champ de requête, ne pas informer le
			// client pour éviter le brute force
			return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
		}
    }
    
    
    /**
     * @param pageable /refDefaultAudioTracks?search=libelle:Femme
     * @param search let's get some items from specific attributes 
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/refDefaultAudioTrackPhases")
    public ResponseEntity<List<RefDefaultAudioTrackPhaseDTO>> searchRefDefaultAudioTrackPhases(Pageable pageable,
            @RequestParam(value = "search", required = false) String search) throws URISyntaxException {

        Page<RefDefaultAudioTrackPhase> page = null;
        
        page = referencesService.getRefDefaultAudioTrackPhaseList(search, pageable);

        try {
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/refDefaultAudioTrackPhases");
            // from this bunch of entities we've been retrieving, let's map it
            // all to DTOs and send it back to the client
            return new ResponseEntity<>(referencesMapper.refDefaultAudioTrackPhaseToDTOs(page.getContent()), headers,
                    HttpStatus.OK);
        } catch (InvalidDataAccessApiUsageException e) {
            // en cas d'erreur sur le champ de requête, ne pas informer le
            // client pour éviter le brute force
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
        }
    }
    
    
    /**
     * @param pageable
     * @param search
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/refCategory")
    public ResponseEntity<List<RefCategoryDTO>> searchCategories(Pageable pageable,
            @RequestParam(value = "search", required = false) String search) throws URISyntaxException {

        Page<RefCategory> page = null;
        
        page = referencesService.getRefCategory(search, pageable);

        try {
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/refCategory");
            // from this bunch of entities we've been retrieving, let's map it
            // all to DTOs and send it back to the client
            return new ResponseEntity<>(referencesMapper.refCategoryToDTOs(page.getContent()), headers,
                    HttpStatus.OK);
        } catch (InvalidDataAccessApiUsageException e) {
            // en cas d'erreur sur le champ de requête, ne pas informer le
            // client pour éviter le brute force
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
        }

    }
    
    /**
     * @param pageable
     * @param search
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/refPhase")
    public ResponseEntity<List<RefPhaseDTO>> searchPhases(Pageable pageable,
            @RequestParam(value = "search", required = false) String search) throws URISyntaxException {

        Page<RefPhase> page = null;
        
        page = referencesService.getRefPhase(search, pageable);

        try {
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/refPhase");
            // from this bunch of entities we've been retrieving, let's map it
            // all to DTOs and send it back to the client
            return new ResponseEntity<>(referencesMapper.refPhaseToDTOs(page.getContent()), headers,
                    HttpStatus.OK);
        } catch (InvalidDataAccessApiUsageException e) {
            // en cas d'erreur sur le champ de requête, ne pas informer le
            // client pour éviter le brute force
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
        }

    }
}
