package orange.omn.rest;

import java.net.URISyntaxException;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author XT0087920
 * 
 * This is needed for keep-alive tests thrown by GTM
 *
 */
@CrossOrigin(maxAge = 3600)
@RestController
public class IAmAliveController { 
	
	@GetMapping("/backendalive")
	public String upAndAlive() throws URISyntaxException {
		return "backend alive";
	
	}

}
