package orange.omn.rest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import orange.omn.common.AudioUtils;
import orange.omn.rest.utils.HeaderUtil;

/**
 * @author XT0087920
 *
 */

@CrossOrigin(maxAge = 3600)
@RestController
public class AudioController {

    @Value("${audio.file.path}")
    private String audioFilePath;
    
    @Value("${audio.default.file.path}")
    private String audioDefaultFilePath;
    
    @Value("${commande.sox}")
    private String soxCmd;  
    
    private final Logger log = LoggerFactory.getLogger(this.getClass());
  
    /**
     * GET /audioFile. : get audio file from its name
     * 
     * @param fileName
     * @return
     * @throws IOException
    */
     @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/audioFile")
     public void getAudioFile(HttpServletResponse response, @RequestParam(value = "fileName", required = false) String fileName) {  
         
         File listenFile = new File(audioFilePath + fileName);
         if(fileName!=null&&fileName!="") {
             try {
                AudioUtils.listenHttpFile(response, listenFile, fileName, soxCmd);
            } catch (IOException e) {
                response.setStatus(0);
            }    
         }
     }  
     
     /**
      * GET /audioDefaultFile. : get audio file from its name
      * 
      * @param fileName
      * @return
      * @throws IOException
     */
      @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/audioDefaultFile")
      public void getAudioDefaultFile(HttpServletResponse response, @RequestParam(value = "fileName", required = false) String fileName) {  
          
          File listenFile = new File(audioDefaultFilePath + fileName);
          if(fileName!=null&&fileName!="") {
              try {
                AudioUtils.listenHttpFile(response, listenFile, fileName, soxCmd);
            } catch (IOException e) {
                response.setStatus(0);
            }    
          }
      }  
      
      /**
       * GET /isAudioFilePresent. : get audio presence file from its name
       * 
       * @param fileName
       * @return
       * @throws IOException
      */
       @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/isAudioFilePresent")
       public boolean isAudioFilePresent(@RequestParam(value = "fileName", required = false) String fileName) {  
           
           Boolean res = false;
           FileInputStream listenFile = null;
           
           try {
               listenFile = new FileInputStream(audioFilePath + fileName);
               res= true;
           } catch (FileNotFoundException e) {
               res = false;
           }
           
           if(!res) {
               try {
                   listenFile = new FileInputStream(audioDefaultFilePath + fileName);
                   res= true;
               } catch (FileNotFoundException e1) {
                   res= false;
               }
           }
           
           return res;
       } 
       
      
      /**
       * Test Audio file format
       * @param file : file to test
       * @return
       */
      @RequestMapping(method = RequestMethod.POST, value = "/isAudioFileA8k")
      public boolean isAudioFileA8k(@RequestParam("file") MultipartFile file){ 
          
          boolean res = false;
          
          try {
              InputStream bufferedIn = new BufferedInputStream(file.getInputStream());
              res = AudioUtils.isA8K(bufferedIn);
          } catch (IOException e) {
              res = false;
          }
        
          return res;          
      } 
       
      
      /**
       * Upload custom audio file
       * @param filename : uploaded file
       * @return
       */
      @RequestMapping(method = RequestMethod.POST, value = "/updateAudioFile/{fileName}")
      public ResponseEntity<?> updateAudioFile(@RequestParam("file") MultipartFile file, @PathVariable("fileName") String fileName){
          
        try {
                       
            AudioUtils.convertAndSaveFileA8K(audioFilePath,
                    fileName,
                    file.getInputStream(), 
                    soxCmd);
            
            log.info("UPDATE custom audio file : " + fileName);
            return ResponseEntity.ok("Audio file updated");  
            
        } catch (IOException e) {
                
            return ResponseEntity.badRequest()
                  .headers(HeaderUtil.createFailureAlert("updateAudioFile", "IOException", e.getMessage())).body(null);            
        }             
      }  
       
      
      /**
       * Upload default audio file
       * @param filename : uploaded file
       * @return
       */
      @RequestMapping(method = RequestMethod.POST, value = "/updateDefaultAudioFile/{fileName}")
      public ResponseEntity<?> updateDefaultAudioFile(@RequestParam("file") MultipartFile file, @PathVariable("fileName") String fileName){
          
        try {
                       
            AudioUtils.convertAndSaveFileA8K(audioDefaultFilePath,
                    fileName,
                    file.getInputStream(), 
                    soxCmd);
            
            log.info("UPDATE default audio file : " + fileName);
            return ResponseEntity.ok("Audio file updated");  
            
        } catch (IOException e) {
                
            return ResponseEntity.badRequest()
                  .headers(HeaderUtil.createFailureAlert("updateAudioFile", "IOException", e.getMessage())).body(null);            
        }             
      }
}
