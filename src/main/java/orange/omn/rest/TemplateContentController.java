package orange.omn.rest;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import orange.omn.bean.ContractCustomer;
import orange.omn.bean.TemplateContent;
import orange.omn.common.RefChanEnum;
import orange.omn.rest.errors.BadInput;
import orange.omn.rest.utils.HeaderUtil;
import orange.omn.rest.utils.PaginationUtil;
import orange.omn.services.interfaces.IContractCustomerService;
import orange.omn.services.interfaces.ITemplateContentService;
import orange.omn.web.dto.ContractCustomerDTO;
import orange.omn.web.dto.TemplateContentDTO;
import orange.omn.web.mapper.ITemplateContentMapper;

/**
 * @author XT0087920
 *
 */
@CrossOrigin(maxAge = 3600)
@RestController
public class TemplateContentController {

	@Autowired
	ITemplateContentService templateContentService;
	
	@Autowired
	IContractCustomerService contractCustomerService;

	@Autowired
	ITemplateContentMapper templateContentMapper;
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/templateContent")
	public ResponseEntity<List<TemplateContentDTO>> searchTemplateContent(Pageable pageable,
			@RequestParam(value = "search", required = false) String search) throws URISyntaxException {

		Page<TemplateContent> page = null;

		page = templateContentService.getTemplateContent(search, pageable);

		try {
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/templateContent");
			// from this bunch of entities we've been retrieving, let's map it
			// all to DTOs and send it back to the client
			return new ResponseEntity<>(templateContentMapper.templateContentToDTOs(page.getContent()), headers,
					HttpStatus.OK);
		} catch (InvalidDataAccessApiUsageException e) {
			// en cas d'erreur sur le champ de requête, ne pas informer le
			// client pour éviter le brute force
			return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
		}

	}


	/**
	 * @param channelId
	 *            : is vocal or text ? we need text only
	 *            (pomniws.ref_channel.id_channel = 2), to display
	 * @return a TemplateContentDTO list containing only templateContentTxt written
	 * @throws URISyntaxException
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/getWritTempContentText")
	public ResponseEntity<List<TemplateContentDTO>> getTemplateContent()
			throws URISyntaxException {

		List<TemplateContent> templateContents = templateContentService.getWrittenTemplateContentText();

		try {
			// from this bunch of entities we've been retrieving, let's map it
			// all to DTOs and send it back to the client
			return new ResponseEntity<>(templateContentMapper.templateContentToDTOs(templateContents), HttpStatus.OK);
		} catch (InvalidDataAccessApiUsageException e) {
			// en cas d'erreur sur le champ de requête, ne pas informer le
			// client pour éviter le brute force
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
	}

	/**
	 * @param idTemplateContent
	 * @return
	 * @throws URISyntaxException
	 */
	@Transactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteTemplateContent/{id}")
	public ResponseEntity<?> deleteTemplateContent(@Valid @PathVariable("id") String idTemplateContent)
			throws URISyntaxException {

		try {
			templateContentService.deleteTemplateContent(idTemplateContent);
		} catch (BadInput e) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("templateContent", "BadInput", e.getMessage())).body(null);
		}
		log.info("DELETE template content : " + idTemplateContent);
		return ResponseEntity.ok("written message deleted");
	}

	/**
	 * @param templateContent,
	 *            which is from the client side, the written message template
	 *            set to sms
	 * @param idTemplateContent
	 * @return
	 */
	@PutMapping("/updateTemplateContent/{id}")
	public ResponseEntity<?> updateTemplateContent(@RequestBody TemplateContentDTO templateContent,
			@Valid @PathVariable("id") String idTemplateContent) {

		try {
			templateContentService.updateTemplateContent(
					templateContentMapper.templateContentDTOToBean(templateContent), idTemplateContent);

		} catch (BadInput e) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("templateContent", "BadInput", e.getMessage())).body(null);
		}
		
		log.info("UPDATE template content : " + idTemplateContent);
		return ResponseEntity.ok("template content updated");

	}

	@PostMapping("/createWritTempContentText")
	public ResponseEntity<?> createTemplateContent(@Valid @RequestBody TemplateContentDTO templateContentDTO)
			throws URISyntaxException {

		try {
			templateContentDTO.setChannelId(RefChanEnum.ECRIT.getChannelId());
			templateContentDTO.setChannelLib(RefChanEnum.ECRIT.getChannelLib());
			templateContentService
					.createTemplateContent(templateContentMapper.templateContentDTOToBean(templateContentDTO));
		} catch (BadInput e) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("TemplateContent", "BadInput", e.getMessage())).body(null);
		}
		
		log.info("CREATE written template content : " + templateContentDTO.getTemplateContentName());
		return ResponseEntity.status(HttpStatus.CREATED).body("template content created");

	}

	@GetMapping("/contractCustomerLabels")
	public ResponseEntity<List<ContractCustomerDTO>> getcontractCustomerLabels() throws URISyntaxException {
		
		List<ContractCustomer> contractCustomers = contractCustomerService.getContractCustomerList();
		
		try {
			// from this bunch of entities we've been retrieving, let's map it
			// all to DTOs and send it back to the client
			return new ResponseEntity<>(templateContentMapper.constractCustomerToDTOs(contractCustomers), HttpStatus.OK);
		} catch (InvalidDataAccessApiUsageException e) {
			// en cas d'erreur sur le champ de requête, ne pas informer le
			// client pour éviter le brute force
			return new ResponseEntity<>(null, HttpStatus.OK);
		}

	}

}
