package orange.omn.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import orange.omn.rest.errors.BadInput;
import orange.omn.rest.utils.HeaderUtil;
import orange.omn.services.interfaces.IQualificationExpressionService;
import orange.omn.web.dto.QualificationExpressionDTO;
import orange.omn.web.mapper.IQualificationExpressionMapper;

/**
 * @author XT0087920
 *
 */

@CrossOrigin(maxAge = 3600)
@RestController
public class QualificationExpressionController {
	
	@Autowired
	IQualificationExpressionService qualifExprService;

	@Autowired
	IQualificationExpressionMapper qualifExprMapper;
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * @param qualExpr, the list of qualification expression objects to save
	 * @return
	 * @throws URISyntaxException
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/createQualExpr/{id}")
	public ResponseEntity<?> createQualExpr(@Valid @RequestBody List<QualificationExpressionDTO> qualExprList, @PathVariable("id") String customerRouteId)
			throws URISyntaxException {

		try {
			qualifExprService.createQualifExpress(qualExprList, customerRouteId);
		} catch (BadInput e) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("qualificationExperssion", "BadInput", e.getMessage())).body(null);
		}
		//return ResponseEntity.ok("qualification expression created");
		log.info("CREATE qualification expressions linked to customer route : " + customerRouteId);
		return ResponseEntity.status(HttpStatus.CREATED).body("qualification expression created");

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/deleteQualExpr")
	public ResponseEntity<?> deleteQualExpr(@Valid @RequestBody QualificationExpressionDTO qualExpr) {
		return ResponseEntity.ok("qualification expression delated");
		
	}


}
