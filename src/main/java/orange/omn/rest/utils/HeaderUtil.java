package orange.omn.rest.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

public class HeaderUtil {
	private static final Logger log = LoggerFactory.getLogger(HeaderUtil.class);

    public static HttpHeaders createAlert(String message, Object param) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-OMNIParcoursClient-alert", message);
        headers.add("X-OMNIParcoursClient-params", param.toString());
        return headers;
    }

    public static HttpHeaders createEntityCreationAlert(String entityName, Object param) {
        return createAlert("A new " + entityName + " is created with identifier " + param, param);
    }

    public static HttpHeaders createEntityUpdateAlert(String entityName, Object param) {
        return createAlert("A " + entityName + " is updated with identifier " + param, param);
    }

    public static HttpHeaders createEntityDeletionAlert(String entityName, Object param) {
        return createAlert("A " + entityName + " is deleted with identifier " + param, param);
    }

    public static HttpHeaders createFailureAlert(String entityName, String errorKey, String defaultMessage) {
        log.error("Entity creation failed, {}", defaultMessage);
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-OMNIParcoursClient-error", defaultMessage);
        headers.add("X-OMNIParcoursClient-params", entityName);
        return headers;
    }

}
