package orange.omn.rest;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import orange.omn.bean.Template;
import orange.omn.rest.errors.BadInput;
import orange.omn.rest.utils.HeaderUtil;
import orange.omn.rest.utils.PaginationUtil;
import orange.omn.services.interfaces.ITemplateService;
import orange.omn.web.dto.TemplateDTO;
import orange.omn.web.mapper.ITemplateMapper;

/**
 * @author XT0087920
 *
 */

@CrossOrigin(maxAge = 3600)
@RestController
public class TemplateController {

	@Autowired
	ITemplateService templateService;

	@Autowired
	ITemplateMapper templateMapper;

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * GET /templates?search=codePhase:<value>, -> get n
	 * template(s),  "template" from template
	 * dto criteria, all of them if none
	 * 
	 * @param pageable
	 * @param search
	 * @return
	 * @throws URISyntaxException
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/templates")
	public ResponseEntity<List<TemplateDTO>> search(Pageable pageable,
			@RequestParam(value = "search", required = false) String search) throws URISyntaxException {

		Page<Template> page = null;
		
		page = templateService.getTemplate(search, pageable);

		try {
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/templates");
			// from this bunch of entities we've been retrieving, let's map it
			// all to DTOs and send it back to the client
			return new ResponseEntity<>(templateMapper.templateToDTOs(page.getContent()), headers,
					HttpStatus.OK);
		} catch (InvalidDataAccessApiUsageException e) {
			// en cas d'erreur sur le champ de requête, ne pas informer le
			// client pour éviter le brute force
			return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
		}

	}
    
    /**
     * GET /templateOnIdTemplate. : get n contract customer(s), aka
     * "parcours client" from criteria, all of them if none
     * 
     * @param pageable
     * @param startDate
     * @param endDate
     * @return
     * @throws URISyntaxException
    */
     @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/templateOnIdTemplate")
     public ResponseEntity<TemplateDTO> search(@RequestParam(value = "idTemplate", required = false) int idTemplate)
             throws URISyntaxException {
         
         Template template = templateService.getTemplateByIdTemplate(idTemplate);
    
         try {
             // from this bunch of entities we've been retrieving, let's map it
             // all to DTOs and send it back to the client
             return new ResponseEntity<>(templateMapper.templateToDTO(template), HttpStatus.OK);
         } catch (InvalidDataAccessApiUsageException e) {
             // en cas d'erreur sur le champ de requête, ne pas informer le
             // client pour éviter le brute force
             return new ResponseEntity<>(null, HttpStatus.OK);
         }    
     }    
     
         
     /**
      * @param 
      * @return
      * @throws URISyntaxException
     */
      @RequestMapping(method = RequestMethod.PUT, value = "/modifyWrittenTemplate/{id}")
      public ResponseEntity<?> modifyWrittenTemplate(@Valid @RequestBody TemplateDTO template, @PathVariable("id") int templateId)
              throws URISyntaxException {
    
          try {
              templateService.modifyWrittenTemplate(template, templateId);
          } catch (BadInput e) {
              return ResponseEntity.badRequest()
                      .headers(HeaderUtil.createFailureAlert("Template", "BadInput", e.getMessage())).body(null);
          }
          log.info("UPDATE template content : " + template.getTemplateName());
          return ResponseEntity.ok("Template updated");
    
      }      /**
       * @param qualExpr, the list of qualification expression objects to save
       * @return
       * @throws URISyntaxException
       */
      @RequestMapping(method = RequestMethod.POST, value = "/createTemplate/{id}")
      public ResponseEntity<?> createProgParent(@Valid @RequestBody TemplateDTO templateDTO, @PathVariable("id") String customerRouteId)
              throws URISyntaxException {

          try {
              templateService.createTemplate(templateDTO);
          } catch (BadInput e) {
              return ResponseEntity.badRequest()
                      .headers(HeaderUtil.createFailureAlert("programParent", "BadInput", e.getMessage())).body(null);
          }
          log.info("CREATE template content : " + templateDTO.getTemplateName());
          return ResponseEntity.ok("template content created");

      } 
     
     /**
      * @param 
      * @return
      * @throws URISyntaxException
     */
      @RequestMapping(method = RequestMethod.DELETE, value = "/deleteTemplate/{id}")
      public ResponseEntity<?> deleteTemplate(@Valid @PathVariable("id") int templateId)
              throws URISyntaxException {

          try {
              templateService.deleteTemplate(templateId);
          } catch (BadInput e) {
              return ResponseEntity.badRequest()
                      .headers(HeaderUtil.createFailureAlert("template", "BadInput", e.getMessage())).body(null);
          }
          
          log.info("DELETE template content : " + templateId);
          
          return ResponseEntity.ok("template content deleted");

      }

      
      /**
       * @param 
       * @return
       * @throws URISyntaxException
      */
       @RequestMapping(method = RequestMethod.PUT, value = "/modifyAudioTemplate/{id}")
       public ResponseEntity<?> modifyAudioTemplate(@Valid @RequestBody TemplateDTO template, @PathVariable("id") int templateId)
               throws URISyntaxException {
     
           try {
               templateService.modifyAudioTemplate(template, templateId);
           } catch (BadInput e) {
               return ResponseEntity.badRequest()
                       .headers(HeaderUtil.createFailureAlert("Template", "BadInput", e.getMessage())).body(null);
           }
           
           log.info("UPDATE template content : " + template.getTemplateName());
           return ResponseEntity.ok("Template updated");
     
       }
}
