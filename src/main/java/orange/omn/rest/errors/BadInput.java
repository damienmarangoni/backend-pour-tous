package orange.omn.rest.errors;

public class BadInput extends Exception {
	public BadInput(String message) {
        super(message);
    }

}
