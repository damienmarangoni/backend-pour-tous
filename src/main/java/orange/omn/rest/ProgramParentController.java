package orange.omn.rest;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import orange.omn.bean.ProgramParent;
import orange.omn.rest.errors.BadInput;
import orange.omn.rest.utils.HeaderUtil;
import orange.omn.rest.utils.PaginationUtil;
import orange.omn.services.interfaces.IProgramParentService;
import orange.omn.web.dto.ProgramParentDTO;
import orange.omn.web.mapper.IProgramParentMapper;

/**
 * @author XT0087920
 *
 */

@CrossOrigin(maxAge = 3600)
@RestController
public class ProgramParentController {

	@Autowired
	IProgramParentService programParentService;

	@Autowired
	IProgramParentMapper programParentMapper;
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	   /**
     * GET /customerRoutesOnDates?startDate=... : get n contract customer(s), aka
     * "parcours client" from criteria, all of them if none
     * 
     * @param pageable
     * @param startDate
     * @param endDate
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/programParentsOnCodePhase")
    public ResponseEntity<List<ProgramParentDTO>> searchProgParentOnCodePhase(Pageable pageable,
            @RequestParam(value = "codePhase", required = false) String codePhase,
            @RequestParam(value = "customerQualificationId", required = false) int customerQualificationId)
            throws URISyntaxException {

        Page<ProgramParent> page = null;
        
        page = programParentService.getProgramParents(codePhase, customerQualificationId, pageable);

        try {
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/programParentsOnCodePhase");
            // from this bunch of entities we've been retrieving, let's map it
            // all to DTOs and send it back to the client
            return new ResponseEntity<>(programParentMapper.programParentToDTOs(page.getContent()), headers,
                    HttpStatus.OK);
        } catch (InvalidDataAccessApiUsageException e) {
            // en cas d'erreur sur le champ de requête, ne pas informer le
            // client pour éviter le brute force
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
        }

    }

    /**
      * GET /customerRoutesOnDates?startDate=... : get n contract customer(s), aka
      * "parcours client" from criteria, all of them if none
      * 
      * @param pageable
      * @param startDate
      * @param endDate
      * @return
      * @throws URISyntaxException
      */
     @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/programParentsOnCustQualifId")
     public ResponseEntity<List<ProgramParentDTO>> searchProgParentsOnCustQualifId(Pageable pageable,
             @RequestParam(value = "customerQualificationId", required = false) int customerQualificationId)
             throws URISyntaxException {
    
         Page<ProgramParent> page = null;
         
         page = programParentService.getProgramParentsByCustQualifId(customerQualificationId, pageable);
    
         try {
             HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/programParentsOnCodePhase");
             // from this bunch of entities we've been retrieving, let's map it
             // all to DTOs and send it back to the client
             return new ResponseEntity<>(programParentMapper.programParentToDTOs(page.getContent()), headers,
                     HttpStatus.OK);
         } catch (InvalidDataAccessApiUsageException e) {
             // en cas d'erreur sur le champ de requête, ne pas informer le
             // client pour éviter le brute force
             return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
         }
    
     }

	/**
	 * GET /programParents?search=idCustomerQualification:<value>, -> get n
	 * template(s),  "template" from template
	 * dto criteria, all of them if none
	 * 
	 * @param pageable
	 * @param search
	 * @return
	 * @throws URISyntaxException
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/programParents")
	public ResponseEntity<List<ProgramParentDTO>> search(Pageable pageable,
			@RequestParam(value = "search", required = false) String search) throws URISyntaxException {

		Page<ProgramParent> page = null;
		
		page = programParentService.getProgramParentByCrit(search, pageable);

		try {
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/programParents");
			// from this bunch of entities we've been retrieving, let's map it
			// all to DTOs and send it back to the client
			return new ResponseEntity<>(programParentMapper.programParentToDTOs(page.getContent()), headers,
					HttpStatus.OK);
		} catch (InvalidDataAccessApiUsageException e) {
			// en cas d'erreur sur le champ de requête, ne pas informer le
			// client pour éviter le brute force
			return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
		}

	}
	
	   /**
     * @param qualExpr, the list of qualification expression objects to save
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(method = RequestMethod.POST, value = "/createProgParent/{id}")
    public ResponseEntity<?> createProgParent(@Valid @RequestBody ProgramParentDTO progParent, @PathVariable("id") String customerRouteId)
            throws URISyntaxException {

        try {
            programParentService.createProgParent(progParent, customerRouteId);
        } catch (BadInput e) {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert("programParent", "BadInput", e.getMessage())).body(null);
        }
//        return ResponseEntity.ok("program parent created");
        log.info("CREATE program parent : " + progParent.getIdProgramParent());
        return ResponseEntity.status(HttpStatus.CREATED).body("program parent created");

    }
    
    /**
  * @param qualExpr, the list of qualification expression objects to save
  * @return
  * @throws URISyntaxException
  */
 @RequestMapping(method = RequestMethod.POST, value = "/modifyProgParent/{id}")
 public ResponseEntity<?> modifyProgParent(@Valid @RequestBody ProgramParentDTO progParent, @PathVariable("id") String customerRouteId)
         throws URISyntaxException {

     try {
         programParentService.modifyProgParent(progParent, customerRouteId);
     } catch (BadInput e) {
         return ResponseEntity.badRequest()
                 .headers(HeaderUtil.createFailureAlert("programParent", "BadInput", e.getMessage())).body(null);
     }
     log.info("UPDATE program parent : " + progParent.getIdProgramParent());
     return ResponseEntity.ok("program parent modified");

 }
    
     /**
    * @param progParent, program parent to delete
    * @return
    * @throws URISyntaxException
    */
    @Transactional
    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteProgParent/{id}")
    public ResponseEntity<?> deleteProgrammation(@Valid @PathVariable("id") int idProgramParent)
        throws URISyntaxException {

        try {
            programParentService.deleteProgrammation(idProgramParent);
        } catch (BadInput e) {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert("programParent", "BadInput", e.getMessage())).body(null);
        }
        log.info("DELETE program parent : " + idProgramParent);
        return ResponseEntity.ok("program parent deleted");
    }   
}
