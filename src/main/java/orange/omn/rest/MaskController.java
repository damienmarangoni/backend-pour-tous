package orange.omn.rest;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import orange.omn.bean.Mask;
import orange.omn.rest.errors.BadInput;
import orange.omn.rest.utils.HeaderUtil;
import orange.omn.rest.utils.PaginationUtil;
import orange.omn.services.interfaces.IMaskService;
import orange.omn.web.dto.MaskDTO;
import orange.omn.web.dto.TemplateContentDTO;
import orange.omn.web.dto.TemplateDTO;
import orange.omn.web.mapper.IMaskMapper;

/**
 * @author XT0009760
 *
 */

@CrossOrigin(maxAge = 3600)
@RestController
public class MaskController {

	@Autowired
	IMaskService maskService;

	@Autowired
	IMaskMapper maskMapper;


	/**
	 * GET /mask?search=codePhase:<value>, -> get n
	 * mask(s),  "mask" from mask
	 * dto criteria, all of them if none
	 * 
	 * @param pageable
	 * @param search
	 * @return
	 * @throws URISyntaxException
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/masks")
	public ResponseEntity<List<MaskDTO>> search(Pageable pageable,
			@RequestParam(value = "search", required = false) String search) throws URISyntaxException {

		Page<Mask> page = null;
		
		page = maskService.getMask(search, pageable);

		try {
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/masks");
			// from this bunch of entities we've been retrieving, let's map it
			// all to DTOs and send it back to the client
			return new ResponseEntity<>(maskMapper.maskToDTOs(page.getContent()), headers,
					HttpStatus.OK);
		} catch (InvalidDataAccessApiUsageException e) {
			// en cas d'erreur sur le champ de requête, ne pas informer le
			// client pour éviter le brute force
			return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
		}

	}
    

	
    @RequestMapping(method = RequestMethod.POST, value = "/modifyMask/{numMasked}")
    public ResponseEntity<?> modifyProgParent(@Valid @RequestBody MaskDTO mask, @PathVariable("numMasked") String masknumMasked)
            throws URISyntaxException {
  
        try {
            maskService.modifyMask(mask, masknumMasked);
        } catch (BadInput e) {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert("Mask", "BadInput", e.getMessage())).body(null);
        }
        return ResponseEntity.ok("Mask updated");
  
    }	
    
	@PostMapping("/createMask")
	public ResponseEntity<?> createMask(@Valid @RequestBody MaskDTO mask)
			throws URISyntaxException {

		try {
			System.out.println("Creation du numéro masqué description: " + maskMapper.maskDTOToBean(mask).getMaskDescription());
			System.out.println("Creation du numéro masqué masqué: " + maskMapper.maskDTOToBean(mask).getMaskMaskedNb());
			System.out.println("Creation du numéro masqué SMS: " + maskMapper.maskDTOToBean(mask).getMaskMessage());
			System.out.println("Creation du numéro masqué Public: " + maskMapper.maskDTOToBean(mask).getMaskPublicNb());
			// On vérifie si le numéro masqué n'existe pas déjà
			
			
			maskService.createMask(maskMapper.maskDTOToBean(mask));
			
		} catch (BadInput e) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("mask", "BadInput", e.getMessage())).body(null);
		}
		return ResponseEntity.status(HttpStatus.CREATED).body("mask created");

	}
	
	/**
	 * @param idNumGIS
	 * @return
	 * @throws URISyntaxException
	 */
	@Transactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteMask/{idNumGIS}")
	public ResponseEntity<?> deleteMask(@Valid @PathVariable("idNumGIS") String stIdNumGIS)
			throws URISyntaxException {

		try {
			maskService.deleteMask(stIdNumGIS);
		} catch (BadInput e) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("mask", "BadInput", e.getMessage())).body(null);
		}
		return ResponseEntity.ok("mask deleted");
	}

	
}
