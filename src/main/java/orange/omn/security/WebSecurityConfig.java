package orange.omn.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
//@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
//	    http
//	      .httpBasic().disable();
//	    http
//	      .authorizeRequests()
//	        .antMatchers("/user", "/").permitAll()
//	        .anyRequest().authenticated()
//	      .and()
//            .formLogin()
//            .loginProcessingUrl("/greeting")
//            .permitAll()
//	      .and()
//	      	.csrf()
//	        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
		http
			.authorizeRequests()
			.antMatchers("/**").permitAll()
			.anyRequest().authenticated()
		.and()
			.formLogin()
			.loginProcessingUrl("/greeting")
			.permitAll()
		.and()
			.csrf().disable();
//			.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
//		.and()
//        	.addFilterAfter(new CsrfCookieGeneratorFilter(), CsrfFilter.class);
	  }

//	@Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests()
//                .antMatchers("/greeting").permitAll()
//                .anyRequest().authenticated()
//                .and()
//            .formLogin()
//                .loginProcessingUrl("/login")
//                .permitAll()
//                .and()
//            .logout()
//                .permitAll();
//    }

//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("user").password("password").roles("USER");
//    }
}