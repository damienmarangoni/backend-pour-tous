package orange.omn.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Configuration
public class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {
//	  @Autowired
//	  AccountDaoImpl accountRepository;

	  @Override
	  public void init(AuthenticationManagerBuilder auth) throws Exception {
	    auth.userDetailsService(userDetailsService());
	  }

	  @Bean
	  UserDetailsService userDetailsService() {
	    return new UserDetailsService() {

	      @Override
	      public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//	        Account account = accountRepository.findUserByName(username);
//	    	  if(account != null) {
//	    		  account.setPassword(username);
//	    		  return new User(account.getFirstName(), account.getPassword(), true, true, true, true,
//	    				  AuthorityUtils.createAuthorityList("USER"));
//	    	  } else {
	    		  throw new UsernameNotFoundException("could not find the user '" + username + "'");
//	    	  }
	      }

	    };
	  }
}
