package orange.omn.bean;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Auto-generated by:
 * org.apache.openjpa.jdbc.meta.ReverseMappingTool$AnnotatedCodeGenerator
 */
@Entity
@Table(name="template_type")
public class TemplateType
{
    @Basic
    @Column(name="template_type_description", nullable=false, length=100)
    private String templateTypeDescription;

    @Id
    @Column(name="template_type_id", columnDefinition="INT")
    private int templateTypeId;


    public TemplateType ()
    {
    }

    public TemplateType (int templateTypeId)
    {
        this.templateTypeId = templateTypeId;
    }

    public String getTemplateTypeDescription ()
    {
        return templateTypeDescription;
    }

    public void setTemplateTypeDescription (String templateTypeDescription)
    {
        this.templateTypeDescription = templateTypeDescription;
    }

    public int getTemplateTypeId ()
    {
        return templateTypeId;
    }

    public void setTemplateTypeId (int templateTypeId)
    {
        this.templateTypeId = templateTypeId;
    }
}