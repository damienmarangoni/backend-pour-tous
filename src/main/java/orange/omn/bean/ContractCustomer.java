package orange.omn.bean;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Auto-generated by:
 * org.apache.openjpa.jdbc.meta.ReverseMappingTool$AnnotatedCodeGenerator
 */
@Entity
@Table(name="contract_customer")
public class ContractCustomer
{
    @Id
    @Column(name="id_contract_customer", columnDefinition="INT")
    private int contractCustomerId;

    @Basic
    @Column(nullable=false, length=45)
    private String label;

    @OneToMany(targetEntity=orange.omn.bean.TemplateContent.class, mappedBy="contractCustomer", cascade=CascadeType.MERGE)
    private Set templateContents = new HashSet ();


    public ContractCustomer ()
    {
    }

    public ContractCustomer (int contractCustomerId)
    {
        this.contractCustomerId = contractCustomerId;
    }

    public ContractCustomer(int contractCustomerId, String label) {
    	this.contractCustomerId = contractCustomerId;
    	this.label = label;
	}

	public int getContractCustomerId ()
    {
        return contractCustomerId;
    }

    public void setContractCustomerId (int contractCustomerId)
    {
        this.contractCustomerId = contractCustomerId;
    }

    public String getLabel ()
    {
        return label;
    }

    public void setLabel (String label)
    {
        this.label = label;
    }

    public Set getTemplateContents ()
    {
        return templateContents;
    }

    public void setTemplateContents (Set templateContents)
    {
        this.templateContents = templateContents;
    }
}