package orange.omn.bean;

import java.io.Serializable;

/**
 * Application identity class for: orange.omn.bean.Poi
 *
 * Auto-generated by:
 * org.apache.openjpa.enhance.ApplicationIdTool
 */
public class PoiId
    implements Serializable
{
    static
    {
        // register persistent class in JVM
        try { Class.forName ("orange.omn.bean.Poi"); }
        catch (Exception e) {}
    }

    public int poiId;
    public String poiZipcode;

    public PoiId ()
    {
    }

    public PoiId (String str)
    {
        fromString (str);
    }

    public int getPoiId ()
    {
        return poiId;
    }

    public void setPoiId (int poiId)
    {
        this.poiId = poiId;
    }

    public String getPoiZipcode ()
    {
        return poiZipcode;
    }

    public void setPoiZipcode (String poiZipcode)
    {
        this.poiZipcode = poiZipcode;
    }

    public String toString ()
    {
        return String.valueOf (poiId)
            + "::" + poiZipcode;
    }

    public int hashCode ()
    {
        int rs = 17;
        rs = rs * 37 + poiId;
        rs = rs * 37 + ((poiZipcode == null) ? 0 : poiZipcode.hashCode ());
        return rs;
    }

    public boolean equals (Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null || obj.getClass () != getClass ())
            return false;

        PoiId other = (PoiId) obj;
        return (poiId == other.poiId)
            && ((poiZipcode == null && other.poiZipcode == null)
            || (poiZipcode != null && poiZipcode.equals (other.poiZipcode)));
    }

    private void fromString (String str)
    {
        Tokenizer toke = new Tokenizer (str);
        str = toke.nextToken ();
        poiId = Integer.parseInt (str);
        str = toke.nextToken ();
        if ("null".equals (str))
            poiZipcode = null;
        else
            poiZipcode = str;
    }

    protected static class Tokenizer
    {
        private final String str;
        private int last;

        public Tokenizer (String str)
        {
            this.str = str;
        }

        public String nextToken ()
        {
            int next = str.indexOf ("::", last);
            String part;
            if (next == -1)
            {
                part = str.substring (last);
                last = str.length ();
            }
            else
            {
                part = str.substring (last, next);
                last = next + 2;
            }
            return part;
        }
    }
}